package net.gianvito.coap.debug.utils;

import java.io.Serializable;

/**
 * Message used by debug software.
 * 
 * @author Gianvito Morena
 *
 */
public class DebugMessage implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	
	private static final long serialVersionUID = -3015644561202001564L;
	private DebugMessageType type;
	Object message;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public DebugMessage(){
		
	}
	
	/**
	 * Constructor.
	 * 
	 * @param type
	 * @param message
	 */
	public DebugMessage(DebugMessageType type, Object message){
		this.type = type;
		this.message = message;
	}
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public DebugMessageType getType() {
		return type;
	}
	public void setType(DebugMessageType type) {
		this.type = type;
	}
	public Object getMessage() {
		return message;
	}
	public void setMessage(Object message) {
		this.message = message;
	}
}
