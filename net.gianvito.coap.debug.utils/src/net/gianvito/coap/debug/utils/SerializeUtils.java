package net.gianvito.coap.debug.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.resources.LinkAttribute;

/**
 * Utility used to change
 * 
 * @author Gianvito Morena
 *
 */
public class SerializeUtils {
	
	/**
	 * Convert GeneralProperty to MiniProperty in a MultiMap.
	 * 
	 * @param oldRes
	 * @return
	 */
	public static HashMap<LinkAttribute, Set<MiniProperty>> serializeResources(HashMap<LinkAttribute, Set<GeneralProperty>> oldRes){
		HashMap<LinkAttribute, Set<MiniProperty>> result = new HashMap<LinkAttribute, Set<MiniProperty>>();
		
		for(LinkAttribute attribute : oldRes.keySet()){
			Set<MiniProperty> miniP = new HashSet<MiniProperty>();
			
			for(GeneralProperty property : oldRes.get(attribute)){
				miniP.add(new MiniProperty(property));
			}
			
			result.put(attribute, miniP);
		}
		return result;
	}
}
