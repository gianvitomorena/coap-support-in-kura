package net.gianvito.coap.debug.utils;

import java.io.Serializable;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.type.PropertyType;

public class MiniProperty implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 6417018398486394545L;
	
	private PropertyType type;
	private String path;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public MiniProperty(){
		path = new String();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param property
	 */
	public MiniProperty(GeneralProperty property){
		type = property.getType();
		
		switch(property.getType()){
			case DIRECT_RESOURCE:
				ResourceProperty propertyR = (ResourceProperty) property;
				this.path = propertyR.toString();
				break;
			case KURA_NODE:
				NodeProperty propertyNo = (NodeProperty) property;
				this.path = propertyNo.getNode().getCompletePath();
				break;
		}
	}
	
	/**
	 * Constructor.
	 * 
	 * @param type
	 * @param path
	 */
	public MiniProperty(PropertyType type, String path){
		this.type = type;
		this.path = path;
	}

	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public PropertyType getType() {
		return type;
	}
	public void setType(PropertyType type) {
		this.type = type;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
