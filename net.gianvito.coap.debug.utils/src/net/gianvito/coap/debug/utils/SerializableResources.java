package net.gianvito.coap.debug.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.resources.LinkAttribute;

/**
 * Optimized version of Google Guava's Multimap for debug software.
 * 
 * @author Gianvito Morena
 *
 */
public class SerializableResources implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 8607848989384273195L;
	
	private HashMap<LinkAttribute, Set<MiniProperty>> resources;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public SerializableResources(){
		resources = new HashMap<LinkAttribute, Set<MiniProperty>>();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param res
	 */
	public SerializableResources(HashMap<LinkAttribute, Set<GeneralProperty>> res){
		resources = SerializeUtils.serializeResources(res);
	}

	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public HashMap<LinkAttribute, Set<MiniProperty>> getResources() {
		return resources;
	}
	public void setResources(HashMap<LinkAttribute, Set<MiniProperty>> resources) {
		this.resources = resources;
	}
	
	
}
