package net.gianvito.coap.debug.utils;

/**
 * Operation typer for received-sent DebugMessage.
 * 
 * @author Gianvito Morena
 *
 */
public enum DebugMessageType {
	PARENT_TREE,
	DEVICES,
	LINKED_NODES,
	PROPERTIES
}
