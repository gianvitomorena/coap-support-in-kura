package net.gianvito.coap.debug.listener;

import net.gianvito.coap.debug.utils.DebugMessage;

/**
 * Listener to implements to debug CoAP broker.
 * 
 * @author Gianvito Morena
 *
 */
public interface CoAPDebugListener {
	void onLinkedNodeRequest(DebugMessage message);
	void onPropertyRequest(DebugMessage message);
}
