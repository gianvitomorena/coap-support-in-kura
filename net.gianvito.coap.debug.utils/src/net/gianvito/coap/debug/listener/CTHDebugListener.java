package net.gianvito.coap.debug.listener;

import net.gianvito.coap.debug.utils.DebugMessage;

public interface CTHDebugListener {
	void onTreeRequest(DebugMessage message);
	void onDevicesRequest(DebugMessage message);
}
