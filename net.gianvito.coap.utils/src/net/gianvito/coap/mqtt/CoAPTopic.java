package net.gianvito.coap.mqtt;

/**
 * Represents a topic used to exchange MQTT messages betweens CoAP brokers.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPTopic {
	// When a '/' keyword is present as name prefix, it means paths are device related.
	
	// CTH Publisher
	
	// CTH topics
 	public static final String CTH_ONLINE_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/cth/online";
    // Root topics
	public static final String ROOT_ADDED_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/root/added";
	public static final String ROOT_RESPONSE_TOPIC = "/coap/root/response";
	public static final String ROOT_NOT_AVAILABLE_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/root/not_available";
	// Node topics
	public static final String NODE_TOPIC_ADDED = CoAPKey.CONTROL_PREFIX + "coap/node/added";
    
    
    // CTH Subscriber - Broker Publisher
    
    // Root Topics
 	public static final String ROOT_REQUEST_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/root/request";
    public static final String ROOT_ON_NODE_UPDATED_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/node/root/updated";
    // Node Topics
    public static final String PARENT_REQUEST_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/parent/request";
    public static final String PARENT_ON_NODE_UPDATED_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/node/parent/updated";
    public static final String NODE_REMOVE_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/node/remove";
    
    // CTH Subscriber
    
    // MQTT/LWT Topic
    public static final String MQTT_LWT_PREFIX = CoAPKey.CONTROL_PREFIX + "MQTT/LWT";
    public static final String MQTT_LWT_TOPIC = CoAPKey.CONTROL_PREFIX + "MQTT/LWT/#";
    

    // Broker Publisher - Broker Subscriber
    
    // Resource topics
    public static final String RESOURCE_UPDATE_TOPIC = "/coap/resource/update";
    public static final String RESOURCE_QUERY_REQUEST_TOPIC = "/coap/resource/request";
    public static final String RESOURCE_QUERY_RESPONSE_TOPIC = "/coap/resource/response";
    
    // Broker Subscriber
         
    // Node topics
    public static final String NODE_ADDED_TOPIC = CoAPKey.CONTROL_PREFIX + "coap/node/added";
    
    // also CTH Publisher
    
    public static final String PARENT_RESPONSE_TOPIC = "/coap/parent/response";
    public static final String PARENT_AVAILABLE_TOPIC = "/coap/parent/available";
    public static final String PARENT_NOT_AVAILABLE_TOPIC = "/coap/parent/not_available";
    public static final String CHILD_NOT_AVAILABLE_TOPIC = "/coap/child/not_available";
  
    
    
}
