package net.gianvito.coap.mqtt;

/**
 * Static variables used in CoAP support classes.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPKey {
	
	
	// Root ID. Used in tree identification.
	public static final String ROOT_ID = "/root";
	// Resource Directory link
	public static final String RESOURCE_DIRECTORY_PATH = "rd";
	// RD-REMOTE name
	public static final String REMOTE_RESOURCE_NAME = "rd-remote";
	
	public static final String CONTROL_PREFIX = "$EDC/";
	
	// Debug levels
	public static final int LOW_DEBUG = 0;
	public static final int MEDIUM_DEBUG = 1;
	public static final int INTENSE_DEBUG = 2;
	
	
	public static final int COAP_DEFAULT_PORT = 5683;
	public static final int COAPS_DEFAULT_PORT = 5684;
}
