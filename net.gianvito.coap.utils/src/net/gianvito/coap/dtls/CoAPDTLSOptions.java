package net.gianvito.coap.dtls;

public class CoAPDTLSOptions {
	
	
	/* ----------------------- Attributes -------------------------- */
	
	
	private String keyStoreLocation = "/home/pi/certs/keyStore.jks";
	private String trustStoreLocation = "/home/pi/certs/trustStore.jks";
	
	private String PSKIdentity = "Client_identity";
	private byte[] PSKPassword = "secretPSK".getBytes();
	private String kSClientIdentity = "client";
	private String kSServerIdentity = "server";
	private String rootCertificateIdentity = "root";
	
	private char[] kSPassword = "endPass".toCharArray();
	private char[] tSPassword = "rootPass".toCharArray();
	
	
	/* ----------------------- Methods ----------------------------- */
	
	public CoAPDTLSOptions(String keyStoreLocation, String trustStoreLocation, String PSKIdentity,
			String kSClientIdentity, String kSServerIdentity, String rootCertificateIdentity,
			byte[] PSKPassword, char[] kSPassword, char[] tSPassword){
		
		this.keyStoreLocation = keyStoreLocation == null? this.keyStoreLocation : keyStoreLocation;
		this.trustStoreLocation = trustStoreLocation == null? this.trustStoreLocation : trustStoreLocation;
		this.PSKIdentity = PSKIdentity == null? this.PSKIdentity : PSKIdentity;
		this.kSClientIdentity = kSClientIdentity == null? this.kSClientIdentity : kSClientIdentity;
		this.kSServerIdentity = kSServerIdentity == null? this.kSServerIdentity : kSServerIdentity;
		this.rootCertificateIdentity = rootCertificateIdentity == null? this.rootCertificateIdentity : rootCertificateIdentity;
		this.PSKPassword = PSKPassword == null? this.PSKPassword : PSKPassword;
		this.kSPassword = kSPassword == null? this.kSPassword : kSPassword;
		this.tSPassword = tSPassword == null? this.tSPassword : tSPassword;
	}


	public char[] getkSPassword() {
		return kSPassword;
	}


	public void setkSPassword(char[] kSPassword) {
		this.kSPassword = kSPassword;
	}


	public char[] gettSPassword() {
		return tSPassword;
	}


	public void settSPassword(char[] tSPassword) {
		this.tSPassword = tSPassword;
	}


	public String getKeyStoreLocation() {
		return keyStoreLocation;
	}


	public void setKeyStoreLocation(String keyStoreLocation) {
		this.keyStoreLocation = keyStoreLocation;
	}


	public String getTrustStoreLocation() {
		return trustStoreLocation;
	}


	public void setTrustStoreLocation(String trustStoreLocation) {
		this.trustStoreLocation = trustStoreLocation;
	}


	public String getPSKIdentity() {
		return PSKIdentity;
	}


	public void setPSKIdentity(String pSKIdentity) {
		PSKIdentity = pSKIdentity;
	}


	public byte[] getPSKPassword() {
		return PSKPassword;
	}


	public void setPSKPassword(byte[] pSKPassword) {
		PSKPassword = pSKPassword;
	}


	public String getkSClientIdentity() {
		return kSClientIdentity;
	}


	public void setkSClientIdentity(String kSClientIdentity) {
		this.kSClientIdentity = kSClientIdentity;
	}


	public String getkSServerIdentity() {
		return kSServerIdentity;
	}


	public void setkSServerIdentity(String kSServerIdentity) {
		this.kSServerIdentity = kSServerIdentity;
	}


	public String getRootCertificateIdentity() {
		return rootCertificateIdentity;
	}


	public void setRootCertificateIdentity(String rootCertificateIdentity) {
		this.rootCertificateIdentity = rootCertificateIdentity;
	}
	
}
