package net.gianvito.coap.dtls;

import java.io.IOException;

import org.eclipse.californium.core.network.CoAPEndpoint;
import org.eclipse.californium.core.network.Endpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.elements.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoAPDTLSEndpoint {
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(CoAPDTLSEndpoint.class);
	
	
	private Endpoint secureEndpoint;
	
	
	/* ----------------------- Methods ----------------------------- */
	/**
	 * Constructor.
	 * 
	 * @param dtlsConnector
	 */
	public CoAPDTLSEndpoint(Connector dtlsConnector){
		secureEndpoint = new CoAPEndpoint(dtlsConnector, NetworkConfig.getStandard());
		//EndpointManager.getEndpointManager().setDefaultSecureEndpoint(secureEndpoint);
		startEndpoint();
	}
	
	/**
	 * Start a secure endpoint.
	 */
	public void startEndpoint(){
		try {
			secureEndpoint.start();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Stop a secure endpoint.
	 */
	public void stopEndpoint(){
		secureEndpoint.stop();
	}

	
	
	public Endpoint getSecureEndpoint() {
		return secureEndpoint;
	}
	public void setSecureEndpoint(Endpoint secureEndpoint) {
		this.secureEndpoint = secureEndpoint;
	}
	
	
}
