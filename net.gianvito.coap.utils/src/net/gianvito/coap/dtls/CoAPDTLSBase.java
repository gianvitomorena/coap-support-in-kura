package net.gianvito.coap.dtls;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.logging.Level;

import org.eclipse.californium.elements.Connector;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.ScandiumLogger;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoAPDTLSBase {
	
	static {
		ScandiumLogger.initialize();
		ScandiumLogger.setLevel(Level.SEVERE);
	}
	
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(CoAPDTLSBase.class);

	private KeyStore keyStore, trustStore;
	private Certificate[] trustedCertificates;
	private Connector dtlsConnector;
	//private Endpoint secureEndpoint;
	private int connectorPort = 0;
	
	private CoAPDTLSOptions dtlsOptions;	
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 * 
	 * @param dtlsOptions
	 */
	public CoAPDTLSBase(CoAPDTLSOptions dtlsOptions){		
			
		this.dtlsOptions = dtlsOptions;
		
		try {			
			loadStores();
	    } catch (GeneralSecurityException | IOException e) {
	    	logger.error(e.getMessage(), e);
        }
	}
	
	/**
	 * Load keyStore and trustStore.
	 * 
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	public void loadStores() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException{
		// load key store
        keyStore = KeyStore.getInstance("JKS");
        InputStream in = new FileInputStream(dtlsOptions.getKeyStoreLocation());
        keyStore.load(in, dtlsOptions.getkSPassword());
            
        // load trust store
        trustStore = KeyStore.getInstance("JKS");
        InputStream inTrust = new FileInputStream(dtlsOptions.getTrustStoreLocation());
        trustStore.load(inTrust, dtlsOptions.gettSPassword());
                    
        // You can load multiple certificates if needed
        trustedCertificates = new Certificate[1];
        trustedCertificates[0] = trustStore.getCertificate(dtlsOptions.getRootCertificateIdentity());
	}
	
	/**
	 * Create a DTLS connector.
	 * 
	 * @param connectorPort
	 */
	public void createDTLSConnector(int connectorPort){
		this.connectorPort = connectorPort < 0? 0 : connectorPort;
		
		try {
			// TODO Check if previous connector has been closed.
			// DTLS connector store configurations.
			dtlsConnector = new DTLSConnector(new InetSocketAddress(this.connectorPort), trustedCertificates);
			
			
			((DTLSConnector) dtlsConnector).getConfig().setPskStore(new StaticPskStore(
					dtlsOptions.getPSKIdentity(),
					dtlsOptions.getPSKPassword()));
		
			((DTLSConnector) dtlsConnector).getConfig().setPrivateKey(
					(PrivateKey)keyStore.getKey(dtlsOptions.getkSClientIdentity(), dtlsOptions.getkSPassword()),
					keyStore.getCertificateChain(dtlsOptions.getkSClientIdentity()),
					true);
			
			// DTLS endpoint configuration.
			//secureEndpoint = new CoAPEndpoint(dtlsConnector, NetworkConfig.getStandard());
			//EndpointManager.getEndpointManager().setDefaultSecureEndpoint(secureEndpoint);
			//secureEndpoint.start();

		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			logger.error(e.getMessage(), e);
		}
	}

	
	public Connector getDtlsConnector() {
		return dtlsConnector;
	}
	public void setDtlsConnector(Connector dtlsConnector) {
		this.dtlsConnector = dtlsConnector;
	}

}
