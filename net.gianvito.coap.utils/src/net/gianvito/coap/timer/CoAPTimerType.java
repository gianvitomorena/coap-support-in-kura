package net.gianvito.coap.timer;

/**
 * Timer type used in the CoAP broker.
 * 
 * @author Gianvito Morena
 *
 */
public enum CoAPTimerType {
	CONNECT_TO_TREE,
	EMERGENCY_PROCEDURE
}
