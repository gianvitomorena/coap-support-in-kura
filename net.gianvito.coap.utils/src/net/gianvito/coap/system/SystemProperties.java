package net.gianvito.coap.system;

import java.util.List;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.core.net.util.NetworkUtil;
import org.eclipse.kura.net.NetInterface;
import org.eclipse.kura.net.NetInterfaceAddress;
import org.eclipse.kura.net.NetworkService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Allows to retrieve system information for connection between brokers.
 * Uses an internal Kura Service: NetworkService.
 * 
 * @author Gianvito Morena
 *
 */
public class SystemProperties {
	
	/* ----------------------- Attributes -------------------------- */
	
	// Logger
	private static final Logger logger = LoggerFactory.getLogger(SystemProperties.class);
	
	// NetworkService
	private NetworkService networkService;
	// SystemService
	private Integer coapPort;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public SystemProperties(){}
	
	/**
	 * Constructor. Used only if we want to obtain addresses.
	 * 
	 * @param networkService
	 */
	public SystemProperties(NetworkService networkService){
		this.networkService = networkService;
		this.coapPort = 5683;
	}
	
	/**
	 * Constructor. CoAP dependent.
	 * 
	 * @param networkService
	 */
	public SystemProperties(NetworkService networkService, Integer coapPort){
		this.networkService = networkService;
		this.coapPort = coapPort;
	}
	
	/**
	 * Activate method.
	 * 
	 * @param componentContext
	 */
	protected void activate(ComponentContext componentContext){
		//getInterfacesInfo();
	}
	
	/**
	 * Deactivate method.
	 * 
	 * @param componentContext
	 */
	protected void deactivate(ComponentContext componentContext){
		
	}
	
	/**
	 * Returns current IP Address.
	 * 
	 * @param ipType
	 * @return
	 */
	public String getCurrentIPAddress(IPType ipType, String interfaceName){
		String address = "";
		
		try {
			// Used when there is no interfaceName passed.
			if(interfaceName == null || interfaceName.equals("")){
				// We must find wlan0 properties because Kura has loopback interface as default one.
				address = searchAddressOnInterface(ipType, "wlan0");
				// Search on Ethernet if nothing is found for WiFi dongle.
				if(address.equals("")){
					address = searchAddressOnInterface(ipType, "eth0");
				}
			}else{
				address = searchAddressOnInterface(ipType, interfaceName);
			}
		} catch (KuraException e) {
			e.printStackTrace();
		}
		
		
		return address;
	}
	
	/**
	 * Returns current MAC address.
	 * 
	 * @return
	 */
	public String getMAC(String interfaceName){
		String address = "";
		
		try {
			
			if(networkService != null){
				if(interfaceName == null || interfaceName.equals("")){
					// Search on the WiFi dongle
					address = searchMACOnInterface("wlan0");
					// Search on Ethernet
					if(address.equals("")){
						address = searchMACOnInterface("eth0");
					}
				}else{
					address = searchMACOnInterface(interfaceName);
				}
			}
			
		} catch (KuraException e) {
			e.printStackTrace();
		}
		
		return address;
	}
	
	/**
	 * Returns current CoAP server port.
	 * 
	 * @return
	 */
	public Integer getCoAPPort(){
		return this.coapPort;
	}
	
	/**
	 * Prints all the info about the local network interfaces.
	 */
	public void getInterfacesInfo(){
		try {
			//address = networkService.getActiveNetworkInterfaces().get(1).getHardwareAddress().toString();
			List<NetInterface<? extends NetInterfaceAddress>> interfaces = networkService.getActiveNetworkInterfaces();
			for(NetInterface<?> singleInterface : interfaces){
				logger.info("Interface name --> " + singleInterface.getName());
				byte[] byteMAC = singleInterface.getHardwareAddress();
				logger.info("MAC address --> " + NetworkUtil.macToString(byteMAC));
				for(NetInterfaceAddress byteIP : singleInterface.getNetInterfaceAddresses()){
					logger.info("Address --> " + byteIP.getAddress().getHostAddress());
				}
			}
		} catch (KuraException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Return network address on specific interface.
	 * 
	 * @param interfaceName
	 * @return
	 * @throws KuraException
	 */
	public String searchAddressOnInterface(IPType ipType, String interfaceName) throws KuraException{
		String address = "";
		
		List<NetInterface<? extends NetInterfaceAddress>> interfaces = networkService.getActiveNetworkInterfaces();
		for(NetInterface<?> singleInterface : interfaces){
			if(singleInterface.getName().equals(interfaceName)){
				switch(ipType){
					case IPv4:
						if(singleInterface.getNetInterfaceAddresses().size() > 1){
							NetInterfaceAddress byteIPv4 = (NetInterfaceAddress) singleInterface.getNetInterfaceAddresses().get(1);
							address = byteIPv4.getAddress().getHostAddress();
						}else{
							NetInterfaceAddress byteIPv4 = (NetInterfaceAddress) singleInterface.getNetInterfaceAddresses().get(0);
							address = byteIPv4.getAddress().getHostAddress();
						}
						break;
					case IPv6:
						NetInterfaceAddress byteIPv6 = (NetInterfaceAddress) singleInterface.getNetInterfaceAddresses().get(0);
						address = byteIPv6.getAddress().getHostAddress();
						break;
				}
			}
		}
		
		return address;
	}
	
	/**
	 * Return MAC address on specific interface.
	 * 
	 * @param interfaceName
	 * @return
	 * @throws KuraException
	 */
	public String searchMACOnInterface(String interfaceName) throws KuraException{
		String address = "";
		
		List<NetInterface<? extends NetInterfaceAddress>> interfaces = networkService.getActiveNetworkInterfaces();
		for(NetInterface<?> singleInterface : interfaces){
			if(singleInterface.getName().equals(interfaceName)){
				byte[] byteMAC = singleInterface.getHardwareAddress();
				address = NetworkUtil.macToString(byteMAC);
			}
		}
		
		return address;
	}
	
	
	/* ------------------------------------------
	 * --- NetworkService handlers ---
	 * ------------------------------------------ */
	protected void setNetworkService(NetworkService networkService){
		this.networkService = networkService;
	}
	protected void unsetNetworkService(NetworkService networkService){
		this.networkService = null;
	}
}
