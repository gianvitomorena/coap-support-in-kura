package net.gianvito.coap.system;

/**
 * IP Enum specifying used IP type for connections.
 * 
 * @author Gianvito Morena
 *
 */
public enum IPType {
	IPv4,
	IPv6
}
