package net.gianvito.coap.message;

import java.io.Serializable;
import java.util.Date;

import net.gianvito.coap.type.CoAPOperation;

/**
 * Message implementation used in all the interactions between Kura nodes and CTH.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPMessage implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -541337271167302144L;
	
	private long serialNumber;
	private KuraNode sender;
	private Object message;
	private CoAPOperation operation;
	private Date creationDate;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public CoAPMessage(){
		serialNumber = 0L;
		sender = new KuraNode();
		message = null;
		operation = null;
		creationDate = new Date();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param serialNumber
	 * @param sender
	 * @param message
	 * @param operation
	 */
	public CoAPMessage(long serialNumber, KuraNode sender, Object message, CoAPOperation operation){
		this.serialNumber = serialNumber;
		this.sender = sender;
		this.message = message;
		this.operation = operation;
		this.creationDate = new Date();
	}
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public long getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(long serialNumber) {
		this.serialNumber = serialNumber;
	}
	public KuraNode getSender() {
		return sender;
	}
	public void setSender(KuraNode sender) {
		this.sender = sender;
	}
	public Object getMessage() {
		return message;
	}
	public void setMessage(Object message) {
		this.message = message;
	}
	public CoAPOperation getOperation() {
		return operation;
	}
	public void setOperation(CoAPOperation operation) {
		this.operation = operation;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
