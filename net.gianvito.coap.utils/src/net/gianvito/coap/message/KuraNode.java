package net.gianvito.coap.message;

import java.io.Serializable;

import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.system.IPType;
import net.gianvito.coap.system.SystemProperties;
import net.gianvito.coap.utils.PathUtils;

/**
 * Object which describes all the properties of the local broker CoAP.
 * 
 * @author Gianvito Morena
 *
 */
public class KuraNode implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -7828664276227157266L;
	
	private String kuraAddress;
	private Integer kuraPort;
	private String interfaceName;
	private KuraNode parentNode;
	private String domain;
	private String group;
	private String macAddress;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 * 
	 * Create a local KuraNode. IP is the only property set.
	 */
	public KuraNode(){
		this.kuraAddress = "127.0.0.1";
		this.interfaceName = "lo0";
		this.kuraPort = CoAPKey.COAP_DEFAULT_PORT;
		this.parentNode = null;
		this.domain = "";
		this.group = "";
		this.macAddress = "";
	}
	
	/**
	 * Constructor.
	 * 
	 * @param properties
	 * @param parentNode
	 * @param domain
	 * @param group
	 */
	public KuraNode(SystemProperties properties, String interfaceName,
						Integer coapPort, KuraNode parentNode, String domain, String group){
		this.kuraAddress = properties.getCurrentIPAddress(IPType.IPv4, interfaceName);
		this.interfaceName = interfaceName;
		this.kuraPort = coapPort == null? 0 : coapPort;
		this.parentNode = parentNode;
		checkDomainGroup(domain, group);
		this.macAddress = properties.getMAC(interfaceName);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param properties
	 * @param parentNode
	 * @param domain
	 * @param group
	 */
	public KuraNode(String IP, String networkInterface, String MAC, Integer coapPort, KuraNode parentNode, String domain, String group){
		this.kuraAddress = IP;
		this.interfaceName = networkInterface;
		this.kuraPort = coapPort;
		this.parentNode = parentNode;
		checkDomainGroup(domain, group);
		this.macAddress = MAC;
	}
	
	/**
	 * 
	 * 
	 * @param domain
	 * @param group
	 */
	private void checkDomainGroup(String domain, String group){
		this.domain = domain == null? "*": domain;
		this.group = group == null? "*": group;
	}
	
	/**
	 * Return KuraNode hierarchy level.
	 * 
	 * @return
	 */
	public int getLevel(){
		if(this.isCurrentlySet()){
			if(this.domain.equals("*") && this.group.equals("*")){
				return 0;
			}else if(!this.domain.equals("*") && this.group.equals("*")){
				return 1;
			}else if(!this.domain.equals("*") && !this.group.equals("*")){
				return 2;
			}else{
				return -1;
			}
		}else{
			return -1;
		}
	}
	
	/**
	 * Return true if both network address and MAC are set.
	 * 
	 * @return
	 */
	public Boolean isCurrentlySet(){
		if(!this.kuraAddress.equals("") && !this.macAddress.equals("")){
			return true;
		}else{
			return false;
		}
	}
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public String getKuraAddress() {
		return kuraAddress;
	}
	public void setKuraAddress(String kuraAddress) {
		this.kuraAddress = kuraAddress;
	}
	public KuraNode getParentNode() {
		return parentNode;
	}
	public void setParentNode(KuraNode parentNode) {
		this.parentNode = parentNode;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	public String getRelativePath(){
		return PathUtils.buildRelativeNodePath(domain, group);
	}
	
	public String getCompletePath(){
		return PathUtils.buildCompleteNodePath(this);
	}
	public int getKuraPort() {
		return kuraPort;
	}
	public void setKuraPort(int kuraPort) {
		this.kuraPort = kuraPort;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/* ------------------------------------------
	 * --- Override methods ---
	 * ------------------------------------------ */
	@Override
    public boolean equals(Object obj) {
		if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final KuraNode other = (KuraNode) obj;
        boolean sameKuraAddress = (this.kuraAddress != null && this.kuraAddress.equalsIgnoreCase(other.kuraAddress));
        if (!sameKuraAddress) return false;
        boolean sameKuraPort = (this.kuraPort == other.kuraPort) || (this.kuraPort != null && this.kuraPort.equals(other.kuraPort));
        if (!sameKuraPort) return false;
        boolean sameDomain = (this.domain != null && this.domain.equalsIgnoreCase(other.domain));
        if (!sameDomain) return false;
        boolean sameGroup = (this.group != null && this.group.equalsIgnoreCase(other.group));
        if (!sameGroup) return false;
        boolean sameMacAddress = (this.macAddress != null && this.macAddress.equalsIgnoreCase(other.macAddress));
        if (!sameMacAddress) return false;
        
        return true;
	}
	
	@Override
	public int hashCode(){
		return kuraAddress.hashCode() * kuraPort.hashCode() * domain.hashCode() * group.hashCode() * macAddress.hashCode();	
	}
	
	@Override
	public String toString(){
		return getCompletePath();
	}
	
}
