package net.gianvito.coap.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CoAPGroup implements Serializable{
	private static final long serialVersionUID = 5869507921181616606L;
	private List<String> group;
	
	public CoAPGroup(){
		this.group = new ArrayList<String>();
	}
	
	public CoAPGroup(String group){
		this.group = new ArrayList<String>();
		this.group = parseGroupToList(group);
	}
	
	private List<String> parseGroupToList(String group){
		ArrayList<String> tempGroup = new ArrayList<String>();
		
		return tempGroup;
		
	}
	
	/**
	 * Used when checking possible matched for resources on advanced AttributeCollection
	 * 
	 * @param parentGroup
	 * @param childGroup
	 * @return
	 */
	public Boolean checkGroupHierarchy(CoAPGroup parentGroup, CoAPGroup childGroup){
		Boolean result = null;
		
		return result;
	}
	
	/**
	 * Remove n levels from group
	 * 
	 * @param levelNumber
	 * @return new Group
	 */
	public CoAPGroup removeLevel(Integer levelNumber){
		CoAPGroup tempGroup = this;
		
		
		
		return tempGroup;
	}
	
	public List<String> getGroup() {
		return group;
	}
	public void setGroup(List<String> group) {
		this.group = group;
	}
}
