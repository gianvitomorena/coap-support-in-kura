package net.gianvito.coap.message;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.server.AttributeCollection;

/**
 * Object used in the propagation of the query to include all the required properties:
 *    - Original query
 *    - Attributes to search
 *    - Nodes to query
 * 
 * @author Gianvito Morena
 *
 */
public class PropagationObject implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -3051731873684340580L;
	
	CoAPMessage originalQuery;
	AttributeCollection attributes;
	Set<NodeProperty> nodeProperties;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public PropagationObject(){
		this.originalQuery = new CoAPMessage();
		this.attributes = new AttributeCollection();
		this.nodeProperties = new HashSet<NodeProperty>();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param originalQuery
	 * @param attributes
	 * @param nodeProperties
	 */
	public PropagationObject(CoAPMessage originalQuery, AttributeCollection attributes, Set<NodeProperty> nodeProperties){
		this.originalQuery = originalQuery;
		this.attributes = attributes;
		this.nodeProperties = nodeProperties;
	}

	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public CoAPMessage getOriginalQuery() {
		return originalQuery;
	}
	public void setOriginalQuery(CoAPMessage originalQuery) {
		this.originalQuery = originalQuery;
	}
	public AttributeCollection getAttributes() {
		return attributes;
	}
	public void setAttributes(AttributeCollection attributes) {
		this.attributes = attributes;
	}
	public Set<NodeProperty> getNodeProperties() {
		return nodeProperties;
	}
	public void setNodeProperties(Set<NodeProperty> nodeProperties) {
		this.nodeProperties = nodeProperties;
	}
}
