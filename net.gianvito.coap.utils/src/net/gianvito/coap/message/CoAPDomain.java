package net.gianvito.coap.message;

import java.io.Serializable;

public class CoAPDomain implements Serializable{
	private static final long serialVersionUID = 1612982369623030395L;
	private String domain;
	
	public CoAPDomain(){
		this.domain = "*";
	}
	
	public CoAPDomain(String domain){
		this.domain = domain;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	
}
