package net.gianvito.coap.message;

public class CoAPPath {
	private String path;
	
	public CoAPPath(){
		path = new String();
	}
	
	/*
	 * Helper for the removing of a level on the MQTT path
	 * Example --> /gianvito.net/bologna --> /gianvito.net
	 */
	public CoAPPath removeLevel(){
		String pathSeparator = "/";
		
		int sep = this.path.lastIndexOf(pathSeparator);
        this.path = this.path.substring(0, sep);
		
		return this;
	}
	
	/*
	 * Helper to the building of the complete MQTT path
	 * Example --> 192.168.1.1/gianvito.net/bologna
	 */
	public CoAPPath buildCompleteNodePath(KuraNode destinationNode){
		path = new String();
		
		path += destinationNode.getKuraAddress();
		path += "/" + destinationNode.getMacAddress();
		path += buildRelativeNodePath(destinationNode.getDomain(), destinationNode.getGroup());
		
		return this;
	}
	
	/*
	 * Helper to the building of the relative MQTT path
	 * Example --> /gianvito.net/bologna
	 */
	public CoAPPath buildRelativeNodePath(String domain, String group){
		path = new String();
		
		if(domain != null && !domain.equals("*")){
			path += "/" + domain;
		}
		if(group != null && !domain.equals("*")){
			path += "/" + group;
		}
		return this;
	}

	public CoAPPath getPath() {
		return this;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
