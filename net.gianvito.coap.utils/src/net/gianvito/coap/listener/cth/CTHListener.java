package net.gianvito.coap.listener.cth;

import net.gianvito.coap.exception.CoAPConfigurationException;
import net.gianvito.coap.message.CoAPMessage;

/**
 * Interface to implement to handle CoAPMessages directed to CTH.
 * 
 * @author Gianvito Morena
 *
 */
public interface CTHListener {
	Boolean onRootRequest(CoAPMessage message) throws CoAPConfigurationException;
	Boolean onParentRequest(CoAPMessage message);
	Boolean onRootRemove(CoAPMessage message);
	Boolean onNodeAdd(CoAPMessage message);
	Boolean onNodeRemove(CoAPMessage message);
	Boolean onRootUpdateOnNode(CoAPMessage message);
	Boolean onParentUpdateOnNode(CoAPMessage message);
}
