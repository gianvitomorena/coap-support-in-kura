package net.gianvito.coap.listener.server;

import net.gianvito.coap.message.CoAPMessage;

/**
 * It contains all the callback methods used by local Kura node
 * to interact with CTH.
 * 
 * @author Gianvito Morena
 *
 */
public interface CoAPBrokerListener {
	
	void onAddedRoot(CoAPMessage message);
	void onAddedNode(CoAPMessage message);
	void onRemovedNode(CoAPMessage message);
	void onNotAvailableRoot(CoAPMessage message);
	void onParentResponse(CoAPMessage message);
	void onResourceQueryRequest(CoAPMessage message);
	void onAvailableParent(CoAPMessage message);
	void onNotAvailableParent(CoAPMessage message);
	void onNotAvailableChild(CoAPMessage message);
	void onCTHConnected(CoAPMessage message);
}
