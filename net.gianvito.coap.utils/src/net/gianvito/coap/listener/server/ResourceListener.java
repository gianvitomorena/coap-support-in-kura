package net.gianvito.coap.listener.server;

import net.gianvito.coap.message.CoAPMessage;

/**
 * Contains all the methods used to handle query requests and resource updates.
 * 
 * @author Gianvito Morena
 *
 */
public interface ResourceListener {
	void onResourceUpdate(CoAPMessage message);
	void onResourceQueryRequest(CoAPMessage message);
	void onQueryResponseReceived(CoAPMessage message);
}
