package net.gianvito.coap.listener.server;

import net.gianvito.coap.message.KuraNode;

// Useless
public interface CoAPServerListener {
	void onDomainAndGroupChange(String domain, String group);
	void onNodeChange(KuraNode node);
}
