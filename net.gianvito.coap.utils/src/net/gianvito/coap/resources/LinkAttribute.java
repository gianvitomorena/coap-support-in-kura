/*******************************************************************************
 * Copyright (c) 2014 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 ******************************************************************************/
package net.gianvito.coap.resources;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;


/**
 * This class implements attributes of the CoRE Link Format.
 */
public class LinkAttribute implements Serializable{

// Constants ///////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = -3757040689953855239L;
	public static final Pattern SEPARATOR      = Pattern.compile("\\s*;+\\s*");
	public static final Pattern WORD           = Pattern.compile("\\w+");
	public static final Pattern QUOTED_STRING  = Pattern.compile("\\G\".*?\"");
	public static final Pattern CARDINAL       = Pattern.compile("\\G\\d+");
	
// Members /////////////////////////////////////////////////////////////////////
	
	private String name;
	private String value;

// Constructors ////////////////////////////////////////////////////////////////
	
	public LinkAttribute() {
		
	}
	
	public LinkAttribute(String name, String value) {
		this.name = name;
		this.value = value;
	}
	public LinkAttribute(String name, int value) {
		this.name = name;
		this.value = Integer.valueOf(value).toString();
	}
	public LinkAttribute(String name) {
		this.name = name;
		this.value = "";
	}

// Serialization ///////////////////////////////////////////////////////////////
	
	public static LinkAttribute parse(String str) {
		return parse(new Scanner(str));
	}

	public static LinkAttribute parse2(String str) {
		LinkAttribute attr = new LinkAttribute();
		
		Iterator<String> string = Splitter.on('=').trimResults(CharMatcher.is('\"')).split(str).iterator();
		
		if(string.hasNext()){
			attr.name = string.next();
			
			if(string.hasNext()){
				attr.value = string.next();
			}else{
				attr.value = "";
			}
			
			return attr;
		}
		
		return null;

	}
	
	public static LinkAttribute parse3(String str) {
		LinkAttribute attr = new LinkAttribute();
		
		Iterator<String> string = Splitter.on('=').split(str).iterator();
		
		if(string.hasNext()){
			attr.name = string.next();
			
			if(string.hasNext()){
				StringBuilder value = new StringBuilder(string.next());
				if(value.indexOf("\"") != -1)
					attr.value = value.substring(1, value.length() - 1);
				else
					attr.value = value.toString();
			}else{
				attr.value = "";
			}
			
			return attr;
		}
		
		return null;

	}
	
	public static LinkAttribute parse4(String str) {
		LinkAttribute attr = null;
		int equalSep = str.indexOf('=');
		
		if(equalSep != -1){
			attr = new LinkAttribute();
			attr.name = str.substring(0, equalSep);
			if(str.charAt(equalSep + 1) == '"'){
				attr.value = str.substring(equalSep + 2, str.lastIndexOf('"'));
			}else{
				attr.value = str.substring(equalSep + 1);
			}
		}
		
		return attr;
	}
	
	public static LinkAttribute parse(Scanner scanner) {
		
		String name = scanner.findInLine(WORD);
		if (name != null) {
			
			LinkAttribute attr = new LinkAttribute();
			attr.name = name;
			
			// check for name-value-pair
			if (scanner.findWithinHorizon("=", 1) != null) {
				
				String value = null;
				if ((value = scanner.findInLine(QUOTED_STRING)) != null) {
					attr.value = value.substring(1, value.length()-1); // trim " "
				} else if ((value = scanner.findInLine(WORD)) != null) {
					attr.value = value;
				} else if ((value = scanner.findInLine(CARDINAL)) != null) {
					attr.value = value;
				} else if (scanner.hasNext()) {
					attr.value = scanner.next();
					throw new RuntimeException("LinkAttribute scanner.next()");
				}
				
			} else {
				// flag attribute
				attr.value = "";
			}
			
			return attr;
		}
		return null;
	}
	
	public String getName() {
		return name;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getIntValue() {
		return Integer.parseInt(value);
	}
	
	// Added
	public void setValue(String value){
		this.value = value;
	}
	
	
	// Code modified
	@Override
    public boolean equals(Object obj) {
		if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final LinkAttribute other = (LinkAttribute) obj;
        boolean sameName = (this.name != null && this.name.equalsIgnoreCase(other.name));
        if (!sameName) return false;
        boolean sameValue = (this.value != null && this.value.equalsIgnoreCase(other.value));
        if (!sameValue) return false;
        
        return true;
	}
	
	@Override
	public int hashCode(){
		return name.hashCode() * value.hashCode();	
	}
	
}
