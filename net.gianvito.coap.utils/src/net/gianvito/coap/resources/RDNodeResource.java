/*******************************************************************************
 * Copyright (c) 2014 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 ******************************************************************************/
package net.gianvito.coap.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
//import java.util.logging.Logger;




import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.utils.StringCoAPUtils;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.Resource;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;


public class RDNodeResource extends CoapResource {

	//private static final  java.util.logging.Logger LOGGER = Logger.getLogger(RDNodeResource.class.getCanonicalName());
	
	//private static final Logger logger = LoggerFactory.getLogger(RDNodeResource.class);
		
	/*
	 * After the lifetime expires, the endpoint has RD_VALIDATION_TIMEOUT seconds
	 * to update its entry before the RD enforces validation and removes the endpoint
	 * if it does not respond.
	 */
	private int lifeTime;
	private Timer lifetimeTimer;
	private ScheduledExecutorService executorTimer;
	private ScheduledFuture<?> timerHandler;
	private DateTime expirationDate;
	
	
	
	private String endpointIdentifier;
	private String domain;
	private String endpointType;
	private String context;
	
	public RDNodeResource(String endpointID, String domain) {
		super(endpointID);		
		this.endpointIdentifier = endpointID;
		this.domain = domain;
		//executorTimer = Executors.newSingleThreadScheduledExecutor();
	}

	/**
	 * Updates the endpoint parameters from POST and PUT requests.
	 * 
	 * @param request A POST or PUT request with a {?et,lt,con} URI Template query
	 * 			and a Link Format payload.
	 * 
	 */
	public boolean setParameters(Request request) {		
		LinkAttribute attr;
		
		int newLifeTime = 86400;
		String newContext = "";
		
		/*
		 * get lifetime from option query - only for PUT request.
		 */
		List<String> query = request.getOptions().getURIQueries();
		for (String q : query) {
			// FIXME Do not use Link attributes for URI template variables
			attr = LinkAttribute.parse(q);
			
			if (attr.getName().equals(LinkFormat.LIFE_TIME)) {
				newLifeTime = attr.getIntValue();
				
				if (newLifeTime < 60) {
					//LOGGER.info("Enforcing minimal RD lifetime of 60 seconds (was "+newLifeTime+")");
					newLifeTime = 60;
				}
			}
			
			if (attr.getName().equals(LinkFormat.CONTEXT)){
				newContext = attr.getValue();
			}
		}
		
		setLifeTime(newLifeTime);
		
		try {
			URI check;
			if (newContext.equals("")) {
				check = new URI("coap", "", request.getSource().getHostName(), request.getSourcePort(), "", "", ""); // required to set port
				context = check.toString().replace("@", "").replace("?", "").replace("#", ""); // URI is a silly class
			} else {
				check = new URI(context);
			}
		} catch (Exception e) {
			LOGGER.warning(e.toString());
			return false;
		}
		
		return updateEndpointResources(request.getPayloadString());
	}
	
	public boolean setParameters2(Request request) {				
		int newLifeTime = 86400;
		String newContext = null;
		
		/*
		 * get lifetime from option query - only for PUT request.
		 */
		List<String> query = request.getOptions().getURIQueries();
		String qTemp;
		for (String q : query) {
			// FIXME Do not use Link attributes for URI template variables
			qTemp = StringCoAPUtils.parseName(q);
			
			if (qTemp.equals(LinkFormat.LIFE_TIME)) {
				newLifeTime = StringCoAPUtils.parseIntValue(q);
				
				if (newLifeTime < 60) {
					//LOGGER.info("Enforcing minimal RD lifetime of 60 seconds (was "+newLifeTime+")");
					newLifeTime = 60;
				}
			}
			
			if (qTemp.equals(LinkFormat.CONTEXT)){
				newContext = StringCoAPUtils.parseValue(q);
				// My add
				context = newContext;
			}
		}
		
		setLifeTime2(newLifeTime);
		
		// If context isn't set.
		if (newContext == null || newContext.length() == 0){
			context = request.getScheme() + "://" + request.getSource().getHostName() + ":" + CoAPKey.COAP_DEFAULT_PORT;
		}
		
		return updateEndpointResources3(request.getPayloadString());
	}
	
	public boolean setParameters3(Request request, int port) {				
		int newLifeTime = 86400;
		//int newLifeTime = 60;
		String newContext = null;
		
		/*
		 * get lifetime from option query - only for PUT request.
		 */
		List<String> query = request.getOptions().getURIQueries();
		String qTemp;
		for (String q : query) {
			qTemp = StringCoAPUtils.parseName(q);
			
			if (qTemp.equals(LinkFormat.LIFE_TIME)) {
				newLifeTime = StringCoAPUtils.parseIntValue(q);
				
				if (newLifeTime < 60) {
					//LOGGER.info("Enforcing minimal RD lifetime of 60 seconds (was "+newLifeTime+")");
					newLifeTime = 60;
				}
			}
			
			if (qTemp.equals(LinkFormat.CONTEXT)){
				newContext = StringCoAPUtils.parseValue(q);
				// My add
				context = newContext;
			}
		}
		
		setExpirationDate(newLifeTime);
		
		// If context isn't set.
		if (newContext == null || newContext.length() == 0){
			//context = request.getScheme() + "://" + request.getSource().getHostName() + ":" + CoAPKey.COAP_DEFAULT_PORT;
			context = (request.getScheme() == null? "coap" : request.getScheme())
					+ "://" + request.getSource().getHostName()
					+ ":" + port;
		}
		
		return updateEndpointResources3(request.getPayloadString());
	}

	/*
	 * add a new resource to the node. E.g. the resource temperature or
	 * humidity. If the path is /readings/temp, temp will be a subResource
	 * of readings, which is a subResource of the node.
	 */
	public CoapResource addNodeResource(String path) {
		Scanner scanner = new Scanner(path);
		scanner.useDelimiter("/");
		String next = "";
		boolean resourceExist = false;
		Resource resource = this; // It's the resource that represents the endpoint
		
		CoapResource subResource = null;
		while (scanner.hasNext()) {
			resourceExist = false;
			next = scanner.next();
			for (Resource res : resource.getChildren()) {
				if (res.getName().equals(next)) {
					subResource = (CoapResource) res;
					resourceExist = true;
				}
			}
			if (!resourceExist) {
				subResource = new RDTagResource(next,true, this);
				resource.add(subResource);
			}
			resource = subResource;
		}
		subResource.setPath(resource.getPath());
		subResource.setName(next);
		scanner.close();
		return subResource;
	}
	
	
	public CoapResource addNodeResource2(String path) {
		//long startNodeR = System.currentTimeMillis();
		
		//Scanner scanner = new Scanner(path);
		//scanner.useDelimiter("/");
		Iterator<String> pathIt = Splitter.on('/').split(path).iterator();
		String next = "";
		boolean resourceExist = false;
		Resource resource = this; // It's the resource that represents the endpoint
		
		CoapResource subResource = null;
		while (pathIt.hasNext()) {
			resourceExist = false;
			next = pathIt.next();
			for (Resource res : resource.getChildren()) {
				if (res.getName().equals(next)) {
					subResource = (CoapResource) res;
					resourceExist = true;
				}
			}
			if (!resourceExist) {
				subResource = new RDTagResource(next,true, this);
				resource.add(subResource);
			}
			resource = subResource;
		}
		subResource.setPath(resource.getPath());
		subResource.setName(next);
		//scanner.close();
		
		//logger.info("Durata addNodeResources2 --> {}", (System.currentTimeMillis() - startNodeR));
		
		return subResource;
	}

	@Override
	public void delete() {

		LOGGER.info("Removing endpoint: "+getContext());
		
		if (timerHandler!=null) {
			timerHandler.cancel(true);
		}
		
		super.delete();
	}
	
	public void deleteBackup() {

		LOGGER.info("Removing endpoint: "+getContext());
		
		if (lifetimeTimer!=null) {
			lifetimeTimer.cancel();
		}
		
		super.delete();
	}

	/*
	 * GET only debug return endpoint identifier
	 */
	@Override
	public void handleGET(CoapExchange exchange) {
		exchange.respond(ResponseCode.FORBIDDEN, "RD update handle");
	}
	
	/*
	 * PUTs content to this resource. PUT is a periodic request from the
	 * node to update the lifetime.
	 */
	@Override
	public void handlePOST(CoapExchange exchange) {
		
		if (lifetimeTimer != null) {
			lifetimeTimer.cancel();
		}
		
		LOGGER.info("Updating endpoint: "+getContext());
		
		setParameters(exchange.advanced().getRequest());
		
		// complete the request
		exchange.respond(ResponseCode.CHANGED);
		
	}
	
	/*
	 * DELETEs this node resource
	 */
	@Override
	public void handleDELETE(CoapExchange exchange) {
		delete();
		exchange.respond(ResponseCode.DELETED);
	}

	/*
	 * set either a new lifetime (for new resources, POST request) or update
	 * the lifetime (for PUT request)
	 */
	public void setLifeTime(int newLifeTime) {
		
		lifeTime = newLifeTime;
		
		if (lifetimeTimer != null) {
			lifetimeTimer.cancel();
		}
		
		lifetimeTimer = new Timer();
		lifetimeTimer.schedule(new ExpiryTask(this), lifeTime * 1000 + 2000);// from sec to ms
	
	}
	
	/*
	 * set either a new lifetime (for new resources, POST request) or update
	 * the lifetime (for PUT request)
	 */
	public void setLifeTime2(int newLifeTime) {
		
		lifeTime = newLifeTime;
		
		if (timerHandler != null) {
			timerHandler.cancel(true);
		}
		
		executorTimer.schedule(new ExpiryTask(this), lifeTime + 2, TimeUnit.SECONDS);// from sec to ms
	
	}
	
	public void setExpirationDate(int newLifeTime) {
		expirationDate = new DateTime();
		expirationDate = expirationDate.plusSeconds(newLifeTime + 2);	
	}
	
	public void checkExpirationDate(){
		if(expirationDate.isBeforeNow()){
			this.delete();
		}
	}
		
	/**
	 * Creates a new subResource for each resource the node wants
	 * register. Each resource is separated by ",". E.g. A node can
	 * register a resource for reading the temperature and another one
	 * for reading the humidity.
	 */
	private boolean updateEndpointResources(String linkFormat) {		
		Scanner scanner = new Scanner(linkFormat);
		
		scanner.useDelimiter(",");
		List<String> pathResources = new ArrayList<String>();
		while (scanner.hasNext()) {
			pathResources.add(scanner.next());
		}		
		
		for (String p : pathResources) {
			scanner = new Scanner(p);			
			/*
			 * get the path of the endpoint's resource. E.g. from
			 * </readings/temp> it will select /readings/temp.
			 */
			String path = "", pathTemp = "";
			if ((pathTemp = scanner.findInLine("</.*?>")) != null) {
				path = pathTemp.substring(1, pathTemp.length() - 1);
			} else {
				scanner.close();
				return false;
			}
			
			CoapResource resource = addNodeResource(path);
			
			/*
			 * Since created the subResource, get all the attributes from
			 * the payload. Each parameter is separated by a ";".
			 */
			scanner.useDelimiter(";");
			//Clear attributes to make registration idempotent
			for(String attribute:resource.getAttributes().getAttributeKeySet()){
				resource.getAttributes().clearAttribute(attribute);
			}
			while (scanner.hasNext()) {
				LinkAttribute attr = LinkAttribute.parse(scanner.next());
				if (attr.getValue() == null)
					resource.getAttributes().addAttribute(attr.getName());
				else resource.getAttributes().addAttribute(attr.getName(), attr.getValue());
			}
			resource.getAttributes().addAttribute(LinkFormat.END_POINT, getEndpointIdentifier());
		}
		
		
		scanner.close();
				
		return true;
	}
	
	
	
	private boolean updateEndpointResources2(String linkFormat) {
		//long startUpdateR = System.currentTimeMillis();
		
		Iterable<String> pathResources = Splitter.on(',').omitEmptyStrings().split(linkFormat);
				
		for (String p : pathResources) {			
			
//			String path = "", pathTemp = "";
//			if ((pathTemp = scanner.findInLine("</.*?>")) != null) {
//				path = pathTemp.substring(1, pathTemp.length() - 1);
//			} else {
//				scanner.close();
//				return false;
//			}
			/*
			 * Since created the subResource, get all the attributes from
			 * the payload. Each parameter is separated by a ";".
			 */
			Iterator<String> attributes = Splitter.on(';').split(p).iterator();
			
			if(attributes.hasNext()){
				String path = attributes.next();
				
				if(!path.isEmpty()){
					/*
					 * get the path of the endpoint's resource. E.g. from
					 * </readings/temp> it will select /readings/temp.
					 */
					path = path.substring(2, path.length() - 1);
					
					CoapResource resource = addNodeResource2(path);
					
					//Clear attributes to make registration idempotent
					for(String attribute : resource.getAttributes().getAttributeKeySet()){
						resource.getAttributes().clearAttribute(attribute);
					}
					while(attributes.hasNext()){
						LinkAttribute attr = LinkAttribute.parse2(attributes.next());
						if(attr != null){
							if (attr.getValue() == null) resource.getAttributes().addAttribute(attr.getName());
							else resource.getAttributes().addAttribute(attr.getName(), attr.getValue());
						}
					}
					resource.getAttributes().addAttribute(LinkFormat.END_POINT, getEndpointIdentifier());
				}
			}
		}
		
		//logger.info("Durata updateEndpointResources2 --> {}", System.currentTimeMillis() - startUpdateR);
						
		return true;
	}
	
	private boolean updateEndpointResources3(String linkFormat) {
		//long startUpdateR = System.currentTimeMillis();
		
		Iterable<String> pathResources = Splitter.on(',').omitEmptyStrings().split(linkFormat);
				
		for (String p : pathResources) {			
			
//			String path = "", pathTemp = "";
//			if ((pathTemp = scanner.findInLine("</.*?>")) != null) {
//				path = pathTemp.substring(1, pathTemp.length() - 1);
//			} else {
//				scanner.close();
//				return false;
//			}
			/*
			 * Since created the subResource, get all the attributes from
			 * the payload. Each parameter is separated by a ";".
			 */
			Iterator<String> attributes = Splitter.on(';').split(p).iterator();
			
			if(attributes.hasNext()){
				StringBuilder path = new StringBuilder(attributes.next());
				
				if(path.length() > 0){
					/*
					 * get the path of the endpoint's resource. E.g. from
					 * </readings/temp> it will select /readings/temp.
					 */
					String pathS = path.substring(2, path.length() - 1);
					CoapResource resource = addNodeResource2(pathS);
					//Clear attributes to make registration idempotent
					for(String attribute : resource.getAttributes().getAttributeKeySet()){
						resource.getAttributes().clearAttribute(attribute);
					}
					while(attributes.hasNext()){
						LinkAttribute attr = LinkAttribute.parse4(attributes.next());
						if(attr != null){
							if (attr.getValue() == null) resource.getAttributes().addAttribute(attr.getName());
							else resource.getAttributes().addAttribute(attr.getName(), attr.getValue());
						}
					}
					resource.getAttributes().addAttribute(LinkFormat.END_POINT, getEndpointIdentifier());
				}
			}
		}
		
		//logger.info("Durata updateEndpointResources2 --> {}", System.currentTimeMillis() - startUpdateR);
						
		return true;
	}

	/*
	 * the following three methods are used to print the right string to put in
	 * the payload to respond to the GET request.
	 */
	public String toLinkFormat(List<String> query) {

		// Create new StringBuilder
		StringBuilder builder = new StringBuilder();
		
		// Build the link format
		buildLinkFormat(this, builder, query);

		// Remove last delimiter
		if (builder.length() > 0) {
			builder.deleteCharAt(builder.length() - 1);
		}

		return builder.toString();
	}

	public String toLinkFormatItem(Resource resource) {
		StringBuilder linkFormat = new StringBuilder();
		
		linkFormat.append("<"+getContext());
		linkFormat.append(resource.getURI().substring(this.getURI().length()));
		linkFormat.append(">");
		
		return linkFormat.append( LinkFormat.serializeResource(resource).toString().replaceFirst("<.+>", "") ).toString();
	}
	

	private void buildLinkFormat(Resource resource, StringBuilder builder, List<String> query) {
		if (resource.getChildren().size() > 0) {

			// Loop over all sub-resources
			for (Resource res : resource.getChildren()) {
				if (LinkFormat.matches(res, query)) {

					// Convert Resource to string representation and add
					// delimiter
					builder.append(toLinkFormatItem(res));
					builder.append(',');
				}
				// Recurse
				buildLinkFormat(res, builder, query);
			}
		}
	}
	
	
	
	/*
	 * Setter And Getter
	 */

	public String getEndpointIdentifier() {
		return endpointIdentifier;
	}

	public String getDomain() {
		return domain;
	}

	public String getEndpointType() {
		return endpointType;
	}

	public void setEndpointType(String endpointType) {
		this.endpointType = endpointType;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
	public DateTime getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(DateTime expirationDate) {
		this.expirationDate = expirationDate;
	}



	class ExpiryTask extends TimerTask {
		RDNodeResource resource;

		public ExpiryTask(RDNodeResource resource) {
			super();
			this.resource = resource;
		}

		@Override
		public void run() {
			delete();
		}
	}
	
}
