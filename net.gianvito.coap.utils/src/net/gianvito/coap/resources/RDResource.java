/*******************************************************************************
 * Copyright (c) 2014 Institute for Pervasive Computing, ETH Zurich and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *    Matthias Kovatsch - creator and main architect
 ******************************************************************************/
package net.gianvito.coap.resources;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import net.gianvito.coap.utils.StringCoAPUtils;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.ConcurrentCoapResource;
import org.eclipse.californium.core.server.resources.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RDResource extends CoapResource {
	//private static final Logger logger = LoggerFactory.getLogger(RDResource.class);
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
	// Expiry time interval in seconds.
	private final int expiryTaskInterval = 30;
	
	public RDResource() {
		this("rd");
		executor.scheduleAtFixedRate(new ExpiryTask(), expiryTaskInterval, expiryTaskInterval, TimeUnit.SECONDS);
	}

	public RDResource(String resourceIdentifier) {
		super(resourceIdentifier);
		getAttributes().addResourceType("core.rd");
		executor.scheduleAtFixedRate(new ExpiryTask(), expiryTaskInterval, expiryTaskInterval, TimeUnit.SECONDS);
	}

	/*
	 * POSTs a new sub-resource to this resource. The name of the new
	 * sub-resource is a random number if not specified in the Option-query.
	 */
//	@Override
//	public void handlePOST(CoapExchange exchange) {
//		// get name and lifetime from option query
//		String endpointIdentifier = "";
//		String domain = "local";
//		RDNodeResource resource = null;
//		
//		ResponseCode responseCode;
//
//		//LOGGER.info("Registration request: "+exchange.getSourceAddress());
//		
//		List<String> query = exchange.getRequestOptions().getURIQueries();
//		String qTemp;
//		for (String q : query) {
//			qTemp = StringCoAPUtils.parseName(q);
//			
//			if (qTemp.equals(LinkFormat.END_POINT)) {
//				endpointIdentifier = StringCoAPUtils.parseValue(q);
//			}
//			
//			if (qTemp.equals(LinkFormat.DOMAIN)) {
//				domain = StringCoAPUtils.parseValue(q);
//			}
//		}
//
//		if (endpointIdentifier.length() == 0) {
//			exchange.respond(ResponseCode.BAD_REQUEST, "Missing endpoint (?ep)");
//			//LOGGER.info("Missing endpoint: "+exchange.getSourceAddress());
//			return;
//		}
//		
//		for (Resource node : getChildren()) {
//			if (((RDNodeResource) node).getEndpointIdentifier().equals(endpointIdentifier) && ((RDNodeResource) node).getDomain().equals(domain)) {
//				resource = (RDNodeResource) node;
//				// TODO Possible break
//			}
//		}
//		
//		if (resource==null) {
//			
//			String randomName;
//			do {
//				randomName = Integer.toString((int) (Math.random() * 10000));
//			} while (getChild(randomName) != null);
//			
//			resource = new RDNodeResource(endpointIdentifier, domain);
//			add(resource);
//			
//			responseCode = ResponseCode.CREATED;
//		} else {
//			responseCode = ResponseCode.CHANGED;
//		}
//		
//		
//		
//		// set parameters of resource
//		if (!resource.setParameters3(exchange.advanced().getRequest(), exchange.advanced().getEndpoint().getAddress().getPort())) {
//			resource.delete();
//			exchange.respond(ResponseCode.BAD_REQUEST, "Parameters invalid.");
//			return;
//		}
//		
//		
//		
//		//LOGGER.info("Adding new endpoint: "+resource.getContext());
//
//		// inform client about the location of the new resource
//		exchange.setLocationPath(resource.getURI());
//		
//		// complete the request
//		exchange.respond(responseCode);
//		
//	}
	
	@Override
	public void handlePOST(CoapExchange exchange) {
		//long inizioHandle = System.currentTimeMillis();
		// get name and lifetime from option query
		LinkAttribute attr;
		String endpointIdentifier = "";
		String domain = "local";
		RDNodeResource resource = null;
		
		ResponseCode responseCode;

		//LOGGER.info("Registration request: "+exchange.getSourceAddress());
		
		List<String> query = exchange.getRequestOptions().getURIQueries();
		for (String q : query) {
			// FIXME Do not use Link attributes for URI template variables
			attr = LinkAttribute.parse(q);
			
			if (attr.getName().equals(LinkFormat.END_POINT)) {
				endpointIdentifier = attr.getValue();
			}
			
			if (attr.getName().equals(LinkFormat.DOMAIN)) {
				domain = attr.getValue();
			}
		}

		if (endpointIdentifier.length() == 0) {
			exchange.respond(ResponseCode.BAD_REQUEST, "Missing endpoint (?ep)");
			//LOGGER.info("Missing endpoint: "+exchange.getSourceAddress());
			return;
		}
		
		for (Resource node : getChildren()) {
			if (((RDNodeResource) node).getEndpointIdentifier().equals(endpointIdentifier) && ((RDNodeResource) node).getDomain().equals(domain)) {
				resource = (RDNodeResource) node;
			}
		}
		
		if (resource==null) {
			
			String randomName;
			do {
				randomName = Integer.toString((int) (Math.random() * 10000));
			} while (getChild(randomName) != null);
			
			resource = new RDNodeResource(endpointIdentifier, domain);
			add(resource);
			
			responseCode = ResponseCode.CREATED;
		} else {
			responseCode = ResponseCode.CHANGED;
		}
		
		
		
		// set parameters of resource
		if (!resource.setParameters(exchange.advanced().getRequest())) {
			resource.delete();
			exchange.respond(ResponseCode.BAD_REQUEST);
			return;
		}
		
		
		
		//LOGGER.info("Adding new endpoint: "+resource.getContext());

		// inform client about the location of the new resource
		exchange.setLocationPath(resource.getURI());

		//logger.info("Fine handlePOST --> " + System.currentTimeMillis());
		
		// complete the request
		exchange.respond(responseCode);
		
		//logger.info("Durata handlePOST --> {}", System.currentTimeMillis() - inizioHandle);
	}
	
	
	/**
	 * Check if a node is expired. If so it will be removed from RD.
	 * 
	 * @author Gianvito Morena
	 *
	 */
	class ExpiryTask implements Runnable{

		public ExpiryTask() {
			super();
		}

		@Override
		public void run() {
			Thread.currentThread().setName("ExpiryTask:Thread");
			Collection<Resource> resources = getChildren();
			Iterator<Resource>  resIt = resources.iterator();
			
			while(resIt.hasNext()){
				((RDNodeResource)resIt.next()).checkExpirationDate();
			}
		}
	}

}
