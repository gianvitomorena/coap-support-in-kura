package net.gianvito.coap.utils;

import java.util.Date;

import net.gianvito.coap.system.SystemProperties;

public class MessageUtils {
	/**
	 * Create a serial number for CoAPMessages
	 * 
	 * @param firstHalf
	 * @return
	 */
	public static long createSerialNumber(long firstHalf){
		String secondHalf = String.valueOf(new Date().getTime());
		StringBuilder completeSerialN = new StringBuilder().append(firstHalf).append(secondHalf.substring(4));
		return Long.parseLong(completeSerialN.toString());
	}
	
	/**
	 * Create the first half of the serial number for CoAPMessages.
	 * 
	 * @param systemProperties
	 * @param networkInterfaceName
	 * @return
	 */
	public static long getFirstHalfSerialNumber(SystemProperties systemProperties, String networkInterfaceName){
		return systemProperties.getMAC(networkInterfaceName).hashCode();
	}
}
