package net.gianvito.coap.utils;

import java.util.List;

import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.server.resources.Resource;

import net.gianvito.coap.message.KuraNode;

/**
 * Helper class for optimal Path handling.
 * 
 * @author Gianvito Morena
 *
 */
public class PathUtils {
	
	public static final String ROOT_ID = "/root";
	
	/**
	 * Helper to the building of the complete MQTT path.
	 * 
	 * Example --> 192.168.1.1/gianvito.net/bologna.
	 * 
	 * @param destinationNode
	 * @return
	 */
	public static String buildCompleteNodePath(KuraNode destinationNode){
		String searchPath = "";
		
		searchPath += destinationNode.getKuraAddress();
		searchPath += "/" + destinationNode.getMacAddress();
		searchPath += buildRelativeNodePath(destinationNode.getDomain(), destinationNode.getGroup());
		
		return searchPath;
	}
	
	/**
	 * Helper to the building of the relative MQTT path.
	 * 
	 * Example --> /gianvito.net/bologna
	 * 
	 * @param domain
	 * @param group
	 * @return
	 */
	public static String buildRelativeNodePath(String domain, String group){
		String searchPath = "";
		
		if(domain != null && !domain.equals("*")){
			searchPath += "/" + domain;
		}
		if(group != null && !group.equals("*") && !group.equals("")){
			searchPath += "/" + group;
		}
		
		searchPath = searchPath.equals("")? ROOT_ID : searchPath;
		
		return searchPath;
	}
	
	/**
	 * Helper for the removing of a level on the MQTT path.
	 * 
	 * Example --> /gianvito.net/bologna --> /gianvito.net
	 * 
	 * @param path
	 * @return
	 */
	public static String removeLevel(String path){
		String newPath = new String();
		String pathSeparator = "/";
		
		int sep = path.lastIndexOf(pathSeparator);
        newPath = path.substring(0, sep);
		
		return newPath;
	}
	
	/**
	 * Helper for know if a path is more general than another.
	 * 
	 * Example --> path1 = /gianvito.net/bologna --> path2 = /gianvito.net ---> return false
	 * 
	 * @param path1
	 * @param path2
	 * @return
	 */
	public static Boolean isMoreGeneralThan(String path1, String path2){
		Boolean result = false;
		
		// If path1 is the root is always more general except when path2 is another root
		if(path1.equals(ROOT_ID) && !path2.equals(path1)){
			result = true;
		}else{
			// If path2 is not equals to path1 and has path1 as prefix, it means path1 is more
			//  general than path2.
			if(!path2.equals(path1) && path2.startsWith(path1)){
				result = true;
			}else{
				result = false;
			}
		}
		
		return result;
	}
	
	/**
	 * Check if input node is valid to be requested by message sender.
	 * 
	 * @return false if it's in the same path.
	 */
	public static Boolean checkNodeValidityForAdding(KuraNode node, KuraNode senderNode){
		Boolean result = true;
		
		// Input node is not valid to be requested by sender because has a more general path.
		//	It means it's located between current node and sender
		if(PathUtils.isMoreGeneralThan(node.getRelativePath(), senderNode.getRelativePath()) ||
				node.equals(senderNode)){
			result = false;
		}
		
		return result;
	}
	
	/**
	 * Adds /* to the LWT path to help CTH finding right node (level one nodes).
	 * 
	 * @param path
	 * @return
	 */
	public static String fixLWTPath(String path){
		String pathSeparator = "/";
		String newPath = path;
		
		// Check if it's a first level node
		int sep = path.lastIndexOf(pathSeparator);
		newPath += sep == -1? "/*" : "";
		
		return newPath;
	}

	/**
	 * Extract path from LWT topic.
	 * 
	 * @param pathMAC
	 * @param rootID
	 * @return
	 */
	public static String getPathFromLWT(String pathMAC, String rootID){
		String pathSeparator = "/";
		
		// Get the last position of '/'
		int lastSlashIndex = pathMAC.lastIndexOf(pathSeparator);
		
		String path = pathMAC.substring(0, lastSlashIndex);
		
		if(!path.equals(rootID) && !path.contains("*")){
			// Add '/*' to path for level 1 nodes
			path = PathUtils.fixLWTPath(path);
		}
		
		return path;
	}
	
	/**
	 * Extract Mac address from LWT topic.
	 * 
	 * @param pathMAC
	 * @return
	 */
	public static String getMACFromLWT(String pathMAC){
		String pathSeparator = "/";
		String mac = "";
		
		// Get the last position of '/'
		int lastSlashIndex = pathMAC.lastIndexOf(pathSeparator);
		
		// Get the MAC from the last part
		mac = pathMAC.substring(lastSlashIndex + 1);
		
		return mac;
	}
	
	/**
	 * USELESS
	 * 
	 * @param query
	 * @return
	 */
	public String toLinkFormat(List<String> query) {

		// Create new StringBuilder
		StringBuilder builder = new StringBuilder();
		
		// Build the link format
		//buildLinkFormat(this, builder, query);

		// Remove last delimiter
		if (builder.length() > 0) {
			builder.deleteCharAt(builder.length() - 1);
		}

		return builder.toString();
	}

	/**
	 * USELESS
	 * 
	 * @param resource
	 * @return
	 */
	public String toLinkFormatItem(Resource resource) {
		StringBuilder linkFormat = new StringBuilder();
		
		//linkFormat.append("<"+getContext());
		linkFormat.append("<" + resource.getPath()+resource.getName());
		linkFormat.append(">");
		
		return linkFormat.append( LinkFormat.serializeResource(resource).toString().replaceFirst("<.+>", "") ).toString();
	}
	
	/**
	 * USELESS
	 * 
	 * @param resource
	 * @param builder
	 * @param query
	 */
	@SuppressWarnings("unused")
	private void buildLinkFormat(Resource resource, StringBuilder builder, List<String> query) {
		if (resource.getChildren().size() > 0) {

			// Loop over all sub-resources
			for (Resource res : resource.getChildren()) {
				// System.out.println(resource.getSubResources().size());
				// System.out.println(res.getName());
				if (LinkFormat.matches(res, query) && res.getAttributes().getCount() > 0) {

					// Convert Resource to string representation and add
					// delimiter
					builder.append(toLinkFormatItem(res));
					builder.append(',');
				}
				// Recurse
				buildLinkFormat(res, builder, query);
			}
		}
	}
}
