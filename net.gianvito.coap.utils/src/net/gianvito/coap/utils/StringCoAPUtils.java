package net.gianvito.coap.utils;

import net.gianvito.coap.resources.LinkAttribute;

public class StringCoAPUtils {
	public static String replaceWithNull(String path, char[] chars){
		StringBuilder pathB = new StringBuilder(path);
		
		for(int i = 0; i < pathB.length(); i++){
			for(int j = 0; j < chars.length; j++){
				if(pathB.charAt(i) == chars[j]){
					pathB.deleteCharAt(i);
					i--;
				}
			}
		}
		
		return pathB.toString();
	}
	
	public static String parseName(String str) {
		int equalSep = str.indexOf('=');
		
		if(equalSep != -1){
			return str.substring(0, equalSep);
		}else{
			return null;
		}
	}
	
	public static String parseValue(String str) {
		int equalSep = str.indexOf('=');
		
		if(equalSep != -1){
			if(str.charAt(equalSep + 1) == '"'){
				return str.substring(equalSep + 2, str.lastIndexOf('"'));
			}else{
				return str.substring(equalSep + 1);
			}
		}else{
			return null;
		}
	}
	
	public static int parseIntValue(String str) {
		int equalSep = str.indexOf('=');
		
		if(equalSep != -1){
			if(str.charAt(equalSep + 1) == '"'){
				return Integer.valueOf(str.substring(equalSep + 2, str.lastIndexOf('"')));
			}else{
				return Integer.valueOf(str.substring(equalSep + 1));
			}
		}else{
			return 0;
		}
	}
}
