package net.gianvito.coap.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.message.PropagationObject;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.pool.KryoFactory;
import com.esotericsoftware.kryo.pool.KryoPool;

/**
 * Utility to serialize and deserialize objects using either normal Java serialization or
 * a fast and efficient framework called Kryo.
 * 
 * @author Gianvito Morena
 *
 */
public class ByteObjectUtils {
	//private static final Logger logger = LoggerFactory.getLogger(ByteObjectUtils.class);
	
//	private static final ThreadLocal<Kryo> kryoThreadLocal = new ThreadLocal<Kryo>() {
// 
//		 @Override
//		 protected Kryo initialValue() {
//		     Kryo kryo = new Kryo();
//		     //kryo.register(CoAPMessage.class);
//		     //kryo.register(KuraNode.class);
//		     //kryo.register(Date.class);
//		     return kryo;
//		 }
//	};
	
	public static KryoFactory factory = new KryoFactory() {
		  public Kryo create () {
		    Kryo kryo = new Kryo();
		    // configure kryo instance, customize settings
		    //kryo.register(CoAPMessage.class);
		    //kryo.register(KuraNode.class);
		    //kryo.register(Date.class);
		    //kryo.register(AttributeCollection.class);
		    //kryo.register(GeneralProperty.class);
		    //kryo.register(NodeProperty.class);
		    //kryo.register(ResourceProperty.class);
		    //kryo.register(PropagationObject.class);
		    return kryo;
		  }
	};
	
	public static KryoPool pool = new KryoPool.Builder(factory).softReferences().build();
	
	/**
	 * Apply normal Java serialization.
	 * 
	 * @param obj
	 * @return
	 */
	public static byte[] serialize(Object obj){
		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ObjectOutputStream os;
		try {
			os = new ObjectOutputStream(out);
			os.writeObject(obj);
		    os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    return out.toByteArray();
	}
	
	/**
	 * Get serialized object using normal Java serialization.
	 * @param <T>
	 * @param <T>
	 * 
	 * @param data
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static <T> T getSerializedObject(byte[] data) throws ClassNotFoundException, IOException{					
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream is = new ObjectInputStream(in);
		
		T object = (T) is.readObject();
				
		return object;
	}
	
	/**
	 * Use Kryo for fast and efficient serialization between brokers.
	 * @param <T>
	 * 
	 * @param obj
	 * @return
	 * @throws IOException
	 */
	public static <T> byte[] toByteArray(T obj) throws IOException {
		//Kryo kryo = new Kryo();
		//Kryo kryo = kryoThreadLocal.get();
		
		
	    //ByteArrayOutputStream out = new ByteArrayOutputStream();
	    //DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(out);
	    
	    Output output = new Output(4096);

	    Kryo kryo = pool.borrow();
	    //Output output = kryo.getStreamFactory().getOutput(deflaterOutputStream, 4096);
	    kryo.writeObject(output, obj);
	    pool.release(kryo);
	    
	    output.flush();
	    //deflaterOutputStream.close();
	    //out.flush();
	    
	    //byte[] array = out.toByteArray();
	    
	    return output.toBytes();
	}
	
	/**
	 * Get serialized object with Kryo.
	 * 
	 * @param data
	 * @param className
	 * @return
	 * @throws IOException
	 */
	public static <T> T  getObject(byte[] data, Class<T> className) throws IOException{
		//logger.info("Inizio getObject interna --> {}", new Date().getTime());
		//Kryo kryo = kryoThreadLocal.get();
		
		//logger.info("Dopo creazione di Kryo interna --> {}", new Date().getTime());
		
		//ByteArrayInputStream in = new ByteArrayInputStream(data);
		//InflaterInputStream infl = new InflaterInputStream(in);
				
		Input input = new Input(data);
			
		Kryo kryo = pool.borrow();
		//Input input = kryo.getStreamFactory().getInput(infl, 4096);
		T object = kryo.readObject(input, className);
		pool.release(kryo);
		
		input.close();
		
		return object;
	}


}
