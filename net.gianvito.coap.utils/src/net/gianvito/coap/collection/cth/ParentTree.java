package net.gianvito.coap.collection.cth;

import java.io.Serializable;
import java.util.Collection;

import net.gianvito.coap.message.KuraNode;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Data structure which links a KuraNode with all its children.
 * 
 * @author Gianvito Morena
 *
 */
public class ParentTree implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	
	private static final long serialVersionUID = 4800487098214170575L;
	
	// Multimap implemented by Google in Guava.
	private Multimap<KuraNode, KuraNode> tree;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public ParentTree(){
		tree = HashMultimap.create();
	}
	
	/**
	 * Add a children to the specified node.
	 * 
	 * @param parent
	 * @param node
	 */
	public void addChildren(KuraNode parent, KuraNode node){
		if(parent != null){
			tree.put(parent, node);
		}
	}
	
	/**
	 * Remove a children from the specified node.
	 * 
	 * @param parent
	 * @param node
	 */
	public void removeChildren(KuraNode parent, KuraNode node){
		if(parent != null){
			tree.remove(parent, node);
		}
	}
	
	/**
	 * Remove all children from the input node.
	 * 
	 * @param parent
	 */
	public void removeAllChildren(KuraNode parent){
		if(parent != null){
			tree.removeAll(parent);
		}
	}
	
	/**
	 * Search all the nodes with 'node' parent.
	 * 
	 * @param node
	 * @return
	 */
	public Collection<KuraNode> searchLinkedNodes(KuraNode node){
		return tree.get(node);
	}
	
	/**
	 * Search a potential duplicate between input parent children.
	 * Uses Google Guava Collections2.
	 * 
	 * @param parent
	 * @param mac
	 * @return
	 */
	public KuraNode searchDeviceByMac(KuraNode parent, final String mac){
		KuraNode foundDevice = null;
		
		Collection<KuraNode> devicesWithMac = Collections2.filter(searchLinkedNodes(parent), new Predicate<KuraNode>(){
			@Override
			public boolean apply(KuraNode node) {
				return node.getMacAddress().equals(mac);
			}
			
		});
		
		if(devicesWithMac.size() > 0){
			foundDevice = devicesWithMac.iterator().next();
		}
				
		return foundDevice;
	}
	
	/**
	 * Remove a duplicate by MAC address.
	 * 
	 * @param parent
	 * @param mac
	 * @return
	 */
	public KuraNode removeDeviceByMac(KuraNode parent, final String mac){
		KuraNode foundDevice = searchDeviceByMac(parent, mac);
		Boolean operationResult = tree.get(parent).remove(foundDevice);
		
		if(operationResult){
			return foundDevice;
		}
		
		return null;
	}

	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public Multimap<KuraNode, KuraNode> getTree() {
		return tree;
	}
	public void setTree(Multimap<KuraNode, KuraNode> tree) {
		this.tree = tree;
	}
	
	
}
