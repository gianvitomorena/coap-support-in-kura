package net.gianvito.coap.collection.cth;

import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.utils.PathUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Collection used by CoAPTreeHandler to handle every request published on the
 * MQTT broker by Kura nodes.
 * 
 * @author Gianvito Morena
 *
 */
public class CTHCollection {
	
	/* ----------------------- Attributes -------------------------- */
	
	private static final transient Logger logger = LoggerFactory.getLogger(CTHCollection.class);
	
	private DevicesMap devices;
	private ParentTree parents;
		
	/* ----------------------- Methods ----------------------------- */
	/**
	 * Constructor.
	 */
	public CTHCollection(){
		devices = new DevicesMap();
		parents = new ParentTree();
	}
	
	/**
	 * Add a root node to the tree.
	 * 
	 * @param node
	 * @return
	 */
	public KuraNode addRoot(KuraNode node){
		// Atomic operation with putIfAbsent
		return devices.putIfAbsent(CoAPKey.ROOT_ID, node);
	}
	
	
	/**
	 * Remove a root node from the tree is it's the one passed.
	 * 
	 * @param node
	 * @return
	 */
	public RemoveResult removeRoot(KuraNode node){
		Boolean operationResult = false;
		RemoveResult result = new RemoveResult();
		
		// Check if a root node is already in the HashMap
		if(devices.containsRoot()){
			// Check if root node is the one to delete
			if(devices.getRoot().equals(node)){
				// Remove the root node
				KuraNode removedRoot = devices.removeRoot();
				// Add removed root to result
				result.setRemovedNode(removedRoot);
				
				// Find all node children
				Set<KuraNode> nodeChildren = (Set<KuraNode>) parents.searchLinkedNodes(node);
				// Add children of the node to the result
				result.setNodeChildren(new HashSet<KuraNode>(nodeChildren));
				
				// Remove all tuples with failed node as parent.
				parents.removeAllChildren(node);
				
				// Update devices Collection
				for(KuraNode nodeChild: nodeChildren){
					// Check if it's secure to replace
					if(devices.get(nodeChild.getRelativePath()).equals(nodeChild)){
						nodeChild.setParentNode(null);
						devices.replace(nodeChild.getRelativePath(), nodeChild);
					}
				}
								
				operationResult = true;
				logger.info("Root removed from tree.");
			}else{
				// Duplicate case. Root node is not the one to delete
				logger.info("Node " + node.getCompletePath() + " is not between devices. Possible duplicate.");
				result.setRemovedNode(parents.removeDeviceByMac(devices.getRoot(), node.getMacAddress()));
			}
		}
		
		result.setOperationResult(operationResult);
		
		return result;
	}
	
	/**
	 * Add a new node to the hierarchy and send him his closest parent.
	 * 
	 * @param node
	 * @return
	 */
	public KuraNode addNodeRequest(KuraNode node){
		KuraNode parent = null;
		
		if(node != null){
			KuraNode currentNodeOnPath = devices.get(node.getRelativePath());
			
			// Check if it's a duplicate.
			if(currentNodeOnPath != null && !currentNodeOnPath.equals(node)){
				// Don't put it in the devices map cause it's a single value map
				// Return current node as parent
				parent = currentNodeOnPath;
				// TODO Possible devices Multimap extension
			}else{
				//devices.put(node.getRelativePath(), node);
				// Find node's parent
				parent = searchNewParentNode(node);
			}
		}
		
		return parent;
	}
	
	/**
	 * Remove a new node from the hierarchy.
	 * 
	 * @param node
	 * @return
	 */
	public RemoveResult removeNode(KuraNode node){
		Boolean operationResult = false;
		RemoveResult result = new RemoveResult();
		
		if(node != null){
			KuraNode currentNodeAtPath = devices.get(node.getRelativePath());
			
			// Check if node is a possibile root one
			if(node.getLevel() == 0){
				logger.info("Removing a root node from CTHCollection");
				result = removeRoot(node);
				operationResult = true;
				
			// Remove a generic node.
			}else if(currentNodeAtPath != null){ // It actually exists.
				// It equals to the current node at the relative path, or it has, at least, the same MAC address.
				// FIXME There was currentNodeAtPath.equals(node)
				if(currentNodeAtPath.getMacAddress().equals(node.getMacAddress())){
					// Find all node children
					Set<KuraNode> nodeChildren = (Set<KuraNode>) parents.searchLinkedNodes(node);
					// Add node children to the result
					result.setNodeChildren(new HashSet<KuraNode>(nodeChildren));
					
					// Remove all tuples with failed node as parent.
					parents.removeAllChildren(node);
					// Remove the link between the parent and the failed node.
					parents.removeChildren(node.getParentNode(), node);
					// Remove the node
					KuraNode removedNode = devices.remove(node.getRelativePath());
					result.setRemovedNode(removedNode);
					
					operationResult = true;
					
					
				// It's probably a duplicate node.
				}else{
					logger.info("Node " + node.getCompletePath() + " is not between devices. Possible duplicate.");
					result.setRemovedNode(parents.removeDeviceByMac(node.getParentNode(), node.getMacAddress()));
					
					operationResult = true;
				}
			}	
		}
		
		result.setOperationResult(operationResult);
		
		return result;
	}
	
	/**
	 * Update information about updated root reference on node.
	 * 
	 * @param node
	 * @param oldRootNode
	 * @return
	 */
	public Boolean updateRootOnNode(KuraNode node, KuraNode oldRootNode){
		Boolean operationResult = false;
		
		KuraNode currentRoot = devices.getRoot();
		
		if(node != null){
			KuraNode parentNode = node.getParentNode();
			
			// Check if the node updated is the root. If exists a root, it's a duplicate
			// Simply add it to the hierarchy (ParentTree)
			if(node.getLevel() == 0){
				if(currentRoot != null && currentRoot.equals(node)){
					// Updated Root on root node.
				}else{
					// Duplicate.
					parents.addChildren(parentNode, node);
				}
			}else{
				// Replace the older one.
				devices.put(node.getRelativePath(), node);
				// Remove children of old root node
				parents.removeChildren(oldRootNode, node);
				// Add a new children to the parent node
				parents.addChildren(parentNode, node);
			}
	
			operationResult = true;
		}
		
		return operationResult;
	}
	
	/**
	 * Update information about parent reference on input node relative to oldParentNode.
	 * 
	 * @param node
	 * @param oldRootNode
	 * @return
	 */
	public Boolean updateParentOnNode(KuraNode node, KuraNode oldParentNode){
		Boolean operationResult = false;
		
		if(node != null){
			KuraNode currentNodeAtPath = devices.get(node.getRelativePath());
			KuraNode parentNode = node.getParentNode();
			
			// If there is a duplicate.
			if(currentNodeAtPath != null && !currentNodeAtPath.equals(node)){
				// Don't remove anything
			}else{
				// Remove the sender from the parent node (as child)
				parents.removeChildren(oldParentNode, node);
				// Replace node on the devices map
				devices.put(node.getRelativePath(), node);
			}
			
			// If new parentNode is null, it's useless to add a new tuple in the tree.
			if(parentNode != null){
				// Add the sender as child to the new parent node
				parents.addChildren(parentNode, node);
			}
	
			operationResult = true;
		}
		
		return operationResult;
	}
	
	
	
	/**
	 * Find a device by his path and then by his MAC.
	 * 
	 * @param pathMAC
	 * @return
	 */
	public KuraNode findDeviceByMAC(String pathMAC){
		KuraNode foundDevice = null;
		
		logger.info("LWT received --> " + pathMAC);

		String path = PathUtils.getPathFromLWT(pathMAC, CoAPKey.ROOT_ID);
		String mac = PathUtils.getMACFromLWT(pathMAC);
		
		// Search for a root node
		if(path.equals(CoAPKey.ROOT_ID)){
			KuraNode currentRoot = devices.getRoot();
			if(currentRoot != null
					&& currentRoot.getMacAddress().equals(mac)){
				
				foundDevice = currentRoot;
				logger.info("Root disconnected --> " + foundDevice.getCompletePath());
			}
		// Search for a normal node
		}else{
			if(devices.containsKey(path)){
				if(devices.get(path).getMacAddress().equals(mac)){
					foundDevice = devices.get(path);
					logger.info("Device disconnected --> " + foundDevice.getCompletePath());
				}
			}
		}
		// If device has not been found with the MAC address, perhaps it's a duplicate.
		if(foundDevice == null){
			foundDevice = searchDuplicateOnParentTree(devices.get(path), mac);
			if(foundDevice != null)
				logger.info("Duplicate disconnected --> " + foundDevice.getCompletePath());
		}

		return foundDevice;
	}
	
	/**
	 * Search a duplicate with a specific MAC address.
	 * 
	 * @param possibleParent
	 * @param mac
	 * @return
	 */
	public KuraNode searchDuplicateOnParentTree(KuraNode possibleParent, String mac){
		return parents.searchDeviceByMac(possibleParent, mac);
	}
	
	/**
	 * Search a new parent for a specific node following the tree starting from leaves.
	 * 
	 * @param node
	 * @return nearest parent for node
	 */
	public KuraNode searchNewParentNode(KuraNode node){
		String searchPath = node.getRelativePath();
		KuraNode parentNode = null;
		
		// First level node
		if(node.getLevel() == 1){
			parentNode = devices.getRoot();
		// Second level node
		}else{
			// Remove a level until path equals a null one.
			do{
				searchPath = PathUtils.removeLevel(searchPath);
				//logger.info("Searching root for node " + node.getCompletePath() + " in --> " + searchPath);
				parentNode = devices.get(searchPath);
			}while(parentNode == null && !searchPath.equals(""));
			
			if(parentNode == null){
				parentNode = devices.getRoot();
				logger.debug("Setting root node as parent node for --> {}", node.getCompletePath());
			}else{
				logger.debug("Found parent node --> {}", parentNode.getCompletePath());
			}
		}
		
		return parentNode;
	}

	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public DevicesMap getDevices() {
		return devices;
	}
	public void setDevices(DevicesMap devices) {
		this.devices = devices;
	}
	public ParentTree getParents() {
		return parents;
	}
	public void setParents(ParentTree parents) {
		this.parents = parents;
	}
	
	/**
	 * Return device associated with a relative path.
	 * 
	 * @param relativePath
	 */
	public KuraNode getDeviceAtPath(String relativePath){
		return devices.get(relativePath);
	}
	
	/**
	 * Return current root node.
	 * 
	 * @return
	 */
	public KuraNode getRootNode(){
		return devices.getRoot();
	}
	
}
