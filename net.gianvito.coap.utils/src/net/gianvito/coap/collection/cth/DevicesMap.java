package net.gianvito.coap.collection.cth;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;

/**
 * Data structure which allows to link a device path with the rappresentation of the Node itself.
 * Example: /gianvito.net/bologna --> KuraNode object
 * 
 * @author Gianvito Morena
 *
 */
public class DevicesMap extends ConcurrentHashMap<String, KuraNode> implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	
	private static final long serialVersionUID = -730646097874298654L;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public DevicesMap(){
		super();
	}
	
	/**
	 * Remove a root from DevicesMap.
	 * 
	 * @return
	 */
	public KuraNode removeRoot(){
		return this.remove(CoAPKey.ROOT_ID);
	}
	
	/**
	 * Return current root or null if it's not available.
	 * 
	 * @return
	 */
	public KuraNode getRoot(){
		if(this.containsKey(CoAPKey.ROOT_ID)){
			return this.get(CoAPKey.ROOT_ID);
		}else{
			return null;
		}
	}
	
	/**
	 * Return true if a root node is available.
	 * 
	 * @return
	 */
	public Boolean containsRoot(){
		if(this.containsKey(CoAPKey.ROOT_ID)){
			return true;
		}else{
			return false;
		}
	}
}
