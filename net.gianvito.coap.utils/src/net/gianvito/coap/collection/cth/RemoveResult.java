package net.gianvito.coap.collection.cth;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.message.KuraNode;

/**
 * Represents fields returned by {@link CTHCollection#removeNode(KuraNode)} method.
 * 
 * @author Gianvito Morena
 *
 */
public class RemoveResult implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 1207987763189992483L;
	
	Boolean operationResult;
	KuraNode removedNode;
	Set<KuraNode> nodeChildren;
	
	/* ----------------------- Methods ----------------------------- */
	
	public RemoveResult(){
		operationResult = false;
		removedNode = null;
		nodeChildren = new HashSet<KuraNode>();
	}
	
	public RemoveResult(Boolean operationResult, KuraNode removedNode, Set<KuraNode> removedChildren){
		this.operationResult = operationResult;
		this.removedNode = removedNode;
		this.nodeChildren = removedChildren;
	}
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public Boolean getOperationResult() {
		return operationResult;
	}
	public void setOperationResult(Boolean operationResult) {
		this.operationResult = operationResult;
	}
	public KuraNode getRemovedNode() {
		return removedNode;
	}
	public void setRemovedNode(KuraNode removedNode) {
		this.removedNode = removedNode;
	}
	public Set<KuraNode> getNodeChildren() {
		return nodeChildren;
	}
	public void setNodeChildren(Set<KuraNode> nodeChildren) {
		this.nodeChildren = nodeChildren;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
