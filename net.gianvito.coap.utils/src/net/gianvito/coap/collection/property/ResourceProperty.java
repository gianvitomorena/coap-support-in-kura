package net.gianvito.coap.collection.property;

import java.io.Serializable;

import net.gianvito.coap.type.PropertyType;

import org.eclipse.californium.core.server.resources.Resource;

/**
 * Represents a link to a local resource for a specific attribute.
 * 
 * @author Gianvito Morena
 *
 */
public class ResourceProperty extends GeneralProperty implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -2747045921248918464L;
	
	private Resource resource;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public ResourceProperty(){
		this.resource = null;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param resource
	 */
	public ResourceProperty(Resource resource){
		super(PropertyType.DIRECT_RESOURCE);
		this.resource = resource;
	}
	

	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	/* ------------------------------------------
	 * --- Override methods ---
	 * ------------------------------------------ */
	@Override
    public boolean equals(Object obj) {
		if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final ResourceProperty other = (ResourceProperty) obj;
        boolean sameResource = (this.resource == other.resource) || 
        								(this.resource != null && this.resource.equals(other.resource));
        if (!sameResource) return false;
        
        return true;
	}
	
	@Override
	public int hashCode(){
		return super.hashCode() + resource.hashCode();	
	}
	
	@Override
	public String toString(){
		return resource.getPath() + resource.getName();
	}
}
