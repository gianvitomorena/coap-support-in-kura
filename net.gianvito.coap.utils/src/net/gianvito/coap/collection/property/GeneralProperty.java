package net.gianvito.coap.collection.property;

import java.io.Serializable;

import net.gianvito.coap.type.PropertyType;

/**
 * General property used in the BrokerCollection inside CoAPServer.
 * 
 * @author Gianvito Morena
 *
 */
public class GeneralProperty implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 6482322912694623001L;
	
	private PropertyType propertyType;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public GeneralProperty(){
		this.propertyType = null;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param propertyType
	 */
	public GeneralProperty(PropertyType propertyType){
		this.propertyType = propertyType;
	}
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public PropertyType getType() {
		return propertyType;
	}
	public void setType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}
	

	/* ------------------------------------------
	 * --- Override methods ---
	 * ------------------------------------------ */
	@Override
    public boolean equals(Object obj) {
		if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final GeneralProperty other = (GeneralProperty) obj;
    	boolean sameType = (this.propertyType == other.propertyType) || (this.propertyType != null && this.propertyType.equals(other.propertyType));
    	if (!sameType) return false;
        
        return true;
	}
	@Override
	public int hashCode(){
		return propertyType.hashCode();	
	}
}
