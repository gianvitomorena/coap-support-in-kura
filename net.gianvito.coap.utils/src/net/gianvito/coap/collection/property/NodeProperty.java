package net.gianvito.coap.collection.property;

import java.io.Serializable;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.type.PropertyType;

/**
 * Represents a link to a particular node for a specific attribute.
 * 
 * @author Gianvito Morena
 *
 */
public class NodeProperty extends GeneralProperty implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -8199597191727514992L;
	
	private KuraNode node;	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public NodeProperty(){
		this.node = new KuraNode();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param node
	 */
	public NodeProperty(KuraNode node){
		super(PropertyType.KURA_NODE);
		this.node = node;
	}

	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public KuraNode getNode() {
		return node;
	}
	public void setNode(KuraNode node) {
		this.node = node;
	}
	
	/* ------------------------------------------
	 * --- Override methods ---
	 * ------------------------------------------ */
	@Override
    public boolean equals(Object obj) {
		if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final NodeProperty other = (NodeProperty) obj;
    	boolean sameNode = (this.node == other.node) || (this.node != null && this.node.equals(other.node));
    	if (!sameNode) return false;
        
        return true;
	}
	@Override
	public int hashCode(){
		return super.hashCode() + node.hashCode();	
	}
	@Override
	public String toString(){
		return node.getCompletePath();
	}
}
