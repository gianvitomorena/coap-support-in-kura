package net.gianvito.coap.collection.server;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import net.gianvito.coap.message.KuraNode;

/**
 * Collection used to link a node with all the attributes available for every
 * resource in it.
 * 
 * @author Gianvito Morena
 *
 */
public class LinkedNodeCollection implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 1467132257771356788L;
    
	private ConcurrentHashMap<KuraNode, AttributeCollection> linkedNodes;
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor
	 */
	public LinkedNodeCollection(){
		linkedNodes = new ConcurrentHashMap<KuraNode, AttributeCollection>();
	}
	
	/**
	 * Add a node to the collection.
	 * 
	 * @param node
	 * @param attributes
	 * @return
	 */
	public Boolean addNode(KuraNode node, AttributeCollection attributes){
		Boolean isAdded = false;
		
		if(containsKey(node)){
			linkedNodes.replace(node, attributes);
			isAdded = true;
		}else{
			linkedNodes.put(node, attributes);
			isAdded = true;
		}
		
		return isAdded;
	}
	
	/**
	 * Remove a node from the collection.
	 * 
	 * @param node
	 * @return
	 */
	public Boolean removeNode(KuraNode node){
		Boolean isRemoved = false;
		
		if(containsKey(node)){
			linkedNodes.remove(node);
			isRemoved = true;
		}
		
		return isRemoved;
	}
	
	/**
	 * Wrappers
	 */
	public Boolean containsKey(KuraNode node){
		try{
			return linkedNodes.containsKey(node);
		}catch(NullPointerException e){
			return false;
		}
	}
	public Set<KuraNode> getNodes(){
		return linkedNodes.keySet();
	}
	public AttributeCollection getCollection(KuraNode node){
		try{
			return linkedNodes.get(node);
		}catch(NullPointerException e){
			return new AttributeCollection();
		}
		
	}
	
	
	/**
	 * Utility for check of the version linked to a particular AttributeCollection stored in the local
	 * LinkedNodeCollection.
	 * 
	 * @param node
	 * @param newAttributes
	 * @return
	 */
	public Boolean checkIfArrivedIsNewer(KuraNode node, AttributeCollection newAttributes){
		if(containsKey(node)){
			return (linkedNodes.get(node).getVersion() < newAttributes.getVersion())? true: false;
		}else{
			return false;
		}
	}
	
	/**
	 * Returns the version of the AttributeCollection linked to the input node.
	 * 
	 * @param node
	 * @return
	 */
	public Long getAttributeVersion(KuraNode node){
		if(containsKey(node)){
			return linkedNodes.get(node).getVersion();
		}else{
			return (long) -1;
		}
	}
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public ConcurrentHashMap<KuraNode, AttributeCollection> getLinkedNodes() {
		return linkedNodes;
	}
	
	/**
	 * Returns a serializable version of the LinkedNodeCollection for
	 * external applications.
	 * 
	 * @return
	 */
	public HashMap<KuraNode, AttributeCollection> getSerializableNodes(){
		return new HashMap<KuraNode, AttributeCollection>(linkedNodes);
	}
	
	
	
}
