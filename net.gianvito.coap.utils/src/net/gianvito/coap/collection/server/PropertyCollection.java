package net.gianvito.coap.collection.server;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;

import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.californium.core.server.resources.ResourceAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Collection used to link LinkAttributes to every property (Resource and Node) which 
 * contains is.
 * 
 * @author Gianvito Morena
 *
 */
public class PropertyCollection implements Serializable{
	// Logger
    private static final transient Logger logger = LoggerFactory.getLogger(PropertyCollection.class);
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 1922428141704475955L;
	
	// ConcurrentHashMap to handle all the attributes
	private ConcurrentHashMap<LinkAttribute, Set<GeneralProperty>> resourceProperties;
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor
	 */
	public PropertyCollection(){
		resourceProperties = new ConcurrentHashMap<LinkAttribute, Set<GeneralProperty>>();
	}
	
	/**
	 * Add the resource properties of the new Resource, just connected to the server.
	 * 
	 * @param resource
	 * @return list of added properties
	 */
	public AttributeCollection addResourceProperties(Resource resource){
		// Added attributes
		AttributeCollection modifiedAttributes = new AttributeCollection();
		
		
		if(resource != null){
			// Get all the resource attributes from the Resource
			ResourceAttributes resAttributes = resource.getAttributes();
			// Build a new ResourcePropertyObject based on the attributes
			GeneralProperty resProperty = new ResourceProperty(resource);
			// Get the KeySet from the resource attributes
			Set<String> attrKeySet = resAttributes.getAttributeKeySet();
			
			for(String attributeName : attrKeySet){			
				List<String> attributeValues = resAttributes.getAttributeValues(attributeName);
				
				// Take all the values for the attribute
				for(String singleValue: attributeValues){
					LinkAttribute linkAttributeKey = new LinkAttribute(attributeName, singleValue);
					
					// If it's added in the correct way, insert a record in modifiedAttributes.
					if(addProperty(linkAttributeKey, resProperty)){
						modifiedAttributes.add(linkAttributeKey);
					}
				}
			}
		}else{
			logger.error("Resource to add is null.");
		}
		
		
		return modifiedAttributes;
	}
	
	/**
	 * Remove the resource properties of the removed Resource.
	 * 
	 * @param resource
	 * @return attributes without any ResourceProperty linked.
	 */
	public AttributeCollection removeResourceProperties(Resource resource){
		// Removed attributes
		AttributeCollection modifiedAttributes = new AttributeCollection();
		
		if(resource != null){
			// Get attributes to remove
			ResourceAttributes resAttributes = resource.getAttributes();
			// Create the object to remove
			GeneralProperty resProperty = new ResourceProperty(resource);
			// Get the KeySet from the resource attributes
			Set<String> attrKeySet = resAttributes.getAttributeKeySet();
			
			for(String attributeName : attrKeySet){
				// Build the key list of the data structure.
				List<String> attributeValues = resAttributes.getAttributeValues(attributeName);
				
				for(String singleValue: attributeValues){
					LinkAttribute linkAttributeKey = new LinkAttribute(attributeName, singleValue);
					
					// If there is no other resource linked, add that LinkAttribute to modifiedAttributes
					if(removeProperty(linkAttributeKey, resProperty)){
						modifiedAttributes.add(linkAttributeKey);
						resourceProperties.remove(linkAttributeKey);
					}
				}
			}	
		}else{
			logger.error("Resource to remove is null.");
		}
		
		return modifiedAttributes;
	}
	
	/**
	 * Add node properties to the collection.
	 * 
	 * @param node
	 * @return
	 */
	public AttributeCollection addNodeProperties(KuraNode node, AttributeCollection newAttributes){
		// Removed attributes
		AttributeCollection modifiedAttributes = new AttributeCollection();
		
		if(node != null && newAttributes != null){
			NodeProperty nodeProperty = new NodeProperty(node);
			
			for(LinkAttribute attribute : newAttributes.getLinkAttributes()){
				if(addProperty(attribute, nodeProperty)){
					modifiedAttributes.add(attribute);
				}
			}
		}
		
		return modifiedAttributes;
		
	}
	
	/**
	 * Remove node properties from the collection.
	 * 
	 * @param node
	 * @return 
	 */
	public AttributeCollection removeNodeProperties(KuraNode node, AttributeCollection oldAttributes){
		// Removed attributes
		AttributeCollection modifiedAttributes = new AttributeCollection();
		
		if(node != null && oldAttributes != null){
			NodeProperty nodeProperty = new NodeProperty(node);
			for(LinkAttribute attribute : oldAttributes.getLinkAttributes()){
				if(removeProperty(attribute, nodeProperty)){
					modifiedAttributes.add(attribute);
					resourceProperties.remove(attribute);
				}
			}
		}
		
		return modifiedAttributes;
	}
		
	
	
	/*
	 * ------------------------------------------
	 * --- ConcurrentHashMap methods ---
	 * ------------------------------------------
	 */	
	/**
	 * Add a property (Resource or Node) to the HashMap.
	 * 
	 * @return	true if property has been added.
	 */
	public Boolean addProperty(LinkAttribute attribute, GeneralProperty property){
		Boolean result = false;
		Set<GeneralProperty> actualResources;
		
		if(resourceProperties.containsKey(attribute)){
			actualResources = getProperties(attribute);
			if(actualResources == null){
				actualResources = new HashSet<GeneralProperty>();
			}
		}else{
			actualResources = new HashSet<GeneralProperty>();
			resourceProperties.put(attribute, actualResources);
		}
		
		if(property != null && actualResources != null){
			// TODO Possible duplicates handling.
			// TODO Add equals and hashCode override in ExtendedResource.
			
			// Returns true if it's the first resource added.			
			if(actualResources.add(property) && actualResources.size() == 1){
				result = true;
			}
		}
		
		return result;
	}
	
	/**
	 * Remove a property from the HashMap.
	 * 
	 * @return	true if there is no other resource with that property.
	 */
	public Boolean removeProperty(LinkAttribute attribute, GeneralProperty property){
		Boolean result = false;
		Set<GeneralProperty> actualResources;
		
		if(resourceProperties.containsKey(attribute)){
			actualResources = this.getProperties(attribute);
						
			if(actualResources.contains(property)){
				actualResources.remove(property);
				result = actualResources.isEmpty();
			}
		}		
		
		return result;
	}
	
	/**
	 * Returns properties for a specific LinkAttribute.
	 * 
	 * @param attribute
	 * @return
	 */
	public Set<GeneralProperty> getProperties(LinkAttribute attribute){
		return resourceProperties.get(attribute);
	}
	
	/**
	 * Returns keySet linked to the attributes. 
	 * 
	 * @return
	 */
	public Set<LinkAttribute> keySet(){
		return resourceProperties.keySet();
	}
	
	/**
	 * Wrapper for contains method.
	 * 
	 * @param property
	 * @return
	 */
	public Boolean containsKey(LinkAttribute property){
		return resourceProperties.containsKey(property);
	}	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public ConcurrentHashMap<LinkAttribute, Set<GeneralProperty>> getResourceProperties() {
		return resourceProperties;
	}
	
	/**
	 * Returns a serializable version of the AttributeCollection for
	 * external applications.
	 * 
	 * @return
	 */
	public AttributeCollection getAttributeCollection(){
		//HashSet<LinkAttribute> attributeC = new HashSet<LinkAttribute>(resourceProperties.keySet()); 
		return new AttributeCollection(resourceProperties.keySet());
	}
	
	/**
	 * Returns a serializable version of this collection.
	 * 
	 * @return
	 */
	public HashMap<LinkAttribute, Set<GeneralProperty>> getSerializableProperties(){
		return new HashMap<LinkAttribute, Set<GeneralProperty>>(resourceProperties);
	}

	public void setResourceProperties(
			ConcurrentHashMap<LinkAttribute, Set<GeneralProperty>> resourceProperties) {
		this.resourceProperties = resourceProperties;
	}
}
