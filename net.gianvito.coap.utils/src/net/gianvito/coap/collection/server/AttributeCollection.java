package net.gianvito.coap.collection.server;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.resources.LinkAttribute;

/**
 * Attribute collection for a specific Node.
 * 
 * @author Gianvito Morena
 *
 */
public class AttributeCollection implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 134216284467522665L;
	
	// Version used in the LinkedNodeCollection to remove old nodes
	private Long version;
	
	// Using a Set so we can automatically have duplicate removing.
	private Set<LinkAttribute> linkAttributes;
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructors
	 */
	public AttributeCollection(){
		linkAttributes = new HashSet<LinkAttribute>();
		// Update version with system milliseconds
		updateVersion();
	}
	public AttributeCollection(Collection<LinkAttribute> attributes){
		this.linkAttributes = new HashSet<LinkAttribute>(attributes);
		updateVersion();
	}
	
	public void updateVersion(){
		// Update version with system nanoseconds
		version = System.nanoTime();
	}
	
	
	/**
	 * Add wrapper. Update version.
	 * 
	 * @param attribute
	 * @return
	 */
	public Boolean add(LinkAttribute attribute){
		if(linkAttributes.add(attribute)){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Contains wrapper.
	 * 
	 * @param attribute
	 * @return
	 */
	public Boolean contains(LinkAttribute attribute){
		return linkAttributes.contains(attribute);
	}
	
	/**
	 * Remove wrapper. Update version.
	 * 
	 * @param attribute
	 * @return
	 */
	public Boolean remove(LinkAttribute attribute){
		if(linkAttributes.remove(attribute)){
			return true;
		}else{
			return false;
		}
		
	}
	
	/**
	 * Size wrapper.
	 * 
	 * @return
	 */
	public int size(){
		return linkAttributes.size();
	}
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public Set<LinkAttribute> getLinkAttributes() {
		return linkAttributes;
	}
	public void setAttributes(Set<LinkAttribute> attributes) {
		this.linkAttributes = (Set<LinkAttribute>) attributes;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	
	
	
	// TODO Possibili metodi per aggiornare le risorse senza inviarle per intero
}
