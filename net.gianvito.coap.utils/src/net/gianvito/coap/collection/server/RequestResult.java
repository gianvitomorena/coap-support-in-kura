package net.gianvito.coap.collection.server;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;

/**
 * Represents fields returned by 
 * {@link BrokerCollection#handleResourceRequest(net.gianvito.coap.message.CoAPMessage)} method.
 * 
 * @author Gianvito Morena
 *
 */
public class RequestResult implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -1606499571300204871L;
	
	String domain;
	String group;
	Set<ResourceProperty> resProperties;
	Set<NodeProperty> nodeProperties;
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public RequestResult(){
		this.domain = "";
		this.group = "";
		this.resProperties = new HashSet<ResourceProperty>();
		this.nodeProperties = new HashSet<NodeProperty>();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param domain
	 * @param group
	 * @param resProperties
	 * @param nodeProperties
	 */
	public RequestResult(String domain, String group, Set<ResourceProperty> resProperties,
							Set<NodeProperty> nodeProperties){
		this.domain = domain;
		this.group = group;
		this.resProperties = resProperties;
		this.nodeProperties = nodeProperties;
	}
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public Set<ResourceProperty> getResProperties() {
		return resProperties;
	}
	public void setResProperties(Set<ResourceProperty> resProperties) {
		this.resProperties = resProperties;
	}
	public Set<NodeProperty> getNodeProperties() {
		return nodeProperties;
	}
	public void setNodeProperties(Set<NodeProperty> nodeProperties) {
		this.nodeProperties = nodeProperties;
	}
	
	
	
}
