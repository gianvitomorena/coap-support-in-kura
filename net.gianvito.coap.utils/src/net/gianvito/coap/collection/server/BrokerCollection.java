package net.gianvito.coap.collection.server;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;
import net.gianvito.coap.utils.PathUtils;

import org.eclipse.californium.core.server.resources.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

/**
 * Collection used by broker to store informations about resources and linked nodes.
 * 
 * @author Gianvito Morena
 *
 */
public class BrokerCollection implements Serializable{
	// Logger
    private static final transient Logger logger = LoggerFactory.getLogger(BrokerCollection.class);
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 2743825498794838004L;
	
	private PropertyCollection resourceCollection;
	private LinkedNodeCollection linkedNodes;
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor
	 */
	public BrokerCollection(){
		// Build the resource Collection
    	resourceCollection = new PropertyCollection();
    	// Build the linked nodes Collection
    	linkedNodes = new LinkedNodeCollection();
	}
	
	
	/**
	 * Apply specific actions for handling a resource update.
	 * 
	 * @param linkedNode
	 * @param newResources
	 * @return
	 */
	public AttributeCollection handleResourceUpdate(KuraNode linkedNode, AttributeCollection newResources){
		AttributeCollection modifiedAttributes = new AttributeCollection();
		
		try{
			Objects.requireNonNull(linkedNode, "Linked node in input is null.");
			Objects.requireNonNull(newResources, "New resources in input are null.");
			
			logger.info("Resource changes from --> " + linkedNode.getCompletePath());
			
			/**** Need to check it's in memory and if it's updated to replace it. ****/
			if(linkedNodes.checkIfArrivedIsNewer(linkedNode, newResources)){
				logger.info("Update linked node.");
				
				// Replace old attributes.
				AttributeCollection removedAttributes = resourceCollection.removeNodeProperties(linkedNode, linkedNodes.getCollection(linkedNode));
				
				
				AttributeCollection addedAttributes = new AttributeCollection();
				// Check if there are new resources
				if(newResources.size() > 0){
					linkedNodes.addNode(linkedNode, newResources);
					addedAttributes = resourceCollection.addNodeProperties(linkedNode, newResources);
				}else{
					// There aren't new resources.
					// It's useless to store a node with no attribute in the collection.
					// It's useful when CTH is disconnected.
					// It could be useful when the same node connects again with other resources or on receiving an old message.
					linkedNodes.removeNode(linkedNode);
				}
				
				
				
				// Advanced check on real new resources or removed attributes
				if(removedAttributes.size() > 0 && addedAttributes.size() > 0){
					// The returned set contains all elements that are contained in either set1 or set2 but not in both.
					// It's zero when the two sets have same attributes.
					Set<LinkAttribute> tempDifference = new HashSet<LinkAttribute>();
					Sets.symmetricDifference(addedAttributes.getLinkAttributes(), removedAttributes.getLinkAttributes())
							.copyInto(tempDifference);
					// Update modifiedAttributes
					modifiedAttributes.setAttributes(tempDifference);
				}else if(removedAttributes.size() == 0){
					modifiedAttributes = addedAttributes;
				}else if(addedAttributes.size() == 0){
					modifiedAttributes = removedAttributes;
				}
				
			/**** If it's not in memory and it contains at least 1 object ****/
			}else if(!linkedNodes.containsKey(linkedNode) && newResources.size() > 0){
				Boolean isAdded = linkedNodes.addNode(linkedNode, newResources);
				
				// If node is added to the linked nodes, update local resources and
				//	send updates to parent node.
				if(isAdded){
					modifiedAttributes = resourceCollection.addNodeProperties(linkedNode, newResources);
					logger.info("Modified attributes --> " + modifiedAttributes.size());
				}
				
			/**** Case with already updated resources. ****/
			}else{
				logger.info("Duplicate message. Resources already updated. Version --> " +
								linkedNodes.getAttributeVersion(linkedNode) + " = " +
								newResources.getVersion());
			}
		}catch(NullPointerException e){
			logger.error("Null exception --> {}", e.getMessage());
		}
		
		return modifiedAttributes;
	}
	
	
	/**
	 * Apply specific actions for handling a resource request.
	 * 
	 * @param attributes
	 * @return RequestResult
	 */
	public RequestResult handleResourceRequest(KuraNode senderNode, AttributeCollection attributes){
		RequestResult result;
		
		// ResourceProperty to post on the Remote RD
		Set<ResourceProperty> resProperties = new HashSet<ResourceProperty>();
		// NodeProperty to send back to the query node
		Set<NodeProperty> nodeProperties = new HashSet<NodeProperty>();
		String domain = "";
		String group = "";
		boolean firstAttribute = true;
		
		try{
			Objects.requireNonNull(senderNode, "Sender node is null");
			Objects.requireNonNull(attributes, "Input attributes are null");
		
			// Search properties in the resource collection and add them in the properly set
			for(LinkAttribute attribute : attributes.getLinkAttributes()){
				// 1. Domain attribute handling
				if(attribute.getName().equals("domain")){
					domain = attribute.getValue();
					if(domain.equals("all")) continue; // Don't try to get properties for this.
				}
				// 2. Group attribute handling
				if(attribute.getName().equals("group")) group = attribute.getValue();
				
				// 3. General attribute handling
				Set<GeneralProperty> properties = resourceCollection.getProperties(attribute);
				
				// Temporary sets for intersection
				Set<ResourceProperty> tempResProperties = new HashSet<ResourceProperty>();
				Set<NodeProperty> tempNodeProperties = new HashSet<NodeProperty>();
				
				// Get all the properties for a specific attribute.
				if(properties != null){
					for(GeneralProperty property : properties){
						// Check the Resource Type. AND search.
						switch(property.getType()){
							case DIRECT_RESOURCE:
								if(firstAttribute) resProperties.add((ResourceProperty) property);
								tempResProperties.add((ResourceProperty) property);
								break;
							case KURA_NODE:
								// Check if node is in the same path of the sender.
								// If so don't add it to the properties because it's already been queried.
								NodeProperty nodeProperty = (NodeProperty) property; 
								if(PathUtils.checkNodeValidityForAdding(nodeProperty.getNode(), senderNode)){
									if(firstAttribute) nodeProperties.add(nodeProperty);
									tempNodeProperties.add(nodeProperty);
								}
								break;
						}
					}
					
				}else{
					logger.debug("No properties for --> {} = {}", attribute.getName(), attribute.getValue());
					logger.debug("AND search. Nothing to do here.");
				}
				
				// We added all properties for the first attribute. Now we apply intersection for AND search.
				firstAttribute = false;
				
				// Use Google Guava utils for intersection of two sets.
				resProperties = intersectionAndReturn(resProperties, tempResProperties);
				logger.debug("Intersection for {} (Resource) --> {}", attribute.getName(), resProperties.size());
				nodeProperties = intersectionAndReturn(nodeProperties, tempNodeProperties);
				logger.debug("Intersection for {} (Node) --> {}", attribute.getName(), nodeProperties.size());
			}
		
		}catch(NullPointerException e){
			logger.error("Null exception --> {}", e.getMessage());
		}
		
		// Build result object
		result = new RequestResult(domain, group, resProperties, nodeProperties);
		
		return result;
	}
	
	/**
	 * Remove child resources for a failed node.
	 * 
	 * @param notAvailableChild
	 * @return
	 */
	public AttributeCollection removeChild(KuraNode notAvailableChild){
		AttributeCollection modifiedAttributes = new AttributeCollection();
		AttributeCollection linkedNodeCollection = linkedNodes.getCollection(notAvailableChild);
		
		try{
			Objects.requireNonNull(notAvailableChild, "Input not available child is null,");
			
			// Remove node properties from PropertyCollection.
			modifiedAttributes = resourceCollection.removeNodeProperties(notAvailableChild, linkedNodeCollection);
			// Remove input node from LinkedNodeCollection.
			linkedNodes.removeNode(notAvailableChild);
		}catch(NullPointerException e){
			logger.error("Null exception --> {}" + e.getMessage());
		}
		
		return modifiedAttributes;
	}
	
	/**
	 * Add resource properties for a specific node.
	 * 
	 * @param resource
	 * @return
	 */
	public AttributeCollection addResourceProperties(Resource resource){
		return resourceCollection.addResourceProperties(resource);
	}
	
	/**
	 * Remove resource properties for a specific node.
	 * 
	 * @param resource
	 * @return
	 */
	public AttributeCollection removeResourceProperties(Resource resource){
		return resourceCollection.removeResourceProperties(resource);
	}
	
	/**
	 * Remove every attribute for linked nodes.
	 * Used in emergency state.
	 * 
	 * @return
	 */
	public void removeLinkedNodesAttributes(){
		for(KuraNode linkedNode : linkedNodes.getLinkedNodes().keySet()){
			removeChild(linkedNode);
		}
	}
	
	/**
	 * Use Guava method to apply intersection to input sets and return the result set.
	 * 
	 * @param set1
	 * @param set2
	 * @return
	 */
	public <E> Set<E> intersectionAndReturn(Set<E> set1, Set<E> set2){
		Set<E> tempSet = new HashSet<E>();
		
		Sets.intersection(set1, set2).copyInto(tempSet);
		
		return tempSet;
	}
	

	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	/**
	 * Wrapper for getting internal AttributeCollection.
	 * 
	 * @return
	 */
	public AttributeCollection getAttributeCollection(){
		return resourceCollection.getAttributeCollection();
	}
	public PropertyCollection getResourceCollection() {
		return resourceCollection;
	}
	public void setResourceCollection(PropertyCollection resourceCollection) {
		this.resourceCollection = resourceCollection;
	}
	public LinkedNodeCollection getLinkedNodes() {
		return linkedNodes;
	}
	public void setLinkedNodes(LinkedNodeCollection linkedNodes) {
		this.linkedNodes = linkedNodes;
	}
	
	
	
	
}
