package net.gianvito.coap.type;

/**
 * Possibile operations included in CoAPMessage exchanged.
 * 
 * @author Gianvito Morena
 *
 */
public enum CoAPOperation {
	// CTH operations
	CTH_ONLINE,
	// Root operations
	//ROOT_ADD,
	ROOT_ADDED,
	ROOT_REMOVE,
	//ROOT_REMOVED,
	ROOT_REQUEST,
	ROOT_RESPONSE,
	ROOT_NOT_AVAILABLE,
	ROOT_ON_NODE_UPDATED,
	// Node operations
	PARENT_ON_NODE_UPDATED,
	NODE_ADD,
	NODE_ADDED,
	NODE_REMOVE,
	NODE_REMOVED,
	PARENT_REQUEST,
	PARENT_RESPONSE,
	PARENT_AVAILABLE,
	PARENT_NOT_AVAILABLE,
	CHILD_NOT_AVAILABLE,
	// Resource operations
	RESOURCE_UPDATE,
	//RESOURCE_UPDATED,
	RESOURCE_QUERY_REQUEST,
	RESOURCE_QUERY_RESPONSE
}
