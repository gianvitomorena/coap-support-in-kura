package net.gianvito.coap.type;

/**
 * Property type stored in the ResourceCollection.
 * 
 * @author Gianvito Morena
 *
 */
public enum PropertyType{
	DIRECT_RESOURCE,
	KURA_NODE
}
