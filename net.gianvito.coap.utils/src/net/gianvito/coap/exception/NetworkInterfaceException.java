package net.gianvito.coap.exception;

public class NetworkInterfaceException extends Exception{
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -5540880103007687280L;
	
	private static String errorMessage = "No network address available for selected network interface.";
	
	/* ----------------------- Methods ----------------------------- */
	
	public NetworkInterfaceException(){
		super(errorMessage);
	}
	
	public NetworkInterfaceException(String text){
		super(text);
	}
}
