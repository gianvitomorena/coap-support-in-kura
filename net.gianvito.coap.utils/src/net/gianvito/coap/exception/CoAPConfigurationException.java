package net.gianvito.coap.exception;

/**
 * Wrong broker configuration exception.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPConfigurationException extends Exception{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = -4718823039580069371L;
	
	private static String errorMessage = "Wrong broker configuration.";
	
	/* ----------------------- Methods ----------------------------- */
	
	public CoAPConfigurationException(){
		super(errorMessage);
	}
	
	public CoAPConfigurationException(String text){
		super(text);
	}
}
