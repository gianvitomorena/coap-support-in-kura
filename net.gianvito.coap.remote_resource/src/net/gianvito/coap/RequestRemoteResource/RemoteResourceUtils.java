package net.gianvito.coap.RequestRemoteResource;

import java.util.List;

import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;

/**
 * Utilities used by RemoteResource to manipulate payload and parse queries.
 * 
 * @author Gianvito Morena
 *
 */
public class RemoteResourceUtils {
	
	/**
	 * Build payload using node properties for posting to remote Resource Directory.
	 * 
	 * @param parentNode
	 * @param attributes
	 * @return
	 */
	public static String buildRequestPayload(KuraNode parentNode, AttributeCollection attributes){
		String payload = "";
		
		try{
			payload = "Request sent to --> " + parentNode.getCompletePath() + "\n\n";
		}catch(NullPointerException e){
			payload = "Resource request error. No available parent";
		}
		
		payload += "Attributes:\n";
		
		for(LinkAttribute attribute : attributes.getLinkAttributes()){
			payload += " - " + attribute.getName() + "=" + attribute.getValue() + "\n";
		}
		
		
		return payload;
	}
	
	/**
	 * Parse request optimized for Domain and Group handling.
	 * 
	 * @param queries
	 * @return
	 */
	public static AttributeCollection parsePlusDG(KuraNode serverNode, List<String> queries){
		AttributeCollection attributes = new AttributeCollection();
		String equalSeparator = "=";
		// Domain and group String for evaluation
		String domain = null;
		String group = null;
		
		for (String query : queries) {
			LinkAttribute attr = LinkAttribute.parse(query);
			if(attr.getName().equals("domain") || attr.getName().equals("group")){
				int pos = query.lastIndexOf(equalSeparator);
				attr.setValue(query.substring(pos + 1));
				
				if(attr.getName().equals("domain")){
					domain = attr.getValue();
				}
				if(attr.getName().equals("group")){
					group = attr.getValue();
				}
			}
			
			//logger.info(attr.getValue());
			//logger.info(query);
			attributes.add(attr);
		}
		
		// Adjust domain and group variables
		if(domain == null && group == null){
			domain = serverNode.getDomain();
			group = serverNode.getGroup();
			attributes.add(new LinkAttribute("domain", domain));
			attributes.add(new LinkAttribute("group", group));
		}else{
			if(domain != null && group == null){
			// Do nothing. Domain search
			}else if(domain == null && group != null){
				attributes.add(new LinkAttribute("domain", "all"));
			}
		}
		
		return attributes;
	}
}
