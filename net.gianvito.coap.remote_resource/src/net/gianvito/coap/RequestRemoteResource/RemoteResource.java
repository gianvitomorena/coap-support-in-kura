package net.gianvito.coap.RequestRemoteResource;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.gianvito.coap.client.CoAPRemoteClient;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.mqtt.CoAPTopic;
import net.gianvito.coap.server.CoAPServer;
import net.gianvito.coap.server.mqtt.CoAPPublisher;
import net.gianvito.coap.type.CoAPOperation;
import net.gianvito.coap.utils.ByteObjectUtils;
import net.gianvito.coap.utils.MessageUtils;
import net.gianvito.coap.utils.PathUtils;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.ConcurrentCoapResource;
import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.kura.KuraException;
import org.eclipse.kura.KuraNotConnectedException;
import org.eclipse.kura.KuraStoreException;
import org.eclipse.kura.KuraTooManyInflightMessagesException;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataTransportService;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CoAPResource used to request remote Resources.
 * 
 * @author Gianvito Morena
 *
 */
public class RemoteResource extends CoapResource{
	
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(RemoteResource.class);
	
	// CoAPServer
	private CoAPServer cs;
	private DataService ds;
	private DataTransportService dataTransportService;
	
	
	// Executor Service
    ExecutorService executor;
    private final int REMOTE_RESOURCE_THREADS = 1;
    
    //MQTTClient client;
	
    
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public RemoteResource() {
		super(CoAPKey.REMOTE_RESOURCE_NAME);
	}

	/*
	 * Activate method.
	 * 
	 * @param componentContext
	 */
	protected void activate(ComponentContext componentContext){
		
		// Create an executor.
        executor = Executors.newFixedThreadPool(REMOTE_RESOURCE_THREADS);
//        client = new MQTTClient();
//        client.connect(cs.getServerNode());
//        logger.info("Sottoscrizione al topic");
//        client.subscribe(cs.getServerNode());
        // FIXME Test.
		//this.setExecutor(executor);
        cs.addSpecialResource(this);
        logger.info("RemoteQueryResource added.");
	}
	
	/**
	 * Deactivate method.
	 * 
	 * @param componentContext
	 */
	protected void deactivate(ComponentContext componentContext){
		
		//client.disconnect();
        executor.shutdown();
		cs.removeSpecialResource(this);
		logger.info("RemoteQueryResource removed.");
	}
	
	/**
	 * GET remote resources with defined attributes.
	 */
	@Override
	public void handleGET(CoapExchange exchange) {
		
		// Attributes to retrieve
		AttributeCollection attributes = new AttributeCollection();
		// Get the current serverNode
		KuraNode serverNode = cs.getServerNode();
		// Query attributes to parse
		List<String> queries = exchange.getRequestOptions().getURIQueries();
		// Parse queries and add domain and group properties
		attributes = RemoteResourceUtils.parsePlusDG(serverNode, queries);

		if((serverNode != null) && (serverNode.getParentNode() != null) && 
				(!serverNode.getParentNode().equals(serverNode))){
	
			
			// Publish a resource query on parent node
			executor.execute(new RequestRunnable(serverNode, attributes, ResourceRequestType.PARENT));
			
			// Start a local search
			executor.execute(new RequestRunnable(serverNode, attributes, ResourceRequestType.LOCAL));
			
			exchange.respond(ResponseCode.CREATED, 
					RemoteResourceUtils.buildRequestPayload(serverNode.getParentNode(), attributes));
		}else{
			// Start a local search
			executor.execute(new RequestRunnable(serverNode, attributes, ResourceRequestType.LOCAL));
			
			exchange.respond(ResponseCode.CREATED,
					"This node is not connected to the brokers tree or its parent is null.\n"
					+ "Start a local and children search.");
		}		
	}
	
	/**
	 * Post local resources to Resource Directory.
	 */
	@Override
	public void handlePOST(CoapExchange exchange) {	
		// Debug method. Put all the local resources in the Resource Directory
		//CoAPRemoteClient client = new CoAPRemoteClient();
		//KuraNode localNode = new KuraNode();
		
		// Separate response
		exchange.accept();
		
		HashSet<Resource> resources = new HashSet<Resource>();
		
		// Post local resources
		for(Resource resource : cs.getLocalResources()){
			resources.add(resource);
		}
		
		CoapResponse result = CoAPRemoteClient.postLocalResources(resources);
		
		logger.info("Result of the local resources post --> " + result.getResponseText() +
							" - Code --> " + result.getCode());
		
		exchange.respond(ResponseCode.CREATED);
	}
	
	/**
	 * Start a local search.
	 * 
	 * @param attributes
	 */
	public void sendLocalSearch(AttributeCollection attributes){
		// Create a local message to request a Resource query.
		CoAPMessage localSearchMessage = new CoAPMessage(new Long(0),
									cs.getServerNode(), attributes, CoAPOperation.RESOURCE_QUERY_REQUEST);
		// Call the correct method
		cs.executeRequest(localSearchMessage);
	}

	
	/* ------------------------------------------
	 * --- Services ---
	 * ------------------------------------------ */
	protected void setCoAPServer(CoAPServer cs){
		this.cs = cs;
	}
	protected void unsetCoAPServer(CoAPServer cs){
		this.cs = null;
	}
	public void setDataService(DataService dataService) {
		this.ds = dataService;
	}
	public void unsetDataService(DataService dataService) {
		this.ds = null;
	}
	protected void setDataTransportService(DataTransportService dataTransportService){
		this.dataTransportService = dataTransportService;
	}
	protected void unsetDataTransportService(DataTransportService dataTransportService){
		this.dataTransportService = null;
	}
	
	/**
	 * Executor Runnable for resource query handling.
	 * 
	 * @author Gianvito Morena
	 *
	 */
	class RequestRunnable implements Runnable{
		
		KuraNode serverNode;
		AttributeCollection attributes;
		ResourceRequestType type;
		
		public RequestRunnable(KuraNode serverNode, AttributeCollection attributes, ResourceRequestType type){
			this.serverNode = serverNode;
			this.attributes = attributes;
			this.type = type;
		}
		
		@Override
		public void run() {
			switch(type){
				case PARENT:
					// Publish a resource query on parent node
					//new CoAPPublisher(ds, cs.getMessagesId()).publishResourceQueryRequest(serverNode, attributes);
					cs.getCoapPublisher().publishResourceQueryRequest(serverNode, attributes);
					
					//client.publishResourceQueryRequest(serverNode, attributes);
					break;
				case LOCAL:
					// Start a local search
					sendLocalSearch(attributes);
					break;
			}
		}
	}

	
}
