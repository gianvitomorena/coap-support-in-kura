package net.gianvito.coap.californium.server;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import net.gianvito.coap.dtls.CoAPDTLSOptions;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.resources.RDLookUpTopResource;
import net.gianvito.coap.resources.RDResource;
import net.gianvito.coap.resources.RDTagTopResource;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoAPEndpoint;
import org.eclipse.californium.core.network.Endpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.network.interceptors.MessageTracer;
import org.eclipse.californium.core.server.resources.ConcurrentCoapResource;
import org.eclipse.californium.elements.ConnectorBase;
import org.eclipse.californium.scandium.DTLSConnector;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaliforniumServer {
	
	/* ----------------------------- Attributes ------------------------------------- */
	
	// Logger
    private static final Logger logger = LoggerFactory.getLogger(CaliforniumServer.class);
	// APP ID
    private static final String APP_ID = "net.gianvito.coap.californium.server.CaliforniumServer";
	
	// CoAP Server
    private CoapServer coapServer;
    private Integer coapPort;
	// CoAP Server resources
    private CoapResource localResources;
    private RDResource rdResource;
    
    private ScheduledExecutorService executor;
    private final int CALIFORNIUM_EXECUTOR_THREADS = 3;
    
    
    // DTLS server
    DTLSServer dtlsS;
    
    
    /* ----------------------------- Methods ------------------------------------- */ 
    
    /**
     * Constructor.
     */
	public CaliforniumServer(){
		coapPort = CoAPKey.COAP_DEFAULT_PORT;
	}
	
	
	/* -------------------------------
     * Component methods
     * ------------------------------- */ 
    /**
     * Activate method.
     * 
     * @param componentContext
     * @param properties
     */
    protected void activate(ComponentContext componentContext, Map<String,Object> properties) {
    	logger.info("Bundle " + APP_ID + " has started!");
    	
    	executor = Executors.newScheduledThreadPool(CALIFORNIUM_EXECUTOR_THREADS);
      	    	
        coapServer = new CoapServer(coapPort);
        coapServer.setExecutor(executor);
        
        // Initialize resources.
        initializeResources();
        // Add resources to the server.
        addRootResources();
                        
        // Start the server
  		coapServer.start();
    }
	
    /**
     * Updated method.
     * 
     * @param componentContext
     * @param properties
     */
    public void updated(ComponentContext componentContext, Map<String, Object> properties) {
    	
    }
    
    /**
     * Deactivate method.
     * 
     * @param componentContext
     */
    protected void deactivate(ComponentContext componentContext) {
    	executor.shutdownNow();
    	stopDTLSConnector();
    	coapServer.stop();
    	logger.info("Bundle " + APP_ID + " has been stopped!");
    }
    
    public void initializeResources(){
    	// Primary resource.
    	localResources = new ConcurrentCoapResource("local");
    	// Resource Directory resource
    	rdResource = new RDResource();
    }
    
    public void addRootResources(){
    	// Local resources
    	coapServer.add(localResources);
        // Resource Directory
 		coapServer.add(rdResource);
 		// Lookup resources
 		coapServer.add(new RDLookUpTopResource(rdResource));
 		coapServer.add(new RDTagTopResource(rdResource));
    }
    
    public void addResource(CoapResource resource){
    	localResources.add(resource);
    }
    
    public void addRootResource(CoapResource resource){
    	coapServer.add(resource);
    }
    
    public void removeResource(CoapResource resource){
    	localResources.remove(resource);
    }
    
    public void removeRootResource(CoapResource resource){
    	coapServer.remove(resource);
    }
	
	public void setPort(Integer port){
		reboot(port);
	}
	
	/**
	 * Reboot server with a specified port.
	 * 
	 * @param port
	 */
	public void reboot(Integer port){
		if(coapServer != null && coapServer.getEndpoints().size() != 0){
			executor.shutdownNow();
			coapServer.stop();
		}
		coapServer = new CoapServer(port);
		executor = Executors.newScheduledThreadPool(CALIFORNIUM_EXECUTOR_THREADS);
		coapServer.setExecutor(executor);
		
		initializeResources();
		addRootResources();
		
		coapServer.start();
	}
	
	/**
	 * Reboot server with a specified port.
	 * 
	 * @param port
	 */
	public void rebootAsDTLS(Integer port, CoAPDTLSOptions dtlsOpt){
		if(coapServer != null && coapServer.getEndpoints().size() != 0){
			executor.shutdownNow();
			coapServer.stop();
			stopDTLSConnector();
		}
		
		coapServer = new CoapServer();
		executor = Executors.newScheduledThreadPool(CALIFORNIUM_EXECUTOR_THREADS);
		coapServer.setExecutor(executor);
		
		addDTLSConnector(port, dtlsOpt);
		
		initializeResources();
		addRootResources();

		coapServer.start();
	}
	
	/**
	 * Stop server.
	 */
	public void stop(){
		coapServer.stop();
	}
	
	public void addDTLSConnector(int coapsPort, CoAPDTLSOptions dtlsOpt){
		dtlsS = new DTLSServer(coapsPort, dtlsOpt);
		if(dtlsS.getDtlsConnector() != null){
			coapServer.addEndpoint(new CoAPEndpoint(dtlsS.getDtlsConnector(), NetworkConfig.getStandard()));
		}
	}
	
	public void startDTLSConnector(){
		dtlsS.start();
	}
	
	public void stopDTLSConnector(){
		if(dtlsS != null && dtlsS.isRunning()){
			dtlsS.stop();
		}
	}
	
	
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public CoapResource getLocalResources(){
		return localResources;
	}
	public CoapServer getCoapServer() {
		return coapServer;
	}
	public void setCoapServer(CoapServer coapServer) {
		this.coapServer = coapServer;
	}
	public Integer getCoapPort() {
		return coapPort;
	}
	public void setCoapPort(Integer coapPort) {
		this.coapPort = coapPort;
	}
	public void setLocalResources(CoapResource localResources) {
		this.localResources = localResources;
	}
}
