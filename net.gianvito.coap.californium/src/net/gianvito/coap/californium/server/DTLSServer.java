package net.gianvito.coap.californium.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.logging.Level;







import net.gianvito.coap.dtls.CoAPDTLSOptions;
import net.gianvito.coap.mqtt.CoAPKey;

import org.eclipse.californium.elements.Connector;
import org.eclipse.californium.elements.ConnectorBase;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.ScandiumLogger;
import org.eclipse.californium.scandium.dtls.pskstore.InMemoryPskStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DTLSServer {
	
	/* ----------------------------- Attributes ------------------------------------- */
	
	// Logger
    private static final Logger logger = LoggerFactory.getLogger(DTLSServer.class);
	
	static {
		ScandiumLogger.initialize();
		ScandiumLogger.setLevel(Level.SEVERE);
	}
	
	CoAPDTLSOptions dtlsOptions;
	int coapsPort = CoAPKey.COAPS_DEFAULT_PORT;
	private Connector dtlsConnector;
	
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public DTLSServer(int coapsPort, CoAPDTLSOptions dtlsOptions){
		this.coapsPort = coapsPort < 5683? this.coapsPort : coapsPort;
		
		// Pre-shared secrets
		InMemoryPskStore pskStore = new InMemoryPskStore();
		pskStore.setKey(dtlsOptions.getPSKIdentity(), dtlsOptions.getPSKPassword());
	   
	    try {
	        // load key store
	        KeyStore keyStore = KeyStore.getInstance("JKS");
	        InputStream in = new FileInputStream(dtlsOptions.getKeyStoreLocation());
	        keyStore.load(in, dtlsOptions.getkSPassword());
	        logger.info("keyStore loaded.");
	            
	        // load trust store
	        KeyStore trustStore = KeyStore.getInstance("JKS");
	        InputStream inTrust = new FileInputStream(dtlsOptions.getTrustStoreLocation());
	        trustStore.load(inTrust, dtlsOptions.gettSPassword());
	        logger.info("trustStore loaded.");
	                    
	        // You can load multiple certificates if needed
	        Certificate[] trustedCertificates = new Certificate[1];
	        trustedCertificates[0] = trustStore.getCertificate(dtlsOptions.getRootCertificateIdentity());

	        dtlsConnector = new DTLSConnector(new InetSocketAddress(this.coapsPort), (java.security.cert.Certificate[]) trustedCertificates);
	        PrivateKey serverKey = (PrivateKey)keyStore.getKey(
	        		dtlsOptions.getkSServerIdentity(),
	        		dtlsOptions.getkSPassword());

	        ((DTLSConnector) dtlsConnector).getConfig().setPrivateKey(
	        		serverKey,
	        		keyStore.getCertificateChain(dtlsOptions.getkSServerIdentity()),
	        		true);
	        ((DTLSConnector) dtlsConnector).getConfig().setPskStore(pskStore);

	        
         } catch (Throwable e) {
             logger.error(e.getMessage(), e);
         }
	}
	
	/**
	 * Start method.
	 */
	public void start() {
		try {
			dtlsConnector.start();
		} catch (Throwable e) {
			logger.error("Unexpected error starting the DTLS UDP server",e);
		}
	}
	
	/**
	 * Stop method.
	 */
	public void stop(){
		dtlsConnector.stop();
	}

	
	public boolean isRunning(){
		if(dtlsConnector != null && ((ConnectorBase) dtlsConnector).isRunning()){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	public Connector getDtlsConnector() {
		return dtlsConnector;
	}
	
	
	
}
