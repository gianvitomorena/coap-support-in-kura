package net.gianvito.coap.cth;


import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.gianvito.coap.collection.cth.CTHCollection;
import net.gianvito.coap.collection.cth.DevicesMap;
import net.gianvito.coap.collection.cth.RemoveResult;
import net.gianvito.coap.cth.debug.CTHPrinter;
import net.gianvito.coap.cth.mqtt.CTHPublisher;
import net.gianvito.coap.cth.mqtt.CTHSubscriber;
import net.gianvito.coap.exception.CoAPConfigurationException;
import net.gianvito.coap.exception.NetworkInterfaceException;
import net.gianvito.coap.listener.cth.CTHListener;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.mqtt.CoAPTopic;
import net.gianvito.coap.system.SystemProperties;
import net.gianvito.coap.utils.ByteObjectUtils;
import net.gianvito.coap.utils.MessageUtils;

import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataServiceListener;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main CoAP Broker tree handler.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPTreeHandler implements ConfigurableComponent, DataServiceListener,
											CTHListener{
	
	/* ----------------------- Attributes -------------------------- */
	
	// Logger
	private static final Logger logger = LoggerFactory.getLogger(CoAPTreeHandler.class);
	private static final String APP_ID = "net.gianvito.coap.CoAPTreeHandler";
	
	// Root ID. Used in tree identification.
	//private static final String ROOT_ID = "/root";
	

	/* ---- Interface properties ---- */
    private Map<String, Object> uiProperties;
    private static final String CTH_ON = "cth.on";
    private static final String CTH_DOMAIN = "cth.domain";
    private static final String CTH_GROUP = "cth.group";
    private static final String CTH_NETWORK_INTERFACE = "cth.network_interface";
	
	// CoapTreeHandler properties
    private Boolean cthOn;
	private String group;
	private String domain;
	private String networkInterface;
	
	// Helper classes
	private CTHPublisher cthPublisher;
	private CTHSubscriber cthSubscriber;
	
	// Services
	private DataService dataService;
	private SystemProperties systemProperties;
	
	// Devices Tree
	private CTHCollection cthCollection;
	
	// Kura Node
	private KuraNode cthNode;
	private long messagesId;
	
	// Executor services
	private ExecutorService callbackExecutor;
	private ExecutorService requestExecutor;
	
	private final int REQUEST_EXECUTOR_THREADS = 3;
	
	
	/* ----------------------- Methods ----------------------------- */
	/*
	 * ------------------------------------------
	 * --- Component methods ---
	 * ------------------------------------------
	 */
	/**
	 * Constructors.
	 */
	public CoAPTreeHandler(){
		cthCollection = new CTHCollection();
		cthSubscriber = new CTHSubscriber();
	}
	
	
	
	/**
	 * Activate Method.
	 * 
	 * @param componentContext
	 * @param properties
	 */
	protected void activate(ComponentContext componentContext, Map<String, Object> properties){
		// Create executors.
		callbackExecutor = Executors.newSingleThreadExecutor();
		requestExecutor = Executors.newFixedThreadPool(REQUEST_EXECUTOR_THREADS);
		
		// Update properties from UI.
		this.uiProperties = properties;
        updatePropertiesFromUI();
        
        if(isActive()){
        	cthCollection = new CTHCollection();
        	logger.info("{} activated.", APP_ID);
            // If DataService is connected subscribes to topics.
            if(dataService.isConnected()){
            	try{
            		updateNodeProperties();
	            	// Publisher creation
	             	cthPublisher = new CTHPublisher(dataService, messagesId);
	             	// Send CTH online message.
	             	cthPublisher.publishAvailableCTH(cthNode);
	             	cthSubscriber.subscribeToTopics(dataService);
            	}catch(NetworkInterfaceException e){
            		logger.error(e.getMessage());
            	}
            }

     		// TODO Possible utils --> Check if there are other CoAPTreeHandler
        }
	}
	
	/**
	 * Deactivate Method.
	 * 
	 * @param componentContext
	 */
	protected void deactivate(ComponentContext componentContext){
		callbackExecutor.shutdown();
		requestExecutor.shutdown();
		cthSubscriber.unsubscribeToTopics(dataService);
		logger.info("{} deactivated.", APP_ID);
	}
	
	/**
	 * Updated method.
	 * 
	 * @param componentContext
	 * @param properties
	 */
    public void updated(ComponentContext componentContext, Map<String, Object> properties) {
        this.uiProperties = properties;
        updatePropertiesFromUI();
    }
    
	
    /**
     * Update the properties from UI.
     */
    public void updatePropertiesFromUI(){
    	cthOn = (Boolean) uiProperties.get(CTH_ON);
    	domain = (String) uiProperties.get(CTH_DOMAIN);
    	group = (String) uiProperties.get(CTH_GROUP);
    	networkInterface = (String) uiProperties.get(CTH_NETWORK_INTERFACE);
    }
    
    /**
     * Update local KuraNode properties.
     * 
     * @throws NetworkInterfaceException 
     */
    public void updateNodeProperties() throws NetworkInterfaceException{
    	// Create the CoAPTreeHandler node
    	cthNode = new KuraNode(systemProperties, networkInterface, null, null, domain, group);
    	
    	messagesId = MessageUtils.getFirstHalfSerialNumber(systemProperties, networkInterface);
    	
    	if(!cthNode.isCurrentlySet()){
    		throw new NetworkInterfaceException();
    	}
    }
    

	/*
	 * ------------------------------------------
	 * --- DataService callback methods ---
	 * ------------------------------------------
	 */
	/**
	 * Notifies that the DataService has established a connection.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onConnectionEstablished()
	 */
	@Override
	public void onConnectionEstablished() {
		if(isActive()){
			//cthCollection = new CTHCollection();
			try{
        		updateNodeProperties();
            	// Publisher creation
             	cthPublisher = new CTHPublisher(dataService, messagesId);
             	// Send CTH online message.
             	cthPublisher.publishAvailableCTH(cthNode);
             	cthSubscriber.subscribeToTopics(dataService);
        	}catch(NetworkInterfaceException e){
        		logger.error(e.getMessage());
        	}
		}
	}
	
	/**
	 * Notifies that the DataService is about to cleanly disconnect.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onDisconnecting()
	 */
	@Override
	public void onDisconnecting() {
		logger.info("CoAPTreeHandler is not available.");
	}
	
	/**
	 * Notifies that the DataService has cleanly disconnected.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onDisconnected()
	 */
	@Override
	public void onDisconnected() {
		logger.info("CoAPTreeHandler has disconnected.");
	}
	
	/**
	 * Notifies that the DataService has unexpectedly disconnected.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onConnectionLost(java.lang.Throwable)
	 */
	@Override
	public void onConnectionLost(Throwable cause) {
		
	}
	
	/**
	 * Notifies a message arrival.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onMessageArrived(java.lang.String, byte[], int, boolean)
	 */
	@Override
	public void onMessageArrived(String topic, byte[] payload, int qos, boolean retained) {		
		if(topic.startsWith(CoAPTopic.MQTT_LWT_PREFIX)){
			KuraNode node = cthCollection.findDeviceByMAC(topic.replace(CoAPTopic.MQTT_LWT_PREFIX, ""));
			if (node != null) onNodeRemove(node);
		}else if(cthSubscriber.checkTopic(topic)){
			try {
				CoAPMessage message = ByteObjectUtils.getObject(payload, CoAPMessage.class);
				// Select the operation to execute
				switch(message.getOperation()){
					case ROOT_REQUEST:
						executeCallback(message);
						break;
					case ROOT_REMOVE:
						executeCallback(message);
						break;
					case PARENT_REQUEST:
						executeRequest(message);
						break;
					case NODE_ADD:
						executeCallback(message);
						break;
					case NODE_REMOVE:
						executeCallback(message);
						break;
					case ROOT_ON_NODE_UPDATED:
						executeCallback(message);
						break;
					case PARENT_ON_NODE_UPDATED:
						executeCallback(message);
						break;					
					default:
						break;
				}
			} catch (IOException e) {
				logger.error(e.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Notifies that a message has been published.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onMessagePublished(int, java.lang.String)
	 */
	@Override
	public void onMessagePublished(int messageId, String topic) {
		
	}
	
	/**
	 * Confirms message delivery to the broker.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onMessageConfirmed(int, java.lang.String)
	 */
	@Override
	public void onMessageConfirmed(int messageId, String topic) {
		
	}
	
	
	/*
	 * ------------------------------------------
	 * --- CoAPTreeHandler callback ---
	 * ------------------------------------------
	 */
	/**
	 * Check the type of the root request and process the correct action.
	 * Called on ROOT_REQUEST message.
	 * 
	 * @param message
	 * @return
	 * @throws CoAPConfigurationException 
	 */
	public Boolean onRootRequest(CoAPMessage message) throws CoAPConfigurationException{
		Boolean operationResult = false;
		// Check for sender's group and domain
		KuraNode sender = message.getSender();
		DevicesMap devices = cthCollection.getDevices();
		
		// Root request from a possible root node
		if(sender.getLevel() == 0){
			// Root already present
			if(devices.containsRoot()){
				// Check if it's the root itself. Already been added.
				if(!devices.getRoot().equals(sender)){
					// Return the root to the sender
					cthPublisher.publishAvailableRoot(cthNode, devices.getRoot(), sender);
				}
			// Possible new root
			}else{
				// Add the sender as root in the internal HashMap and publish the new root
				if(cthCollection.addRoot(sender) == null){
					// Publish the new root
					cthPublisher.publishAddedRoot(cthNode, sender);
				}else{
					// Return the root to the sender. ROOT_RESPONSE
					cthPublisher.publishAvailableRoot(cthNode, devices.getRoot(), sender);
				}
			}
		// Root request from a level 1 node.
		}else if(sender.getLevel() == 1){
			// First level node
			if(devices.containsRoot()){
				// It's useless to return an already set root parent.
				if(sender.getParentNode() == null || !sender.getParentNode().equals(devices.getRoot())){
					// Return the root to the sender
					cthPublisher.publishAvailableRoot(cthNode, devices.getRoot(), sender);
				}
			}else{
				// No root available to return to the first level node.
				// This message is important so every node is aware of this new situation.
				cthPublisher.publishNotAvailableRoot(cthNode, sender);
				cthCollection.updateRootOnNode(sender, null);
			}
			// Add the new device if it's not already in the tree. Useless
			//cthCollection.addNode(sender);
		}else{
			throw new CoAPConfigurationException("Root request with a wrong configuration.");			
		}
		
		return operationResult;
	}
	
	// NEVER USED
	public Boolean onRootRemove(CoAPMessage message){
		Boolean operationResult = false;
		
		RemoveResult result = cthCollection.removeRoot(message.getSender());
		Boolean removeResult = result.getOperationResult();
		KuraNode removedRoot = result.getRemovedNode();
		Set<KuraNode> rootChildren = result.getNodeChildren();
		
		CTHPrinter.printRemoveResult(result);
		
		if(removeResult){
			// Publish a message to the ROOT_NOT_AVAILABLE topic so every node on the first level can see it.
			//cthPublisher.publishRemovedRoot(removedRoot);
			cthPublisher.publishNotAvailableRoot(cthNode, removedRoot);
			
			// Notify the previous children.
			cthPublisher.publishNotAvailableParent(cthNode, rootChildren, message.getSender());
			
			operationResult = true;
		}
		
		
		return operationResult;
	}
	
	public Boolean onNodeAdd(CoAPMessage message){
		Boolean operationResult = false;
		
		KuraNode parentNode = cthCollection.addNodeRequest(message.getSender());
		
		if(parentNode != null){
			cthPublisher.sendParentNode(cthNode, parentNode, message.getSender());
		}
		
		return operationResult;
	}
	
	public Boolean onNodeRemove(CoAPMessage message){
		Boolean operationResult = false;
		
		onNodeRemove(message.getSender());
		
		return operationResult;
	}
	
	/**
	 * Node remove used with LWT failed nodes.
	 * 
	 * @param node
	 * @return
	 */
	public Boolean onNodeRemove(KuraNode node){
		Boolean operationResult = false;
		
		RemoveResult result = cthCollection.removeNode(node);
		
		Boolean removeResult = result.getOperationResult();
		KuraNode removedNode = result.getRemovedNode();
		Set<KuraNode> nodeChildren = result.getNodeChildren();
		
		CTHPrinter.printRemoveResult(result);
		
		// If the operation has been successful.
		if(removeResult && removedNode != null){
			// Check if removed node is a root one and if it it was the current one.
			if(removedNode.getRelativePath().equals(CoAPKey.ROOT_ID) && cthCollection.getRootNode() == null){
				// Publish a message to the ROOT_NOT_AVAILABLE topic so every node on the first level can see it.
				cthPublisher.publishNotAvailableRoot(cthNode, removedNode);
			}else{
				// Notify the parent of the failed node for node with level greater than 1.
				// 	Valid also for duplicate nodes.
				cthPublisher.publishNotAvailableChild(cthNode, removedNode.getParentNode(), removedNode);
			}
			
			// Notify the previous children. It's important for node with level greater than 2
			//  linked to root node.
			cthPublisher.publishNotAvailableParent(cthNode, nodeChildren, removedNode);
			
			operationResult = true;
		}
		
		return operationResult;
	}
	
	/**
	 * Allows to handle a parent request in the proper way.
	 * 
	 * @param message
	 * @return
	 */
	public Boolean onParentRequest(CoAPMessage message){
		Boolean operationResult = false;
		KuraNode sender = message.getSender();
		String relativeSearchPath = sender.getRelativePath();
		DevicesMap devices = cthCollection.getDevices();
		
		KuraNode currentNodeAtPath = devices.get(relativeSearchPath);
		if(currentNodeAtPath != null){
			// Find node's parent
			logger.debug("Node already present --> {}", currentNodeAtPath.getCompletePath());
			KuraNode parentFound = cthCollection.searchNewParentNode(sender);
			KuraNode currentParent = currentNodeAtPath.getParentNode();
			
			// Send the parent to the node. Only if it's different from the current one
			if(parentFound == null){
				// Already null.
				if(currentParent == null){
					logger.info("No available parents for --> {}", sender.getCompletePath());
				// Current parent set to another node.
				}else{
					cthPublisher.sendParentNode(cthNode, parentFound, sender);
				}
			// Parent found, not equals to null.
			}else{
				// Send the new parent
				if(currentParent == null || !parentFound.equals(currentParent)){
					cthPublisher.sendParentNode(cthNode, parentFound, sender);
				}else{
					// Parent already present in the node.
					if(parentFound.equals(currentParent)){
						logger.debug("Parent already present in --> {}", sender.getCompletePath());
					}
				}
			}
		}else{
			logger.info("Node not available in the tree");
			KuraNode parentFound = cthCollection.addNodeRequest(sender);
			// Send a message to the node to notify its parent.
			cthPublisher.sendParentNode(cthNode, parentFound, sender);
		}
		
		return operationResult;
	}
	
	/**
	 * Update local information on root updates on node.
	 * 
	 * @param message
	 * @return
	 */
	public Boolean onRootUpdateOnNode(CoAPMessage message){
		Boolean operationResult = false;
		KuraNode updatedNode = message.getSender();
		KuraNode oldParentNode = (KuraNode) message.getMessage();
		
		
		cthCollection.updateRootOnNode(updatedNode, oldParentNode);
		
		return operationResult;
	}
	
	/**
	 * Update local information on parent updates on node.
	 * Publish a notAvailableChild to the old parent, because now we are sure it's changed.
	 * 
	 * @param message
	 * @return
	 */
	public Boolean onParentUpdateOnNode(CoAPMessage message){
		Boolean operationResult = false;
		
		KuraNode sender = message.getSender();
		KuraNode oldParentNode = (KuraNode) message.getMessage();
		KuraNode parentNode = sender.getParentNode();
		
		// Modify CTHCollection
		operationResult = cthCollection.updateParentOnNode(sender, oldParentNode);
				
		// Publish a notAvailableChild to the old parent.
		if(oldParentNode != null && parentNode != null // Not null nodes
				&& !oldParentNode.equals(parentNode) // It's not the same as the new one
				&& cthCollection.getDeviceAtPath(oldParentNode.getRelativePath()).equals(oldParentNode)){ // It exists

			cthPublisher.publishNotAvailableChild(cthNode, oldParentNode, sender);
		}
		
		return operationResult;
	}
	
	/**
	 * Check if the current CTH is active after got the information from Web UI.
	 * 
	 * @return
	 */
    public Boolean isActive(){    	
    	return (cthOn == null || cthOn == false)? false : true;
    }
    
	
	/* ------------------------------------------
	 * --- Setting services ---
	 * ------------------------------------------ */
	protected void setDataService(DataService dataService){
		this.dataService = dataService;
	}
	protected void unsetDataService(DataService dataService){
		this.dataService = null;
	}
	protected void setSystemProperties(SystemProperties systemProperties){
		this.systemProperties = systemProperties;
	}
	protected void unsetSystemProperties(SystemProperties systemProperties){
		this.systemProperties = null;
	}
	
	
	/* ------------------------------------------
	 * --- Setters and getters ---
	 * ------------------------------------------ */
	public CTHCollection getCthCollection() {
		return cthCollection;
	}
	public void setCthCollection(CTHCollection cthCollection) {
		this.cthCollection = cthCollection;
	}
	
	
	
	class CTHThread implements Runnable{

		CoAPMessage message;
		
		public CTHThread(CoAPMessage message){
			this.message = message;
		}
		
		@Override
		public void run() {
			switch(message.getOperation()){
				case ROOT_REQUEST:
					logger.info("CoAPTreeHandler. Root Request from {}", message.getSender().getCompletePath());
					try {
						onRootRequest(message);
					} catch (CoAPConfigurationException e) {
						e.printStackTrace();
					}
					break;
				case ROOT_REMOVE:
					logger.info("CoAPTreeHandler. Root Remove from {}", message.getSender().getCompletePath());
					onRootRemove(message);
					break;
				case PARENT_REQUEST:
					logger.info("CoAPTreeHandler. Parent Request from {}", message.getSender().getCompletePath());
					onParentRequest(message);
					break;
				case NODE_ADD:
					logger.info("CoAPTreeHandler. Node Add from {}", message.getSender().getCompletePath());
					onNodeAdd(message);
					break;
				case NODE_REMOVE:
					logger.info("CoAPTreeHandler. Node Remove from {}", message.getSender().getCompletePath());
					onNodeRemove(message);
					break;
				case ROOT_ON_NODE_UPDATED:
					logger.info("CoAPTreeHandler. Root on node update from {}", message.getSender().getCompletePath());
					onRootUpdateOnNode(message);
					CTHPrinter.printTree(cthCollection.getParents());
					break;
				case PARENT_ON_NODE_UPDATED:
					logger.info("CoAPTreeHandler. Parent on node update from {}", message.getSender().getCompletePath());
					onParentUpdateOnNode(message);
					CTHPrinter.printTree(cthCollection.getParents());
					break;					
				default:
					break;
			}
		}
		
	}
	
	/**
	 * Add the execution of a Resource query to broker Executor.
	 * 
	 * @param message
	 */
	public void executeRequest(CoAPMessage message){
		requestExecutor.execute(new CTHThread(message));
	}
	
	/**
	 * Execute a callback handler.
	 * 
	 * @param message
	 */
	public void executeCallback(CoAPMessage message){
		callbackExecutor.execute(new CTHThread(message));
	}
}
