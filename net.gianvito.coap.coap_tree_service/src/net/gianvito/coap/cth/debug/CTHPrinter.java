package net.gianvito.coap.cth.debug;

import java.util.Map;
import java.util.Set;

import net.gianvito.coap.collection.cth.DevicesMap;
import net.gianvito.coap.collection.cth.ParentTree;
import net.gianvito.coap.collection.cth.RemoveResult;
import net.gianvito.coap.message.KuraNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shows state information about CTH.
 * 
 * @author Gianvito Morena
 *
 */
public class CTHPrinter {
	private static final Logger logger = LoggerFactory.getLogger(CTHPrinter.class);
	
	/**
	 * Shows every device currently connected.
	 * 
	 * @param devices
	 */
	public static void printDevices(DevicesMap devices){
		logger.info("-----------------------------------------");
		logger.info("------- CoAPTreeHandler Devices -------");
		logger.info("-----------------------------------------");
		for(Map.Entry<String, KuraNode> entry: devices.entrySet()){
			String currentParent = entry.getValue().getParentNode() != null?
									entry.getValue().getParentNode().getCompletePath() : "null";
			
			logger.info("Node --> {}", entry.getValue().getCompletePath());
			logger.info("  Relative path --> {}", entry.getValue().getRelativePath());
			logger.info("  Parent --> {}", currentParent);					
			logger.info("-----------------------------------------");
		}
		logger.info("-----------------------------------------");
		logger.info("-----------------------------------------");
	}
	
	/**
	 * Prints every information available about the tree.
	 * 
	 * @param tree
	 */
	@SuppressWarnings("rawtypes")
	public static void printTree(ParentTree tree){
		logger.info("-----------------------------------------");
		logger.info("------- CoAPTreeHandler Tree -------");
		logger.info("-----------------------------------------");
		for(Map.Entry entry: tree.getTree().entries()){
			KuraNode currentNode = (KuraNode) entry.getKey();
			KuraNode currentChild = (KuraNode) entry.getValue();
			String nodeCompletePath = currentNode == null? "null" : currentNode.getCompletePath();
			logger.info("Node --> {}", nodeCompletePath);
			logger.info("Child --> {}", currentChild.getCompletePath());
			logger.info("-----------------------------------------");
		}
		logger.info("-----------------------------------------");
		logger.info("-----------------------------------------");
	}
	
	/**
	 * Print results of a removed node.
	 * 
	 * @param result
	 */
	public static void printRemoveResult(RemoveResult result){
		Boolean removeResult = result.getOperationResult();
		KuraNode removedNode = result.getRemovedNode();
		Set<KuraNode> nodeChildren = result.getNodeChildren();
		
		logger.info("operationResult --> {}", removeResult);
		String removedNodePath = removedNode == null? "null": removedNode.getCompletePath();
		logger.info("removedNode --> {}", removedNodePath);
		String nodeChildrenString = nodeChildren == null? "null": nodeChildren.toString();
		logger.info("nodeChildren --> {}", nodeChildrenString);
	}
}
