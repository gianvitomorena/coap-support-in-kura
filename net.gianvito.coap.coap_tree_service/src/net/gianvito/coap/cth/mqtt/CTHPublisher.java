package net.gianvito.coap.cth.mqtt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.mqtt.CoAPTopic;
import net.gianvito.coap.type.CoAPOperation;
import net.gianvito.coap.utils.ByteObjectUtils;
import net.gianvito.coap.utils.MessageUtils;
import net.gianvito.coap.utils.PathUtils;

import org.eclipse.kura.KuraStoreException;
import org.eclipse.kura.data.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MQTT Publisher used by CoAPTreeHandler for message exchange.
 * 
 * @author Gianvito Morena
 *
 */
public class CTHPublisher {
	
	/* ----------------------- Attributes -------------------------- */
	
	// Logger
    private static final Logger logger = LoggerFactory.getLogger(CTHPublisher.class);
    
    /* --- Topics ---
	 * When a '/' keyword is present before the name, it means path is device related.
	 */
    // CTH topics
 	//public static final String CTH_ONLINE_TOPIC = "coap/cth/online";
    // Root topics
	//private static final String ROOT_ADDED_TOPIC = "coap/root/added";
	//private static final String ROOT_RESPONSE_TOPIC = "/coap/root/response";
	//private static final String ROOT_NOT_AVAILABLE_TOPIC = "coap/root/not_available";
	// Node topics
	//private static final String NODE_TOPIC_ADDED = "coap/node/added";
    //private static final String PARENT_RESPONSE_TOPIC = "/coap/parent/response";
    //private static final String PARENT_AVAILABLE_TOPIC = "coap/parent/available";
    //private static final String PARENT_NOT_AVAILABLE_TOPIC = "/coap/parent/not_available";
    //private static final String CHILD_NOT_AVAILABLE_TOPIC = "/coap/child/not_available";
    
    // Id used in CoAPMessages
    private long messagesId;
    
    // Topic properties (publish properties)
    private static final Integer COAP_BROKER_PUBLISH_QOS = 2;
    private static final Boolean COAP_BROKER_RETAIN = false;
    
    private static final Integer TREE_HANDLING_PRIORITY = 0;
    private static final Integer REQUEST_PRIORITY = 7;
    
    
    // Services and system utilities
	private DataService dataService;
	
	/* ----------------------- Methods ----------------------------- */
	/*
	 * ------------------------------------------
	 * --- Constructors ---
	 * ------------------------------------------
	 */
	/**
	 * Constructor.
	 * 
	 * @param dataService
	 * @param systemProperties
	 */
	public CTHPublisher(DataService dataService, Long messagesId){
		this.dataService = dataService;
		this.messagesId = messagesId;
	}
	
	/*
	 * ------------------------------------------
	 * --- Publish methods ---
	 * ------------------------------------------
	 */
	/**
	 * Publish a message to notify presence of a new CTH.
	 */
	public Integer publishAvailableCTH(KuraNode cthNode){
		Integer token = null;
		
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, "new_cth", CoAPOperation.CTH_ONLINE);
		
		token = publishMessage(message, CoAPTopic.CTH_ONLINE_TOPIC, null, 
								null, null, null);
		
		return token;
	}
	
	/**
	 * Send Root Node to the destination (when root node is already available).
	 * 
	 * @param rootNode
	 * @param destination
	 */
	public Integer publishAvailableRoot(KuraNode cthNode, KuraNode rootNode, KuraNode destination){
		Integer token = null;
		if(destination != null){
			CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, rootNode, CoAPOperation.ROOT_RESPONSE);
			token = publishMessage(message, CoAPTopic.ROOT_RESPONSE_TOPIC, destination, 
									null, null, null);
		}
		
		return token;
	}
	
	/**
	 * Send a message to the general topic about failure of a previous root node.
	 * 
	 * @param destination
	 */
	public Integer publishNotAvailableRoot(KuraNode cthNode, KuraNode notAvailableRoot){
		Integer token = null;
		
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, notAvailableRoot, CoAPOperation.ROOT_NOT_AVAILABLE);
		token = publishMessage(message, CoAPTopic.ROOT_NOT_AVAILABLE_TOPIC, null, 
								null, null, null);
		
		return token;
	}
	
	/**
	 * Send the parent to the destination.
	 * 
	 * @param parent
	 * @param destination
	 */
	public Integer sendParentNode(KuraNode cthNode, KuraNode parent, KuraNode destination){		
		Integer token = null;
		if(destination != null){
			CoAPMessage parentNodeMessage = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, parent, CoAPOperation.PARENT_RESPONSE);
			token = publishMessage(parentNodeMessage, CoAPTopic.PARENT_RESPONSE_TOPIC, destination, 
									null, null, REQUEST_PRIORITY);
		}
		
		return token;
	}
	
	/**
	 * Publish added root to a general topic.
	 * 
	 * @param newRoot
	 */
	public Integer publishAddedRoot(KuraNode cthNode, KuraNode newRoot){
		Integer token = null;
		// Create the message with the newRoot as message field
		CoAPMessage addedRootMessage = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, newRoot, CoAPOperation.ROOT_ADDED);
		token = publishMessage(addedRootMessage, CoAPTopic.ROOT_ADDED_TOPIC, null, 
								null, null, null);
		
		return token;
	}
	
	/**
	 * Publish added node to a general topic.
	 * 
	 * @param addedNode
	 */
	public Integer publishAddedNode(KuraNode cthNode, KuraNode addedNode){
		Integer token = null;
		// Create the message with the newRoot as message field
		CoAPMessage addedRootMessage = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, addedNode, CoAPOperation.NODE_ADDED);
		token = publishMessage(addedRootMessage, CoAPTopic.NODE_TOPIC_ADDED, null, 
								null, null, null);
		
		return token;
	}
	
	/**
	 * Publish available parent to a general topic.
	 * 
	 * @param destination
	 */
	public Integer publishAvailableParent(KuraNode cthNode, KuraNode availableParent){
		Integer token = null;
		// Create the message with the newRoot as message field
		CoAPMessage availableParentMessage = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, availableParent, CoAPOperation.PARENT_AVAILABLE);
		token = publishMessage(availableParentMessage, CoAPTopic.PARENT_AVAILABLE_TOPIC, null, 
								null, null, null);
		
		return token;
	}
	
	/**
	 * Send a message to the specific node topic about failure of a previous parent node.
	 * 
	 * @param destinations
	 * @param failedNode
	 */
	public List<Integer> publishNotAvailableParent(KuraNode cthNode, Set<KuraNode> destinations, KuraNode failedNode){
		List<Integer> tokenList = null;
		
		if(destinations != null && destinations.size() > 0){
			// Create the message with the newRoot as message field
			CoAPMessage notAvailableParentMessage = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, failedNode, CoAPOperation.PARENT_NOT_AVAILABLE);
			tokenList = publishToMultipleNodes(CoAPTopic.PARENT_NOT_AVAILABLE_TOPIC, notAvailableParentMessage, destinations, 
												null, null, null);
		}
		
		return tokenList;
	}
	
	/**
	 * Send a message to the specific node topic about failure of one of its child node.
	 * 
	 * @param parent
	 * @param failedChild
	 */
	public Integer publishNotAvailableChild(KuraNode cthNode, KuraNode parent, KuraNode failedChild){
		Integer token = null;
		if(parent != null){
			// Create the message with the newRoot as message field
			CoAPMessage notAvailableChildMessage = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							cthNode, failedChild, CoAPOperation.CHILD_NOT_AVAILABLE);
			token = publishMessage(notAvailableChildMessage, CoAPTopic.CHILD_NOT_AVAILABLE_TOPIC, parent, 
									null, null, null);
		}
		
		return token;
	}
	
	/**
	 * Publish to multiple nodes.
	 * 
	 * @param destinationTopic
	 * @param message
	 * @param nodes
	 * @param qos
	 * @param retain
	 * @param priority
	 */
	public List<Integer> publishToMultipleNodes(String destinationTopic, CoAPMessage message, Set<KuraNode> nodes,
										Integer qos, Boolean retain, Integer priority){
		List<Integer> tokenList = new ArrayList<Integer>();
		
		for(KuraNode node : nodes){
			Integer token = publishMessage(message, destinationTopic, node, qos, retain, priority);
			if(token != null) tokenList.add(token);
		}
		
		return tokenList;
	}
	
	/**
	 * Standard method to publish a message to a specified destination.
	 * 
	 * @param message
	 * @param destinationTopic
	 * @param destination
	 * @param qos
	 * @param retain
	 * @param priority
	 */
	public Integer publishMessage(CoAPMessage message, String destinationTopic, KuraNode destination, 
									Integer qos, Boolean retain, Integer priority){
		Integer messageID = null;
		
		// Publish the message to the sender if destination is null
		String topicToSend = destination != null? CoAPKey.CONTROL_PREFIX + 
				PathUtils.buildCompleteNodePath(destination) + destinationTopic : "";
		topicToSend = (topicToSend.equals(""))? destinationTopic : topicToSend;
		
		Integer finalQoS = qos == null? COAP_BROKER_PUBLISH_QOS : qos;
		Boolean finalRetain = retain == null? COAP_BROKER_RETAIN : retain;
		Integer finalPriority = priority == null? TREE_HANDLING_PRIORITY : priority;
		
		try {
			messageID = dataService.publish(topicToSend,
								ByteObjectUtils.toByteArray(message),
								finalQoS,
								finalRetain, 
								finalPriority);
			logger.info("Message published on " + topicToSend);
		} catch (KuraStoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return messageID;
	}
}
