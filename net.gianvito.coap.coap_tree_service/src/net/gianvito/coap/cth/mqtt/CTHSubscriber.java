package net.gianvito.coap.cth.mqtt;

import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.mqtt.CoAPTopic;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.KuraNotConnectedException;
import org.eclipse.kura.KuraTimeoutException;
import org.eclipse.kura.data.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MQTT Subscriber used by CoAPTreeHandler to specify interest for particular topics.
 * 
 * @author Gianvito Morena
 *
 */
public class CTHSubscriber {
	
	/* ----------------------- Attributes -------------------------- */
	
	// Logger
	private static final Logger logger = LoggerFactory.getLogger(CTHSubscriber.class);
	
	/* ---- Topics ----
     * When a '/' keyword is present as name prefix, it means paths are device related.
     */
    // Root Topics
	//public static final String ROOT_REQUEST_TOPIC = "coap/root/request";
    //public static final String ROOT_ON_NODE_UPDATED_TOPIC = "coap/node/root/updated";
    // Node Topics
    //public static final String PARENT_REQUEST_TOPIC = "coap/parent/request";
    //public static final String PARENT_ON_NODE_UPDATED_TOPIC = "coap/node/parent/updated";
    //public static final String NODE_REMOVE_TOPIC = "coap/node/remove";
    // MQTT/LWT Topic
    //public static final String MQTT_LWT_TOPIC = "MQTT/LWT/#";
    
    // CoAP --> MQTT subscribe properties
    public static final Integer COAP_BROKER_SUBSCRIBE_QOS = 2;
    
    private Set<String> topics;
    
    /* ----------------------- Methods ----------------------------- */
    /**
     * Constructor.
     */
    public CTHSubscriber(){
    	topics = new HashSet<String>();
    	topics.add(CoAPTopic.MQTT_LWT_TOPIC);
    	topics.add(CoAPTopic.NODE_REMOVE_TOPIC);
    	// Parent and topic requests and updates
    	topics.add(CoAPTopic.ROOT_REQUEST_TOPIC);
    	topics.add(CoAPTopic.PARENT_REQUEST_TOPIC);
    	topics.add(CoAPTopic.ROOT_ON_NODE_UPDATED_TOPIC);
    	topics.add(CoAPTopic.PARENT_ON_NODE_UPDATED_TOPIC);
    }
	
    /**
     * Subscribe to topics.
     * 
     * @param dataService
     */
	public void subscribeToTopics(DataService dataService){
		try {
			logger.info("Subscribing to CoAPTreeHandler topics");
			dataService.subscribe(CoAPTopic.MQTT_LWT_TOPIC, COAP_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(CoAPTopic.NODE_REMOVE_TOPIC, COAP_BROKER_SUBSCRIBE_QOS);
			// Parent and topic requests and updates
			dataService.subscribe(CoAPTopic.ROOT_REQUEST_TOPIC, COAP_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(CoAPTopic.PARENT_REQUEST_TOPIC, COAP_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(CoAPTopic.ROOT_ON_NODE_UPDATED_TOPIC, COAP_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(CoAPTopic.PARENT_ON_NODE_UPDATED_TOPIC, COAP_BROKER_SUBSCRIBE_QOS);
		} catch (KuraException e) {
			e.printStackTrace();
		} catch (NullPointerException e){
			logger.error("Errore null");
		}
	}
	
	/**
	 * Unsubscribe to topics.
	 * 
	 * @param dataService
	 */
	public void unsubscribeToTopics(DataService dataService){
		try {
			dataService.unsubscribe(CoAPTopic.MQTT_LWT_TOPIC);
			dataService.unsubscribe(CoAPTopic.NODE_REMOVE_TOPIC);
			dataService.unsubscribe(CoAPTopic.ROOT_REQUEST_TOPIC);
			dataService.unsubscribe(CoAPTopic.PARENT_REQUEST_TOPIC);
			dataService.unsubscribe(CoAPTopic.ROOT_ON_NODE_UPDATED_TOPIC);
			dataService.unsubscribe(CoAPTopic.PARENT_ON_NODE_UPDATED_TOPIC);
			// Remove topics
			topics.clear();
		} catch (KuraTimeoutException e) {
			e.printStackTrace();
		} catch (KuraNotConnectedException e) {
			e.printStackTrace();
		} catch (KuraException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Utility method to check if CTH is interested in receiving messages
	 * coming from input topic.
	 * 
	 * @param topic
	 * @return
	 */
	public Boolean checkTopic(String topic){
		if(topic != null){
			return topics.contains(topic);
		}else{
			return false;
		}
		
	}
}
