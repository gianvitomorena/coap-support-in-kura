package net.gianvito.coap.debug_broker;

import java.io.IOException;

import net.gianvito.coap.debug.listener.CoAPDebugListener;
import net.gianvito.coap.debug.utils.DebugMessage;
import net.gianvito.coap.debug.utils.DebugMessageType;
import net.gianvito.coap.debug.utils.SerializableResources;
import net.gianvito.coap.server.CoAPServer;
import net.gianvito.coap.system.IPType;
import net.gianvito.coap.system.SystemProperties;
import net.gianvito.coap.utils.ByteObjectUtils;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.KuraNotConnectedException;
import org.eclipse.kura.KuraStoreException;
import org.eclipse.kura.KuraTooManyInflightMessagesException;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataServiceListener;
import org.eclipse.kura.data.DataTransportService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Receives MQTT requests for obtaining information about CoAP server.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPDebug implements DataServiceListener, CoAPDebugListener{
	
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(CoAPDebug.class);
	
	private DataService dataService;
	private DataTransportService dataTransportService;
	private SystemProperties systemProperties;
	private CoAPServer cs;
	private String address;
	
	private static final String LINKED_NODE_REQUEST = "/coap/debug/request/nodes";
	private static final String PROPERTY_REQUEST = "/coap/debug/request/properties";
	private static final String LINKED_NODE_RESPONSE = "coap/debug/response/nodes";
	private static final String PROPERTY_RESPONSE = "coap/debug/response/properties";
	
	private static final int DEBUG_SUBSCRIBE_QOS = 0;
	private static final int DEBUG_PUBLISH_QOS = 0;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public CoAPDebug(){}
	
	
	/* 
	 * Activate Method
	 */
	protected void activate(ComponentContext componentContext){		
		if(dataService.isConnected()){
			address = systemProperties.getCurrentIPAddress(IPType.IPv4, cs.getNetworkInterface());
         	try {
				dataService.subscribe(address + LINKED_NODE_REQUEST, DEBUG_SUBSCRIBE_QOS);
				dataService.subscribe(address + PROPERTY_REQUEST, DEBUG_SUBSCRIBE_QOS);
			} catch (KuraException e) {
				e.printStackTrace();
			}
         }
		logger.info("CoAPDebug activated.");
	}
	
	/* 
	 * Deactivate Method
	 */
	protected void deactivate(ComponentContext componentContext){
		try {
			dataService.unsubscribe(address + LINKED_NODE_REQUEST);
			dataService.unsubscribe(address + PROPERTY_REQUEST);
		} catch (KuraException e) {
			e.printStackTrace();
		}
		logger.info("CoAPDebug deactivated.");
	}
	
	public void onLinkedNodeRequest(DebugMessage message){
		DebugMessage cthMessage = new DebugMessage(DebugMessageType.LINKED_NODES,
										cs.getBrokerCollection().getLinkedNodes().getSerializableNodes());
		
		try {
			dataService.publish(LINKED_NODE_RESPONSE, ByteObjectUtils.toByteArray(cthMessage), DEBUG_PUBLISH_QOS, false, 0);
		} catch (KuraStoreException | IOException e) {
			e.printStackTrace();
		}
	}

	public void onPropertyRequest(DebugMessage message){
		DebugMessage cthMessage = new DebugMessage(DebugMessageType.PROPERTIES,
				new SerializableResources(cs.getBrokerCollection().getResourceCollection().getSerializableProperties()));
		
		try {
			//dataService.publish(PROPERTY_RESPONSE, ByteObjectUtils.toByteArray(cthMessage), DEBUG_PUBLISH_QOS, false, 0);
			dataTransportService.publish(PROPERTY_RESPONSE, ByteObjectUtils.toByteArray(cthMessage), DEBUG_PUBLISH_QOS, false);
		} catch (KuraStoreException | IOException e) {
			e.printStackTrace();
		} catch (KuraTooManyInflightMessagesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KuraNotConnectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KuraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onConnectionEstablished() {
     	try {
			address = systemProperties.getCurrentIPAddress(IPType.IPv4, cs.getNetworkInterface());
			dataService.subscribe(address + LINKED_NODE_REQUEST, 1);
			dataService.subscribe(address + PROPERTY_REQUEST, 1);
		} catch (KuraException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDisconnecting() {
		
	}

	@Override
	public void onDisconnected() {
		
	}

	@Override
	public void onConnectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMessageArrived(String topic, byte[] payload, int qos,
			boolean retained) {
		DebugMessage message;
		if(isTopicRight(topic)){
			try {
				message = (DebugMessage)ByteObjectUtils.getObject(payload, DebugMessage.class);
				// Select the operation to execute
				DebugMessageType type = message.getType();
				switch(type){
					case LINKED_NODES:
						onLinkedNodeRequest(message);
						break;
					case PROPERTIES:
						onPropertyRequest(message);
						break;
					default:
						break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public boolean isTopicRight(String topic){
		if(topic.equals(address + LINKED_NODE_REQUEST) || topic.equals(address + PROPERTY_REQUEST)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public void onMessagePublished(int messageId, String topic) {
		
	}

	@Override
	public void onMessageConfirmed(int messageId, String topic) {
		
	}
	
	/*
	 * ------------------------------------------
	 * --- Setting services ---
	 * ------------------------------------------
	 */
	protected void setDataService(DataService dataService){
		this.dataService = dataService;
	}
	protected void unsetDataService(DataService dataService){
		this.dataService = null;
	}
	protected void setDataTransportService(DataTransportService dataTransportService){
		this.dataTransportService = dataTransportService;
	}
	protected void unsetDataTransportService(DataTransportService dataTransportService){
		this.dataTransportService = null;
	}
	protected void setSystemProperties(SystemProperties systemProperties){
		this.systemProperties = systemProperties;
	}
	protected void unsetSystemProperties(SystemProperties systemProperties){
		this.systemProperties = null;
	}
	protected void setCoAPServer(CoAPServer cs){
		this.cs = cs;
	}
	protected void unsetCoAPServer(CoAPServer cs){
		this.cs = null;
	}
}
