package net.gianvito.coap.server.debug;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Printer utils for KuraNode.
 * 
 * @author Gianvito Morena
 *
 */
public class NodePrinter {
	private static final Logger logger = LoggerFactory.getLogger(NodePrinter.class);
	
	/**
	 * Prints information about input KuraNode.
	 * 
	 * @param node
	 */
	public static void printNode(KuraNode node, int debugLevel){
		if(debugLevel > CoAPKey.LOW_DEBUG){
			logger.info("-----------------------------------------");
			logger.info("------- Node Hierarchy -------");
			logger.info("-----------------------------------------");
			logger.info("Node --> {}", node.getCompletePath());
			String parentPath = node.getParentNode() == null? "null" : node.getParentNode().getCompletePath();
			logger.info("Parent --> {}", parentPath);
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
		}
	}
	
	
}
