package net.gianvito.coap.server.debug;

import net.gianvito.coap.collection.server.BrokerCollection;

public class BrokerPrinter {
	public static void printBroker(BrokerCollection bc, int debugMode){
		LinkedNodePrinter.printCollection(bc.getLinkedNodes(), debugMode);
		ResourcesPrinter.printCollection(bc.getResourceCollection(), debugMode);
	}
}
