package net.gianvito.coap.server.debug;

import java.util.Set;

import net.gianvito.coap.collection.property.GeneralProperty;
import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.collection.server.PropertyCollection;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.resources.LinkAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Printer utils for PropertyCollection.
 * 
 * @author Gianvito Morena
 *
 */
public class ResourcesPrinter {
	private static final Logger logger = LoggerFactory.getLogger(ResourcesPrinter.class);
	
	/**
	 * Prints information about input PropertyCollection.
	 * 
	 * @param resourcePC
	 */
	public static void printCollection(PropertyCollection resourcePC, int debugMode){
		if(debugMode > CoAPKey.INTENSE_DEBUG){
			logger.info("-----------------------------------------");
			logger.info("-- ResourcePropertyCollection print -----");
			logger.info("-----------------------------------------");
			for(LinkAttribute attribute: resourcePC.keySet()){
				logger.info("Property --> {}", attribute.getName() + " = " + attribute.getValue());
				Set<GeneralProperty> properties = resourcePC.getProperties(attribute);
				logger.info("Number --> {}", properties.size());
				for(GeneralProperty property : properties){
					logger.info("Resource type --> {} --> {}", property.getType(), property.toString());
				}
				logger.info("-----------------------------------------");
			}
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
		}
	}
	
	
	public static void printResourceSet(Set<ResourceProperty> resProperties){
		logger.info("-----------------------------------------");
		logger.info("-- ResourceProperty result print -----");
		for(ResourceProperty rp : resProperties){
			logger.info("Property --> {}", rp.getResource().getURI() + rp.getResource().getPath());
		}
		logger.info("-----------------------------------------");
	}
	
	public static void printNodeSet(Set<NodeProperty> nodeProperties){
		logger.info("-----------------------------------------");
		logger.info("-- NodeProperty result print -----");
		for(NodeProperty np : nodeProperties){
			logger.info("Property --> {}",  np.getNode().getCompletePath());
		}
		logger.info("-----------------------------------------");
	}
}
