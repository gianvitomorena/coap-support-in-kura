package net.gianvito.coap.server.debug;

import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.PropagationObject;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.resources.LinkAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Printer utils for Queries.
 * 
 * @author Gianvito Morena
 *
 */
public class QueryPrinter {
	// Logger
	private static final Logger logger = LoggerFactory.getLogger(QueryPrinter.class);
	
	/**
	 * Prints informations about Query Request.
	 * 
	 * @param message
	 */
	public static void printQueryRequest(CoAPMessage message, int debugMode){
		if(debugMode > CoAPKey.INTENSE_DEBUG){
			AttributeCollection attributes = (AttributeCollection) message.getMessage();
			
			logger.info("-----------------------------------------");
			logger.info("------------ Query details --------------");
			logger.info("-----------------------------------------");
			logger.info("Sender node --> {}", message.getSender().getCompletePath());
			for(LinkAttribute attribute : attributes.getLinkAttributes()){
				logger.info("Query attribute --> {} = {}", attribute.getName(), attribute.getValue());
			}
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
		}
	}
	
	/**
	 * Prints informations about Query Response.
	 * 
	 * @param message
	 */
	public static void printQueryResponse(CoAPMessage message, int debugMode){
		if(debugMode > CoAPKey.INTENSE_DEBUG){
			PropagationObject propObj = (PropagationObject) message.getMessage();
			logger.info("-----------------------------------------");
			logger.info("------------ Query details --------------");
			logger.info("-----------------------------------------");
			logger.info("Sender node --> {}", message.getSender().getCompletePath());
			logger.info("--- Original message ---");
			logger.info("Sender --> {}", propObj.getOriginalQuery().getSender());
			for(LinkAttribute attribute : propObj.getAttributes().getLinkAttributes()){
				logger.info("Query attribute --> {}", attribute.getName() + "=" + attribute.getValue());
			}
			logger.info("--- Nodes to query ---" );
			for(NodeProperty node : propObj.getNodeProperties()){
				logger.info("Address --> {}", node.getNode().getCompletePath());
			}
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
		}
	}
}
