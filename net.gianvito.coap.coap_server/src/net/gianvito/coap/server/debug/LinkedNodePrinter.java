package net.gianvito.coap.server.debug;

import net.gianvito.coap.collection.server.LinkedNodeCollection;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.resources.LinkAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Printer utils for LinkedNodeCollection.
 * 
 * @author Gianvito Morena
 *
 */
public class LinkedNodePrinter {
	private static final Logger logger = LoggerFactory.getLogger(LinkedNodePrinter.class);
	
	/**
	 * Prints information about input LinkedNodeCollection
	 * 
	 * @param linkedNodes
	 */
	public static void printCollection(LinkedNodeCollection linkedNodes, int debugMode){
		if(debugMode > CoAPKey.INTENSE_DEBUG){
			logger.info("-----------------------------------------");
			logger.info("-------- LinkNodeCollection print -------");
			logger.info("-----------------------------------------");
			for(KuraNode node: linkedNodes.getNodes()){
				logger.info("Node --> {}", node.getCompletePath());
				for(LinkAttribute attribute : linkedNodes.getCollection(node).getLinkAttributes()){
					logger.info("--------- Property --> {} = {}", attribute.getName(),
										  attribute.getValue());
				}
			}
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
			logger.info("-----------------------------------------");
		}
	}
}
