package net.gianvito.coap.server;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import net.gianvito.coap.californium.server.CaliforniumServer;
import net.gianvito.coap.client.CoAPDTLSRemoteClient;
import net.gianvito.coap.client.CoAPRemoteClient;
import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.collection.server.BrokerCollection;
import net.gianvito.coap.collection.server.RequestResult;
import net.gianvito.coap.dtls.CoAPDTLSOptions;
import net.gianvito.coap.exception.CoAPConfigurationException;
import net.gianvito.coap.exception.NetworkInterfaceException;
import net.gianvito.coap.listener.server.CoAPBrokerListener;
import net.gianvito.coap.listener.server.ResourceListener;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.message.PropagationObject;
import net.gianvito.coap.resources.LinkAttribute;
import net.gianvito.coap.server.debug.LinkedNodePrinter;
import net.gianvito.coap.server.debug.NodePrinter;
import net.gianvito.coap.server.debug.QueryPrinter;
import net.gianvito.coap.server.debug.ResourcesPrinter;
import net.gianvito.coap.server.mqtt.CoAPPublisher;
import net.gianvito.coap.server.mqtt.CoAPSubscriber;
import net.gianvito.coap.server.utils.KuraNodeUtils;
import net.gianvito.coap.system.SystemProperties;
import net.gianvito.coap.timer.CoAPTimerType;
import net.gianvito.coap.utils.ByteObjectUtils;
import net.gianvito.coap.utils.MessageUtils;
import net.gianvito.coap.utils.PathUtils;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataServiceListener;
import org.eclipse.kura.data.DataTransportService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

/**
 * CoAP Server with distributed capabilities.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPServer implements ConfigurableComponent, DataServiceListener,
									CoAPBrokerListener, ResourceListener{
	
	/* ----------------------------- Attributes ------------------------------------- */
	// Logger
    private static final Logger logger = LoggerFactory.getLogger(CoAPServer.class);
    
    // APP ID
    private static final String APP_ID = "net.gianvito.coap.server.CoAPServer";
  

    /* ---------- Interface properties ----------------- */
    private Map<String, Object> uiProperties;
    private static final String COAP_SERVER_ON = "coap.on";
    private static final String NETWORK_INTERFACE = "coap.network_interface";
    private static final String COAP_PORT = "coap.port";
    private static final String COAP_DOMAIN = "coap.domain";
    private static final String COAP_GROUP = "coap.group";
    private static final String PARENT_UPDATE_TIMER = "coap.parent.update.timer";
    private static final String FIRST_CONNECTION_DELAY = "coap.first_connection_delay";
    private static final String DEBUG_MODE = "coap.debug";
    private static final String EMERGENCY_MODE = "coap.emergency";
    private static final String EMERGENCY_DELAY = "coap.emergency_delay";
    private static final String COAP_DTLS_ON = "coap.dtls.on";
    private static final String DTLS_KEYSTORE = "coap.keystore";
    private static final String DTLS_TRUSTSTORE = "coap.truststore";
    private static final String DTLS_PSK_IDENTITY = "coap.psk_identity";
    private static final String DTLS_PSK_PASSWORD = "coap.psk_password";
    private static final String DTLS_KS_CLIENT_IDENTITY = "coap.ks_client_identity";
    private static final String DTLS_KS_SERVER_IDENTITY = "coap.ks_server_identity";
    private static final String DTLS_ROOT_CERTIFICATE_IDENTITY = "coap.root_certificate_identity";
    private static final String DTLS_KS_PASSWORD = "coap.ks_password";
    private static final String DTLS_TS_PASSWORD = "coap.ts_password";
    
    
    // UI properties
    private String networkInterfaceName;
    private Integer coapPort;
    private Integer debugMode;
    private Boolean emergencyMode;
    private Integer emergencyDelay;
    private boolean dtlsOn;
    
    // CoAP Server
    private CaliforniumServer californiumServer;
    
    // CoAP
    private CoAPRemoteClient remoteClient;
    private CoAPDTLSRemoteClient clientDTLS;
    private CoAPDTLSOptions dtlsOptions;
    private CoAPPublisher coapPublisher;
    private CoAPSubscriber coapSubscriber;
	
    // Node properties
    private KuraNode thisNode;
	private KuraNode parentNode;
	private String domain;
	private String group;
	private Boolean coapBrokerOn;
	private long messagesId;
	
	// Collection
	private BrokerCollection brokerCollection;
	
	// Timers and properties.
	private Integer firstConnectionDelay;
	private Integer parentUpdatePeriod;
	private ScheduledFuture<?> parentTimerHandler;
	
	// Services
    private DataService dataService;
    private DataTransportService dataTransportService;
    private SystemProperties systemProperties;
    
    // Executor Service
    private ExecutorService callbackExecutor;
    private ExecutorService resourceExecutor;
    private ScheduledExecutorService chooseOperationExecutor;
    private ScheduledExecutorService timerExecutor;
    private final int RESOURCE_EXECUTOR_THREADS = 2;    
    
    
    /* ----------------------------- Methods ------------------------------------- */    
    /**
     * Constructor.
     */
    public CoAPServer(){
    	super();
    	// Create BrokerCollection
    	brokerCollection = new BrokerCollection();
    	remoteClient = new CoAPRemoteClient();
    }
    
    
    /* -------------------------------
     * Component methods
     * ------------------------------- */ 
    /**
     * Activate method.
     * 
     * @param componentContext
     * @param properties
     */
    protected void activate(ComponentContext componentContext, Map<String,Object> properties) {
        logger.info("Bundle " + APP_ID + " has started!");
        
        // Create executors.
        callbackExecutor = Executors.newSingleThreadExecutor();
        resourceExecutor = Executors.newFixedThreadPool(RESOURCE_EXECUTOR_THREADS);
        timerExecutor = Executors.newSingleThreadScheduledExecutor();
        chooseOperationExecutor = Executors.newSingleThreadScheduledExecutor();
        
        // Get the properties from UI
        this.uiProperties = properties;
        CoAPDTLSOptions dtlsOptions = updatePropertiesFromUI();
        
        // Deactivate directly if it's set in the UI
        if(coapBrokerOn){
        	
        	// Reboot Californium server.
     		rebootCaliforniumServer(dtlsOptions);
     		
     		if(dtlsOn){
     			// Create DTLS client
         		clientDTLS = new CoAPDTLSRemoteClient(dtlsOptions);
     		}
     		
         	// If DataService is connected, subscribe to topics.
            if(dataService.isConnected()){
            	try {
					updateNodeProperties();
					coapSubscriber = new CoAPSubscriber(thisNode);
	    			coapSubscriber.subscribeToTopics(dataService);
	    			//coapPublisher = new CoAPPublisher(dataService, messagesId);
	    			coapPublisher = new CoAPPublisher(dataTransportService, messagesId);
	    			// Start the timer
	    			startTimer(CoAPTimerType.CONNECT_TO_TREE);
				} catch (NetworkInterfaceException e) {
					logger.error(e.getMessage());
				}
            }
        }
    }
    
    /**
     * Deactivate method.
     * 
     * @param componentContext
     */
    protected void deactivate(ComponentContext componentContext) {
        logger.info("Bundle {} has stopped!", APP_ID);
    	stopTimer();
    	callbackExecutor.shutdown();
    	resourceExecutor.shutdown();
    	chooseOperationExecutor.shutdown();
    	timerExecutor.shutdownNow();
    	//californiumServer.stop();
    	disconnectFromTree();
    	thisNode = new KuraNode(systemProperties, networkInterfaceName, coapPort, parentNode, domain, group);
    	coapSubscriber.unsubscribeToTopics(dataService);
    	logger.info("CoAP Server deactivated");
    }
    
    /**
     * Updated method.
     * 
     * @param componentContext
     * @param properties
     */
    public void updated(ComponentContext componentContext, Map<String, Object> properties) {
//    	try{
//    		deactivate(componentContext);
//    	}catch(Exception e){
//    		logger.info("Deactivation problem. Probably broker was not connected.");
//    	}
//    		
//        activate(componentContext, properties);
    	this.uiProperties = properties;
    }
    
    /**
     * Update node properties from Web UI
     */
    public CoAPDTLSOptions updatePropertiesFromUI(){
    	CoAPDTLSOptions dtlsOptions;
    	
    	coapBrokerOn = (Boolean) uiProperties.get(COAP_SERVER_ON);
    	networkInterfaceName = (String) uiProperties.get(NETWORK_INTERFACE);
    	coapPort = (Integer) uiProperties.get(COAP_PORT);
    	domain = (String) uiProperties.get(COAP_DOMAIN);
    	group = (String) uiProperties.get(COAP_GROUP);
    	parentUpdatePeriod = (Integer) uiProperties.get(PARENT_UPDATE_TIMER);
    	debugMode = (Integer) uiProperties.get(DEBUG_MODE) == null? 3 : (Integer) uiProperties.get(DEBUG_MODE);
    	emergencyMode = (Boolean) uiProperties.get(EMERGENCY_MODE);
    	emergencyDelay = (Integer) uiProperties.get(EMERGENCY_DELAY);
    	firstConnectionDelay = (Integer) uiProperties.get(FIRST_CONNECTION_DELAY);
    	dtlsOn = (boolean) uiProperties.get(COAP_DTLS_ON);
    	
    	dtlsOptions = new CoAPDTLSOptions(
    			(String) uiProperties.get(DTLS_KEYSTORE),
    			(String) uiProperties.get(DTLS_TRUSTSTORE),
    			(String) uiProperties.get(DTLS_PSK_IDENTITY),
    			(String) uiProperties.get(DTLS_KS_CLIENT_IDENTITY),
    			(String) uiProperties.get(DTLS_KS_SERVER_IDENTITY),
    			(String) uiProperties.get(DTLS_ROOT_CERTIFICATE_IDENTITY),
    			((String) uiProperties.get(DTLS_PSK_PASSWORD)).getBytes(),
    			((String) uiProperties.get(DTLS_KS_PASSWORD)).toCharArray(),
    			((String) uiProperties.get(DTLS_TS_PASSWORD)).toCharArray());
    	
    	this.dtlsOptions = dtlsOptions;
    	
    	return dtlsOptions;
    }
    
    /**
     * Update local KuraNode properties.
     * 
     * @throws NetworkInterfaceException 
     */
    public void updateNodeProperties() throws NetworkInterfaceException{
    	thisNode = new KuraNode(systemProperties, networkInterfaceName, coapPort, parentNode, domain, group);
    	
    	messagesId = MessageUtils.getFirstHalfSerialNumber(systemProperties, networkInterfaceName);
    	
    	if(!thisNode.isCurrentlySet()){
    		throw new NetworkInterfaceException();
    	}
    }
    
    /**
     * Reboot Californium server with input attributes as keyStores.
     * 
     * @param kSLocation
     * @param tSLocation
     */
    public void rebootCaliforniumServer(CoAPDTLSOptions dtlsOpt){
    	if(dtlsOn){
    		californiumServer.rebootAsDTLS(coapPort, dtlsOpt);
    	}else{
    		californiumServer.reboot(coapPort);
    	}
    }
    
    
    
    /* ------------------------------------------
	 * --- Startup-Shutdown methods ---
	 * ------------------------------------------ */
    /**
     * Connect to CoAP Brokers tree through MQTT.
     * 
     * @throws CoAPConfigurationException 
     */
    public void connectToTree() throws CoAPConfigurationException{
    	// Level 0 node.
    	if(thisNode.getLevel() == 0){
    		coapPublisher.publishRootRequest(thisNode);
    	// Level 1 node.
    	}else if(thisNode.getLevel() == 1){
    		coapPublisher.publishRootRequest(thisNode);
    	// Level 2 node.
    	}else if(thisNode.getLevel() == 2){
    		coapPublisher.publishParentRequest(thisNode);
    	// Wrong configuration.
    	}else{
    		throw new CoAPConfigurationException();
    	}
    }
    
    /**
     * Disconnect from CoAP Broker tree.
     */
    public void disconnectFromTree(){
    	// Send a message to CoAPTreeHandler for complete disconnection
    	coapPublisher.publishDisconnectFromTree(thisNode);
    }
    
    
    /**
     * Called after CTH is off.
     */
    public void emergencyProcedure(){
    	// Execute a soft reboot.
    	disconnectFromTree();
    	brokerCollection.removeLinkedNodesAttributes();
    	thisNode.setParentNode(null);
    	//connectToTree();
		startTimer(CoAPTimerType.CONNECT_TO_TREE);
    }
    

	
	/* ------------------------------------------
	 * --- Resources handling utils ---
	 * ------------------------------------------ */		
	/**
	 * Add the resource to the tree.
	 * If there are modified general properties, sends updates to parent node.
	 * 
	 * @param resource
	 * @param resourceClass
	 */
    public void addResource(CoapResource resource, Class<? extends CoapResource> resourceClass){
    	
    	AttributeCollection modifiedAttributes = new AttributeCollection();
    	
    	if(resource instanceof CoapResource){
    		resource = (resourceClass.cast(resource));
    		californiumServer.addResource(resource);
    		
    		// Adds the Resource attributes to the local HashMap
        	modifiedAttributes = brokerCollection.addResourceProperties(resource);
        	
        	if(modifiedAttributes.size() > 0){
        		KuraNodeUtils.sendAttributesToParent(thisNode,
													brokerCollection.getAttributeCollection(),
													coapPublisher);
        	}
    	}
    	
    	ResourcesPrinter.printCollection(brokerCollection.getResourceCollection(), debugMode);
    }
    
    
    
    /**
     * Remove the resource from the tree.
     * If there are modified general properties, sends updates to parent node.
     * 
     * @param resource
     * @param resourceClass
     */
    public void removeResource(CoapResource resource, Class<? extends CoapResource> resourceClass){
    	AttributeCollection modifiedAttributes = new AttributeCollection();
    	
    	if(resource instanceof CoapResource){
    		resource = (resourceClass.cast(resource));
    		// Remove the Resource attributes from the local HashMap
        	modifiedAttributes = brokerCollection.removeResourceProperties(resource);
        	
        	if(modifiedAttributes.size() > 0){
        		KuraNodeUtils.sendAttributesToParent(thisNode,
										brokerCollection.getAttributeCollection(),
										coapPublisher);
        	}
        	
        	//localResources.remove(resource);
        	californiumServer.removeResource(resource);
    	}
    	
    	ResourcesPrinter.printCollection(brokerCollection.getResourceCollection(), debugMode);
    }
    
    /**
     * Add a resource to the root tree.
     * 
     * @param resource
     */
    public void addSpecialResource(CoapResource resource){    	
    	//coapServer.add(resource);
    	californiumServer.addRootResource(resource);
    }
    
    /**
     * Remove a resource from the root tree.
     * 
     * @param resource
     */
    public void removeSpecialResource(CoapResource resource){    	
    	//coapServer.remove(resource);
    	californiumServer.removeRootResource(resource);
    }

    /* ------------------------------------------
	 * --- DataService callback methods ---
	 * ------------------------------------------ */
	/**
	 * Notifies that the DataService has established a connection.
	 * It includes every method and property changing on established connection.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onConnectionEstablished()
	 */
	@Override
	public void onConnectionEstablished() {
		if(coapBrokerOn){
			try {
				// Local KuraNode properties update
				updateNodeProperties();
				// Subscriber creation
				coapSubscriber = new CoAPSubscriber(thisNode);
				coapSubscriber.subscribeToTopics(dataService);
				// Publisher creation
				//coapPublisher = new CoAPPublisher(dataService, messagesId);
				coapPublisher = new CoAPPublisher(dataTransportService, messagesId);
				// Start the timer to connect to the tree.
				startTimer(CoAPTimerType.CONNECT_TO_TREE);
			} catch (NetworkInterfaceException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	/**
	 * Notifies a message arrival.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onMessageArrived(java.lang.String, byte[], int, boolean)
	 */
	@Override
	public void onMessageArrived(String topic, final byte[] payload, int qos, boolean retained) {
		chooseOperationExecutor.submit(new Runnable(){
			@Override
			public void run() {
				executeChooseOperation(payload);
			}
		});
	}

	/**
	 * Notifies that the DataService is about to cleanly disconnect.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onDisconnecting()
	 */
	@Override
	public void onDisconnecting() {
		// Unsubscribe to all topics
		coapSubscriber.unsubscribeToTopics(dataService);
		stopTimer();
	}
	
	/**
	 * Notifies that the DataService has cleanly disconnected.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onDisconnected()
	 */
	@Override
	public void onDisconnected() {
		stopTimer();
	}
	
	/**
	 * Notifies that the DataService has unexpectedly disconnected.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onConnectionLost(java.lang.Throwable)
	 */
	@Override
	public void onConnectionLost(Throwable cause) {
		// Stop the timer. It's useless in this situation to continue to check
		stopTimer();		
	}

	/*
	 * Notifies the a message has been published.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onMessagePublished(int, java.lang.String)
	 */
	@Override
	public void onMessagePublished(int messageId, String topic) {
//		if(topic.equals(CoAPTopic.PARENT_REQUEST_TOPIC)
//				|| topic.equals(CoAPTopic.ROOT_REQUEST_TOPIC)){
//			logger.info("Timer stopped. Waiting for publishing.");
//			stopTimer();
//		}
	}
	
	/*
	 * Confirms message delivery to the broker.
	 * 
	 * @see org.eclipse.kura.data.DataServiceListener#onMessageConfirmed(int, java.lang.String)
	 */
	@Override
	public void onMessageConfirmed(int messageId, String topic) {
//		if(topic.equals(CoAPTopic.PARENT_REQUEST_TOPIC)
//				|| topic.equals(CoAPTopic.ROOT_REQUEST_TOPIC)){
//			logger.info("Message confirmed. Restart request timer.");
//			restartTimer(CoAPTimerType.CONNECT_TO_TREE);
//		}
	}
	
	
	/* ------------------------------------------
	 * --- Broker callback methods ---
	 * ------------------------------------------ */
	/**
	 * Called when a Root node is added to the tree.
	 * 
	 * @param message MQTT message received.
	 */
	public void onAddedRoot(CoAPMessage message){
		
		KuraNode newRoot = (KuraNode)message.getMessage();
		KuraNode oldRoot = thisNode.getParentNode();
		
		// Check if there is already a parent and 
		// if the current parent node is the one received. If not do nothing.
		if(oldRoot != null && oldRoot.equals(newRoot)){
			logger.info("Parent node is already updated.");
		}else{
			thisNode.setParentNode(newRoot);
			coapPublisher.publishRootUpdated(thisNode, oldRoot);			
			
			KuraNodeUtils.sendAttributesToParent(thisNode,
													brokerCollection.getAttributeCollection(), 
													coapPublisher);
			
			// Case where thisNode is the root itself
			//if(thisNode.equals(newRoot)){} // Do nothing.
			
			logger.info("Parent node (root) updated to: {}", thisNode.getParentNode().getCompletePath());
		}
	}
	
	
	/**
	 * Called when a generic node has been added to the tree. Useless at the moment.
	 * 
	 * @param message MQTT message received.
	 */
	public void onAddedNode(CoAPMessage message){
		logger.info("Node " + message.getSender().getCompletePath() + " added.");
	}
	
	
	/**
	 * Called when a generic node has been removed from the tree. Useless at the moment.
	 * 
	 * @param message MQTT message received.
	 */
	public void onRemovedNode(CoAPMessage message){
		//KuraNode removedNode = (KuraNode)message.getMessage();
	}
	
	
	/**
	 * Called when a root is not available anymore.
	 * 
	 * @param message MQTT message received.
	 */
	public void onNotAvailableRoot(CoAPMessage message){
		KuraNode lostRoot = (KuraNode) message.getMessage();
		KuraNode currentParentNode = thisNode.getParentNode();
		
		// Check if the lost root is the current one
		if(currentParentNode != null && currentParentNode.equals(lostRoot)){
			thisNode.setParentNode(null);
			logger.info("Root removed from node --> {}", thisNode.getCompletePath());
			coapPublisher.publishRootUpdated(thisNode, lostRoot);
		// If parent was null and current root is null, don't send anything if receive next
		//	null root messages.
		}else if(currentParentNode == null && lostRoot == null){}
	}
	
	
	/**
	 * Called when there is a response for a parent request.
	 * 
	 * @param message MQTT message received.
	 */
	public void onParentResponse(CoAPMessage message){		
		onAvailableParent(message);
	}
	
	
	/**
	 * Called when there is an available parent.
	 * 
	 * @param message MQTT message received.
	 */
	public void onAvailableParent(CoAPMessage message){
		KuraNode newParent = (KuraNode)message.getMessage();
		KuraNode currentParent = thisNode.getParentNode();
		
		// Check if there is already a parent.
		if(newParent != null){				
			// Check if path of the parentNode is at least one level directly lower than the current one.
			if((currentParent != null && !PathUtils.isMoreGeneralThan(newParent.getRelativePath(),
													currentParent.getRelativePath()))
				|| newParent.equals(currentParent) // It means CTH needs an ACK on current parent.
				||	currentParent == null){ // Current null parent
				
				// Change parent and notify CTH.
				KuraNodeUtils.changeParentAndUpdate(thisNode, newParent,
						brokerCollection.getAttributeCollection(),
						coapPublisher);
				
			}
		// If it's null change it directly and publish a message to CTH
		}else{
			KuraNode oldParent = thisNode.getParentNode();
			thisNode.setParentNode(newParent);
			NodePrinter.printNode(thisNode, debugMode);
			// Publish an update to CTH node
			coapPublisher.publishParentUpdated(thisNode, oldParent);
		}
	}
	
	
	/**
	 * Called when a parent is not available anymore.
	 * 
	 * @param message MQTT message received.
	 */
	public void onNotAvailableParent(CoAPMessage message){
		KuraNode lostParent = (KuraNode) message.getMessage();
		KuraNode currentParentNode = thisNode.getParentNode();
		
		
		// Check failed node is the current parent
		if(currentParentNode != null && currentParentNode.equals(lostParent)){
			// Clear the parent node
			thisNode.setParentNode(null);
			logger.info("Parent removed from node --> " + thisNode.getCompletePath());
			// Send parent update to CTH
			logger.info("Update root on not available parent");
			coapPublisher.publishParentUpdated(thisNode, lostParent);
			// Request a new parent
			coapPublisher.publishParentRequest(thisNode);
		}
	}
	
	
	/**
	 * Called when a child is not available anymore.
	 * 
	 * @param message MQTT message received.
	 */
	public void onNotAvailableChild(CoAPMessage message){
		KuraNode notAvailableChild = (KuraNode) message.getMessage();
						
		// Remove node and its properties from the resource collection.
		AttributeCollection modifiedAttributes = brokerCollection.removeChild(notAvailableChild);
		
		// If there are modified general properties, update parent node
		if(modifiedAttributes.size() > 0){
			KuraNodeUtils.sendAttributesToParent(thisNode,
												brokerCollection.getAttributeCollection(),
												coapPublisher);
		}
	}
	
	
	/* ------------------------- Resources handling --------------------------- */
	/**
	 * Called on a resource update received from a child node.
	 * 
	 * @param message MQTT message received.
	 */
	public void onResourceUpdate(CoAPMessage message){
		KuraNode linkedNode = message.getSender();
		AttributeCollection newResources = (AttributeCollection) message.getMessage();
		AttributeCollection modifiedAttributes = new AttributeCollection();
		
		modifiedAttributes = brokerCollection.handleResourceUpdate(linkedNode, newResources);
		
		if(modifiedAttributes.size() > 0)
			KuraNodeUtils.sendAttributesToParent(thisNode,
													brokerCollection.getAttributeCollection(),
													coapPublisher);		
	}
	
	/**
	 * Called when receiving a query for specific resource properties.
	 * 
	 * @param message MQTT message received.
	 */
	public void onResourceQueryRequest(CoAPMessage message){
		RequestResult result;
		// Create two set for different responses
		// ResourceProperty to post on the Remote RD
		Set<ResourceProperty> resProperties = new HashSet<ResourceProperty>();
		// NodeProperty to send back to the query node
		Set<NodeProperty> nodeProperties = new HashSet<NodeProperty>();
		// Variable used in the checkPropagationOnParent
		String domain = "";
		String group = "";
		
		
		result = brokerCollection.handleResourceRequest(message.getSender(), (AttributeCollection) message.getMessage());
		
		
		domain = result.getDomain();
		group = result.getGroup();
		resProperties = result.getResProperties();
		nodeProperties = result.getNodeProperties();
		
		// Post resources to the remote Resource Directory
		if(resProperties.size() > 0){
			//clientDTLS.restartEndpoint();
			for(int i = 0; i < 500; i++){
				logger.info("Posting {} resources to sender's Resource Directory.", resProperties.size());
				if(dtlsOn){
					clientDTLS.postResources(thisNode, message.getSender(), resProperties);
				}else{
					remoteClient.postResources(thisNode, message.getSender(), resProperties);
				}
			}
			try{
				clientDTLS.closeSession(message.getSender());
			}catch(Throwable e){
				logger.info("NullPointer on closeSession.");
			}
		}else{
			logger.debug("No local resources to post on the sender's Resource Directory.");
		}
		
		// Check if we want to propagate the query to parentNode with specific domain and group
		if(KuraNodeUtils.checkPropagationOnParent(thisNode, message.getSender(), domain, group)){
			logger.info("Need to propagate also on parent.");
			nodeProperties.add(new NodeProperty(thisNode.getParentNode()));
		}else{
			logger.debug("No need to propagate on the parent.");
		}
		
		// Return Kura nodes for request query to sender.
		if(nodeProperties.size() > 0){
			logger.info("Returning info on possible matching nodes --> {}", nodeProperties.size());
			// Optimization for local search.
			if(message.getSender().equals(thisNode)){
				//executeRequest(KuraNodeUtils.createPropagationMessage(thisNode, message, nodeProperties));
				onQueryResponseReceived(KuraNodeUtils.createPropagationMessage(thisNode, message, nodeProperties));
			}else{
				coapPublisher.publishResourceQueryResponse(thisNode, message,
						(AttributeCollection) message.getMessage(), nodeProperties);
			}
		}else{
			logger.debug("No matching nodes for requested attributes.");
		}		
	}
	
	
	/**
	 * Called on receiving a response for a request query for resources.
	 * 
	 * @param message MQTT message received.
	 */
	public void onQueryResponseReceived(CoAPMessage message){
		
		PropagationObject propObj = (PropagationObject) message.getMessage();
		// Get properties from the propagation object.
		// Original message. Not used in this moment.
		//CoAPMessage originalMessage = propObj.getOriginalQuery();
		Set<NodeProperty> nodesP = propObj.getNodeProperties();
		AttributeCollection attributesToSearch = propObj.getAttributes();
		
		// Remove domain=all attribute if sender is the root so we toggle off forced propagation on parent
		LinkAttribute domainAllAttribute = new LinkAttribute("domain", "all");
		if (attributesToSearch.contains(domainAllAttribute) && message.getSender().getDomain().equals("*")){
			logger.debug("Removing domain=all attribute with root as last sender");
			attributesToSearch.remove(domainAllAttribute);
		}
		
		// Handle possible loop case with parent response
		for(NodeProperty nodeP : nodesP){
			if(!nodeP.getNode().equals(thisNode.getParentNode())){
				coapPublisher.publishSpecificResourceQueryRequest(thisNode, nodeP.getNode(), attributesToSearch);
				//new CoAPPublisher(dataService, messagesId).publishSpecificResourceQueryRequest(thisNode, nodeP.getNode(), attributesToSearch);
			}
		}
	}
	
	/**
	 * Called on receiving a message about a new online CTH
	 * 
	 * @param message
	 */
	public void onCTHConnected(CoAPMessage message){		
		// Set a new scheduler for the emergency procedure
		if(emergencyMode){
			stopTimer();
			timerExecutor.schedule(new CoAPTimer(CoAPTimerType.EMERGENCY_PROCEDURE),
													emergencyDelay,
													TimeUnit.MILLISECONDS);
			logger.info("New CTH Connected. Soft reboot between {} milliseconds.", emergencyDelay);
		}
	}
	
	/* ------------------------------------------
	 * --- Setting services ---
	 * ------------------------------------------ */
	protected void setDataService(DataService dataService){
		this.dataService = dataService;
	}
	protected void unsetDataService(DataService dataService){
		this.dataService = null;
	}
	public void unsetDataTransportService(DataTransportService dataTransportService) {
		this.dataTransportService = null;
	}
	public void setDataTransportService(DataTransportService dataTransportService) {
		this.dataTransportService = dataTransportService;
	}
	protected void setSystemProperties(SystemProperties systemProperties){
		this.systemProperties = systemProperties;
	}
	protected void unsetSystemProperties(SystemProperties systemProperties){
		this.systemProperties = null;
	}
	protected void setCaliforniumServer(CaliforniumServer californiumServer){
		this.californiumServer = californiumServer;
	}
	protected void unsetCaliforniumServer(CaliforniumServer californiumServer){
		this.californiumServer = null;
	}
	
	

    
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public KuraNode getServerNode() {
		return thisNode;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public CoAPPublisher getCoapPublisher() {
		return coapPublisher;
	}
	public void setCoapPublisher(CoAPPublisher coapPublisher) {
		this.coapPublisher = coapPublisher;
	}
	public Collection<Resource> getLocalResources(){
		return californiumServer.getLocalResources().getChildren();
	}
	public BrokerCollection getBrokerCollection() {
		return brokerCollection;
	}
	public void setBrokerCollection(BrokerCollection brokerCollection) {
		this.brokerCollection = brokerCollection;
	}
	public String getNetworkInterface() {
		return networkInterfaceName;
	}
	public void setNetworkInterface(String networkInterface) {
		this.networkInterfaceName = networkInterface;
	}
	public Long getMessagesId() {
		return messagesId;
	}
	public void setMessagesId(Long messagesId) {
		this.messagesId = messagesId;
	}

	/**
	 * Executor Runnable for resource query handling.
	 * 
	 * @author Gianvito Morena
	 *
	 */
	class CoAPThread implements Runnable{
		
		CoAPMessage message;
		
		public CoAPThread(CoAPMessage message){
			this.message = message;
		}
		
		@Override
		public void run() {
			switch(message.getOperation()){
				case ROOT_ADDED:
					logger.info("onMessageArrived. Root added message.");
					onAddedRoot(message);
					NodePrinter.printNode(thisNode, debugMode);
					break;
				case NODE_ADDED:
					logger.info("onMessageArrived. Node added message.");
					onAddedNode(message);
					break;
				case PARENT_AVAILABLE:
					logger.info("onMessageArrived. Parent available message.");
					onAvailableParent(message);
					NodePrinter.printNode(thisNode, debugMode);
					break;
				case ROOT_RESPONSE:
					logger.info("onMessageArrived. Root response message.");
					onAddedRoot(message);
					NodePrinter.printNode(thisNode, debugMode);
					break;
				case PARENT_RESPONSE:
					logger.info("onMessageArrived. Parent response message.");
					onParentResponse(message);
					NodePrinter.printNode(thisNode, debugMode);
					break;
				case ROOT_NOT_AVAILABLE:
					logger.info("onMessageArrived. Root not available message.");
					onNotAvailableRoot(message);
					NodePrinter.printNode(thisNode, debugMode);
					break;
				case PARENT_NOT_AVAILABLE:
					logger.info("onMessageArrived. Parent not available message.");
					onNotAvailableParent(message);
					NodePrinter.printNode(thisNode, debugMode);
					break;
				case CHILD_NOT_AVAILABLE:
					logger.info("onMessageArrived. Child not available message.");
					onNotAvailableChild(message);
					LinkedNodePrinter.printCollection(brokerCollection.getLinkedNodes(), debugMode);
					ResourcesPrinter.printCollection(brokerCollection.getResourceCollection(), debugMode);
					break;
				case RESOURCE_UPDATE:
					logger.info("onMessageArrived. Resource update message.");
					onResourceUpdate(message);
					LinkedNodePrinter.printCollection(brokerCollection.getLinkedNodes(), debugMode);
					ResourcesPrinter.printCollection(brokerCollection.getResourceCollection(), debugMode);
					break;
				case RESOURCE_QUERY_REQUEST:
					logger.info("onMessageArrived. Resource query request message.");
					QueryPrinter.printQueryRequest(message, debugMode);
					onResourceQueryRequest(message);
					break;
				case RESOURCE_QUERY_RESPONSE:
					logger.info("onMessageArrived. Resource query response message.");
					QueryPrinter.printQueryResponse(message, debugMode);
					onQueryResponseReceived(message);
					break;
				case CTH_ONLINE:
					logger.info("onMessageArrived. CTH online message.");
					onCTHConnected(message);
					break;
				default:
					break;
			}
		}
	}

	/**
	 * Add the execution of a Resource query to broker Executor.
	 * 
	 * @param message
	 */
	public void executeRequest(CoAPMessage message){
		resourceExecutor.execute(new CoAPThread(message));
	}
	
	/**
	 * Execute a callback handler.
	 * 
	 * @param message
	 */
	public void executeCallback(CoAPMessage message){
		callbackExecutor.execute(new CoAPThread(message));
	}
	
	/**
	 * Execute a choose operation handler.
	 * 
	 * @param payload
	 */
	public void executeChooseOperation(byte[] payload){
		//if(coapSubscriber.checkTopic(topic)){
		//logger.info("topic checked --> {}", new Date().getTime());
		try{
			CoAPMessage message = ByteObjectUtils.getObject(payload, CoAPMessage.class);
			//logger.info("getObjectDone --> {}", new Date().getTime());
			// Select the operation to execute
			switch(message.getOperation()){
				// When a root is added to the CTH
				case ROOT_ADDED:
					executeCallback(message);
					break;
				// On a root request response
				case ROOT_RESPONSE:
					executeCallback(message);
					break;
				case NODE_ADDED:
					executeCallback(message);
					break;
				// When a root node is not available anymore
				case ROOT_NOT_AVAILABLE:
					executeCallback(message);
					break;
				case PARENT_RESPONSE:
					executeCallback(message);
					break;
				// When a new parent is available
				case PARENT_AVAILABLE:
					executeCallback(message);
					break;
				// When a parent is not available anymore
				case PARENT_NOT_AVAILABLE:
					executeCallback(message);
					break;
				// When a child is not available anymore
				case CHILD_NOT_AVAILABLE:
					executeCallback(message);
					break;
				case RESOURCE_UPDATE:
					executeCallback(message);
					break;
				case RESOURCE_QUERY_REQUEST:
					executeRequest(message);
					break;
				case RESOURCE_QUERY_RESPONSE:
					executeRequest(message);
					break;
				case CTH_ONLINE:
					executeCallback(message);
					break;
				default:
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/* ------------------------------------------
	 * --- Timers ---
	 * ------------------------------------------ */
	class CoAPTimer implements Runnable{
		CoAPTimerType operation;
		
		public CoAPTimer(CoAPTimerType operation){
			super();
			this.operation = operation;
		}
		
		@Override
		public void run() {
			switch(operation){
				case CONNECT_TO_TREE:
					try {
						if(dataService.isConnected()) connectToTree();
					} catch (CoAPConfigurationException e) {
						e.printStackTrace();
					}
					break;
				case EMERGENCY_PROCEDURE:
					emergencyProcedure();
					break;
			}
		}
	}
	
	/**
	 * Start the timer.
	 * 
	 * @param operation
	 */
    public void startTimer(CoAPTimerType operation){
    	stopTimer();
    	
    	parentTimerHandler = timerExecutor.scheduleWithFixedDelay(new CoAPTimer(operation),
    									firstConnectionDelay,
    									parentUpdatePeriod,
    									TimeUnit.MILLISECONDS);
    }
    
    /**
	 * Restart the timer.
	 * 
	 * @param operation
	 */
    public void restartTimer(CoAPTimerType operation){
    	stopTimer();
    	
    	parentTimerHandler = timerExecutor.scheduleWithFixedDelay(new CoAPTimer(operation),
    								parentUpdatePeriod,
    								parentUpdatePeriod,
    								TimeUnit.MILLISECONDS);
    }
    
    /**
     * Stop the timer.
     * 
     * @param operation
     */
    public void stopTimer(){
    	if(parentTimerHandler != null){
    		parentTimerHandler.cancel(true);
    	}    	
    }
    
}