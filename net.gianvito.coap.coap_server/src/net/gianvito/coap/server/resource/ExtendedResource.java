package net.gianvito.coap.server.resource;

import java.util.Date;
import java.util.Set;

import net.gianvito.coap.resources.LinkAttribute;

import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

/**
 * Simple BrokerResource extension with a private value.
 * 
 * @author Gianvito Morena
 *
 */
public class ExtendedResource extends BrokerResource{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 8617163801023355421L;
	
	private String privateValue;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public ExtendedResource(){
		super("null");
	}
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 */
	public ExtendedResource(String name) {
		super(name);
	}
	
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param creationDate
	 */
	public ExtendedResource(String name, Date creationDate) {
		super(name, creationDate);
	}
	
	/**
	 * Constructor
	 * 
	 * @param name
	 * @param resourceType
	 * @param interfaceDescription
	 * @param attributes
	 */
	public ExtendedResource(String name, String resourceType, String interfaceDescription,
								Set<LinkAttribute> attributes) {
		super(name, resourceType, interfaceDescription, attributes);
	}
	
	
	
	@Override
	public void handleGET(CoapExchange exchange) {
		exchange.respond(ResponseCode.CONTENT, privateValue);
	}
	
	@Override
	public void handlePOST(CoapExchange exchange) {
		privateValue = exchange.getRequestPayload().toString();
		exchange.respond(ResponseCode.CREATED, privateValue);
	}
	
	@Override
	public void handleDELETE(CoapExchange exchange) {
		this.privateValue = "";
		exchange.respond(ResponseCode.DELETED, privateValue);
	}
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public String getPrivateValue(){
		return privateValue;
	}
	public void setPrivateValue(String privateValue) {
		this.privateValue = privateValue;
		this.changed();
	}
}

