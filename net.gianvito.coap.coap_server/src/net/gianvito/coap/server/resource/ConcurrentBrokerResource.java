package net.gianvito.coap.server.resource;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;
import net.gianvito.coap.server.CoAPServer;

import org.eclipse.californium.core.server.resources.ConcurrentCoapResource;

/**
 * Minimal resource with broker capabilities with dedicated thread.
 * 
 * @author Gianvito Morena
 *
 */
public class ConcurrentBrokerResource extends ConcurrentCoapResource implements Serializable{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 9059705997512700515L;
	
	private String domain;
	private String group;
	private Date creationDate;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public ConcurrentBrokerResource(){
		super("null");
	}
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 */
	public ConcurrentBrokerResource(String name){
		super(name);
		setObservable(true);
		this.creationDate = new Date();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param creationDate
	 */
	public ConcurrentBrokerResource(String name, Date creationDate) {
		super(name);
		setObservable(true);
		this.creationDate = creationDate;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param resourceType
	 * @param interfaceDescription
	 * @param attributes
	 */
	public ConcurrentBrokerResource(String name, String resourceType, String interfaceDescription,
								Set<LinkAttribute> attributes){
		
		super(name);
		setObservable(true);
		this.creationDate = new Date();
		this.setResourceType(resourceType);
		this.addInterfaceDescription(interfaceDescription);
		addAttributes(attributes);
	}
	
	
	/*
	 * Helpers for attribute method
	 */
	public void addAttribute(String name, String value){
		this.getAttributes().addAttribute(name, value);
	}
	public void removeAttribute(String name, String value){
		this.getAttributes().clearAttribute(name);
	}
	public void setResourceType(String value){
		this.getAttributes().addResourceType(value);
	}
	public List<String> getResourceType(){
		return this.getAttributes().getResourceTypes();
	}
	public void addInterfaceDescription(String value){
		this.getAttributes().addResourceType(value);
	}
	public List<String> getInterfaceDescription(){
		return this.getAttributes().getInterfaceDescriptions();
	}
	public void addAttributes(Set<LinkAttribute> properties){
		for(LinkAttribute property : properties){
			this.addAttribute(property.getName(), property.getValue());
		}
	}
	
	
	/**
	 * Helper for obtaining broker information.
	 * 
	 * @param cs
	 */
	public void updateDomainAndGroup(CoAPServer cs){
		if(cs != null && cs.getDomain() != null && cs.getGroup() != null){
			String domain = cs.getDomain();
			String group = cs.getGroup();
			if(!domain.equals("")){
				if(domain.equals("*")) domain = "root";
				this.addAttribute("domain", domain);
			}
			if(!group.equals("")){
				if(group.equals("*")) group = "all";
				this.addAttribute("group", group);
			}
		}
	}
	
	/**
	 * Useless at the moment.
	 * 
	 * @param domain
	 * @param group
	 */
	public void onDomainAndGroupChange(String domain, String group) {
		if(domain != null && !domain.equals("")) this.addAttribute("domain", domain);
		if(group != null && !group.equals("")) this.addAttribute("group", group);
	}
	

	public void onNodeChange(KuraNode node) {
		
	}
	
	
	/* ------------------------------------------
	 * --- Getters and setters ---
	 * ------------------------------------------ */
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

}
