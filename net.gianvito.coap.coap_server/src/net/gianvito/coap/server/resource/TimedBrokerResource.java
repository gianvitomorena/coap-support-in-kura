package net.gianvito.coap.server.resource;

import java.util.Date;
import java.util.Set;

import net.gianvito.coap.resources.LinkAttribute;

import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

/**
 * CoAPResource with validity interval integrated.
 * It doesn't respond to the REST request after some time (specified) 
 * it was created.
 * In that case a ResponseCode equals to SERVICE_UNAVAILABLE is sent.
 * 
 * @author Gianvito Morena
 *
 */
public class TimedBrokerResource extends ConcurrentBrokerResource{
	
	/* ----------------------- Attributes -------------------------- */
	private static final long serialVersionUID = 4100676614192054801L;
	
	private Integer validTimeInterval = 1000;
	private Date validDate;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public TimedBrokerResource(){
		super("null");
	}
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 */
	public TimedBrokerResource(String name) {
		super(name, new Date());
		validDate = new Date(this.getCreationDate().getTime()/1000 + validTimeInterval);
	}
	
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param creationDate
	 */
	public TimedBrokerResource(String name, Date creationDate) {
		super(name, creationDate);
		validDate = new Date(this.getCreationDate().getTime()/1000 + validTimeInterval);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param resourceType
	 * @param interfaceDescription
	 * @param attributes
	 */
	public TimedBrokerResource(String name, String resourceType, String interfaceDescription,
								Set<LinkAttribute> attributes) {
		super(name, resourceType, interfaceDescription, attributes);
		this.setCreationDate(new Date());
		validDate = new Date(this.getCreationDate().getTime()/1000 + validTimeInterval);
	}
	
	
	@Override
	public void handleGET(CoapExchange exchange) {
		Boolean valid = checkValidity();
		if(!valid){
			exchange.respond(ResponseCode.SERVICE_UNAVAILABLE);
			return;
		}
	}
	
	@Override
	public void handlePOST(CoapExchange exchange) {
		Boolean valid = checkValidity();
		if(!valid){
			exchange.respond(ResponseCode.SERVICE_UNAVAILABLE);
			return;
		}
	}
	
	@Override
	public void handlePUT(CoapExchange exchange) {
		Boolean valid = checkValidity();
		if(!valid){
			exchange.respond(ResponseCode.SERVICE_UNAVAILABLE);
			return;
		}
	}
	
	@Override
	public void handleDELETE(CoapExchange exchange) {
		Boolean valid = checkValidity();
		if(!valid){
			exchange.respond(ResponseCode.SERVICE_UNAVAILABLE);
			return;
		}
	}
	
	
	/**
	 * Check Resource validity before process any action.
	 * 
	 * @return Boolean related to validity of this Resource
	 */
	public Boolean checkValidity(){
		Date now = new Date();
		if(now.after(validDate)){
			return false;
		}else{
			return true;
		}	
	}
}
