package net.gianvito.coap.server.utils;

import java.util.Set;

import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.message.PropagationObject;
import net.gianvito.coap.server.mqtt.CoAPPublisher;
import net.gianvito.coap.type.CoAPOperation;
import net.gianvito.coap.utils.PathUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utility for manipulating KuraNode attributes.
 * 
 * @author Gianvito Morena
 *
 */
public class KuraNodeUtils {
	private static final Logger logger = LoggerFactory.getLogger(KuraNodeUtils.class);
	
	
	/**
	 * Returns MAC address without ":".
	 * 
	 * @param node
	 * @return
	 */
	public static String getMACForRD (KuraNode node){
		return node.getMacAddress().replace(":", "");
	}
	
	/**
	 * Returns IP address without "."
	 * 
	 * @param node
	 * @return
	 */
	public static String getAddressForRD (KuraNode node){
		return node.getKuraAddress().replace(".", "");
	}
	
	/**
	 * Returns name used in the "rd" attribute for the Resource Directory.
	 * 
	 * @param node
	 * @return
	 */
	public static String getEPNameForRD (KuraNode node){
		return getAddressForRD(node) + getMACForRD(node);
	}
	
	/**
	 * Return node level. Useful in subscribing to specific topics.
	 * 
	 * @param node node to get level from.
	 * @return level on the gerarchy.
	 */
	public static int getNodeLevel(KuraNode node){
		int level = 0;
		String domain = node.getDomain();
		String group = node.getGroup();
		
		if(domain.equals("*")){
			level = 0;
		}else{
			level = group.equals("*")? 1 : 2;
		}
		
		return level;
	}
	
	/**
	 * Allows to know if it worths to return also the parent node.
	 * 
	 * @param domain
	 * @param group
	 * @return true if query have to propagate on the parent node.
	 */
	public static Boolean checkPropagationOnParent(KuraNode serverNode, KuraNode requestNode, String domain, String group){
		Boolean operationResult = false;
		
		if(serverNode.getParentNode() != null && !serverNode.getParentNode().equals(serverNode)){
			
			String serverNodePath = serverNode.getRelativePath();
			logger.debug("serverNode path --> {}", serverNode.getRelativePath());
			String parentNodePath = serverNode.getParentNode().getRelativePath();
			logger.debug("parentNode path --> {}", parentNodePath);
			String requestNodePath = requestNode.getRelativePath();
			logger.debug("requestNode path --> {}", requestNodePath);			
			String specificQueryPath = PathUtils.buildRelativeNodePath(domain, group);
			logger.debug("specificQueryPath --> {}", specificQueryPath);
			
			// Same domain to search.
			if(serverNode.getDomain().equals(domain)){
				// Search path more general than current path
				if(PathUtils.isMoreGeneralThan(specificQueryPath, serverNodePath)){
					// It has to be at least in the same group relative to the sender.
					if(PathUtils.isMoreGeneralThan(serverNodePath, requestNodePath))
						operationResult = true;
						
				}
			// Different domain to search.
			}else{
				// Only if serverNode is in the path to the root.
				if(PathUtils.isMoreGeneralThan(serverNodePath, requestNodePath))
					operationResult = true;				
			}
			
			
			// Different domain search active. We always want to propagate all the way to the root.
			if(domain.equals("all") &&
						serverNode.getParentNode() != null && // No null parent node.
						!serverNode.getParentNode().equals(serverNode) && // Avoid cycle between root and root.
						//serverNode.getDomain().equals(requestNode.getDomain()) && // Same domain node
						PathUtils.isMoreGeneralThan(parentNodePath, requestNodePath)){ // Same domain and more general
				operationResult = true;
			}
		}
		
		
		return operationResult;
	}
	
	/**
	 * Send current properties to the parent.
	 * 
	 * @param serverNode
	 * @param currentResources
	 * @param coapPublisher
	 */
	public static void sendAttributesToParent(KuraNode serverNode, AttributeCollection currentResources,
										CoAPPublisher coapPublisher){
		// Check if it's a root node (avoiding self send) or parentNode is null
		if(serverNode != null && serverNode.getParentNode() != null){
			if(!serverNode.getParentNode().equals(serverNode)){
				coapPublisher.publishResourceUpdateToParent(serverNode, currentResources);
			}
		}
	}
	
	/**
     * Changes parent and sends updated attributes to it.
     * 
     * @param newParent new parent node to connect to.
     */
	public static void changeParentAndUpdate(KuraNode serverNode, KuraNode newParent,
											AttributeCollection currentResources, CoAPPublisher coapPublisher){
		
		KuraNode oldParent = serverNode.getParentNode();
		serverNode.setParentNode(newParent);
		// Publish an update to CTH node
		coapPublisher.publishParentUpdated(serverNode, oldParent);
		// Publish update attributes to parent
		KuraNodeUtils.sendAttributesToParent(serverNode,
				 							currentResources,
											coapPublisher);
		
	}
	
	/**
	 * Create a propagation message for local search.
	 * 
	 * @param requestNode
	 * @param originalMessage
	 * @param nodeProperties
	 * @return
	 */
	public static CoAPMessage createPropagationMessage(KuraNode requestNode, CoAPMessage originalMessage, Set<NodeProperty> nodeProperties){
		PropagationObject propObj = new PropagationObject(originalMessage, (AttributeCollection) originalMessage.getMessage(), nodeProperties);
		return new CoAPMessage(new Long(0), requestNode, propObj, CoAPOperation.RESOURCE_QUERY_RESPONSE);
	}
	
}
