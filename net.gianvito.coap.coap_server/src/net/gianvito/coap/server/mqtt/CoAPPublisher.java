package net.gianvito.coap.server.mqtt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.message.PropagationObject;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.mqtt.CoAPTopic;
import net.gianvito.coap.type.CoAPOperation;
import net.gianvito.coap.utils.ByteObjectUtils;
import net.gianvito.coap.utils.MessageUtils;
import net.gianvito.coap.utils.PathUtils;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.KuraNotConnectedException;
import org.eclipse.kura.KuraStoreException;
import org.eclipse.kura.KuraTooManyInflightMessagesException;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataTransportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CoAP Server Publisher for MQTT broker.
 * Contains every method used to interact with other brokers and CTH.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPPublisher {
	
	/* ----------------------- Attributes -------------------------- */
	
	// Logger
    private static final Logger logger = LoggerFactory.getLogger(CoAPPublisher.class);
	
	// Services and system utils
	private DataService dataService;
	private DataTransportService dataTransportService;
	
	
	private long messagesId;
	
	/* --- Topics --- */
	// Root topics
    //private static final String ROOT_REQUEST_TOPIC = "coap/root/request";
    //private static final String ROOT_ON_NODE_UPDATED_TOPIC = "coap/node/root/updated";
    // Node topics
    //private static final String PARENT_REQUEST_TOPIC = "coap/parent/request";
    //private static final String PARENT_ON_NODE_UPDATED_TOPIC = "coap/node/parent/updated";
    //private static final String NODE_REMOVE_TOPIC = "coap/node/remove";
    // Resource topics
    //private static final String RESOURCE_UPDATE_TOPIC = "/coap/resource/update";
    //private static final String RESOURCE_QUERY_REQUEST_TOPIC = "/coap/resource/request";
    //private static final String RESOURCE_QUERY_RESPONSE_TOPIC = "/coap/resource/response";
	
    /* CoAP --> MQTT publish properties */
	private static final Integer RESOURCE_REQUEST_PUBLISH_QOS = 0;
    private static final Integer NORMAL_PUBLISH_QOS = 1;
    private static final Integer ROOT_BROKER_TOPIC_PUBLISH_QOS = 2;
    private static final Integer NODE_BROKER_TOPIC_PUBLISH_QOS = 2;
    // Retain properties.
    private static final Boolean COAP_BROKER_RETAIN = false;
    // Priority properties.
    private static final Integer BROKER_CTH_PRIORITY = 0;
    private static final Integer BROKER_BROKER_PRIORITY = 2;
    private static final Integer BROKER_RESOURCE_PRIORITY = 9;
    private static final Integer LOWEST_PRIORITY = 10;
        
 	
 	/* ----------------------- Methods ----------------------------- */
    /*
	 * ------------------------------------------
	 * --- Constructors ---
	 * ------------------------------------------
	 */
 	/**
 	 * Constructor
 	 * 
 	 * @param dataService
 	 * @param networkService
 	 */
	public CoAPPublisher(DataService dataService, Long messagesId){
		this.dataService = dataService;
		this.messagesId = messagesId;
	}
	
	/**
 	 * Constructor
 	 * 
 	 * @param dataService
 	 * @param networkService
 	 */
	public CoAPPublisher(DataTransportService dataTransportService, Long messagesId){
		this.dataTransportService = dataTransportService;
		this.messagesId = messagesId;
	}
	
	/*
	 * ------------------------------------------
	 * --- Publish methods ---
	 * ------------------------------------------
	 */
	/**
	 * Publish a root request.
	 * 
	 * @param requestNode
	 */
	public Integer publishRootRequest(KuraNode requestNode){
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
							requestNode, "root_request", CoAPOperation.ROOT_REQUEST);
		Integer token = publishMessage(message, CoAPTopic.ROOT_REQUEST_TOPIC, null, 
										ROOT_BROKER_TOPIC_PUBLISH_QOS, null, BROKER_CTH_PRIORITY);
		
		logger.debug("Root request published.");
		return token;
	}
	
	/**
	 * Send information about updating of the root node on local Kura broker.
	 * 
	 * @param requestNode
	 * @param oldRoot
	 */
	public Integer publishRootUpdated(KuraNode requestNode, KuraNode oldRoot){
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
								requestNode, oldRoot, CoAPOperation.ROOT_ON_NODE_UPDATED);
		Integer token = publishMessage(message, CoAPTopic.ROOT_ON_NODE_UPDATED_TOPIC, null, 
										ROOT_BROKER_TOPIC_PUBLISH_QOS, null, BROKER_CTH_PRIORITY);		
		logger.debug("Root on node update published.");
		return token;
	}
	
	/**
	 * Publish a parent request.
	 * 
	 * @param requestNode
	 */
	public Integer publishParentRequest(KuraNode requestNode){
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
									requestNode, "parent_request", CoAPOperation.PARENT_REQUEST);
		Integer token = publishMessage(message, CoAPTopic.PARENT_REQUEST_TOPIC, null, 
										NODE_BROKER_TOPIC_PUBLISH_QOS, null, BROKER_CTH_PRIORITY);
		logger.debug("Parent request published.");
		return token;
	}
	
	/**
	 * Publish the information about the update of parent node on local broker.
	 * 
	 * @param requestNode
	 * @param oldParent
	 */
	public Integer publishParentUpdated(KuraNode requestNode, KuraNode oldParent){
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
									requestNode, oldParent, CoAPOperation.PARENT_ON_NODE_UPDATED);		
		Integer token = publishMessage(message, CoAPTopic.PARENT_ON_NODE_UPDATED_TOPIC, null, 
										NODE_BROKER_TOPIC_PUBLISH_QOS, null, BROKER_CTH_PRIORITY);
		logger.debug("Parent on node update published.");
		return token;
	}
	
	
	/**
	 * Publish a NODE_REMOVE message to CTH.
	 * 
	 * @param requestNode
	 */
	public Integer publishDisconnectFromTree(KuraNode requestNode){
		CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
									requestNode, "node_remove", CoAPOperation.NODE_REMOVE);
		Integer token = publishMessage(message, CoAPTopic.NODE_REMOVE_TOPIC, null, 
										NODE_BROKER_TOPIC_PUBLISH_QOS, null, BROKER_CTH_PRIORITY);
		return token;
    }
	
	/**
	 * Publish resource updates to parent.
	 * 
	 * @param requestNode
	 * @param newAttributes
	 */
	public Integer publishResourceUpdateToParent(KuraNode requestNode, AttributeCollection newAttributes){
		Integer token = null;
		
		if(requestNode.getParentNode() != null){
			CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
										requestNode, newAttributes, CoAPOperation.RESOURCE_UPDATE);
			token = publishMessage(message, CoAPTopic.RESOURCE_UPDATE_TOPIC, requestNode.getParentNode(), 
									null, null, BROKER_BROKER_PRIORITY);
		}
		
		return token;
	}
	
	/**
	 * Publish resource query to parent node.
	 * 
	 * @param requestNode
	 * @param attributes
	 */
	public Integer publishResourceQueryRequest(KuraNode requestNode, AttributeCollection attributes){
		Integer token = null;
		
		if(requestNode.getParentNode() != null){
			CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
									requestNode, attributes, CoAPOperation.RESOURCE_QUERY_REQUEST);
			token = publishMessage(message, CoAPTopic.RESOURCE_QUERY_REQUEST_TOPIC,
						requestNode.getParentNode(),
						RESOURCE_REQUEST_PUBLISH_QOS, null, BROKER_RESOURCE_PRIORITY);
		}
		
		return token;
	}
	
	
	/**
	 * Publish resource query to a specific node.
	 * 
	 * @param requestNode
	 * @param destination
	 * @param attributes
	 */
	public Integer publishSpecificResourceQueryRequest(KuraNode requestNode, KuraNode destination,
														AttributeCollection attributes){
		Integer token = null;
		
		if(destination != null){
			CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
										requestNode, attributes, CoAPOperation.RESOURCE_QUERY_REQUEST);
			token = publishMessage(message, CoAPTopic.RESOURCE_QUERY_REQUEST_TOPIC,
						destination,
						RESOURCE_REQUEST_PUBLISH_QOS, null, BROKER_RESOURCE_PRIORITY);
		}
		
		return token;
	}
	
	
	/**
	 * Publish response for a resource query.
	 * 
	 * @param requestNode
	 * @param originalQuery original query message. Used to check if continue to publish requests.
	 * @param attributes attributes to search.
	 * @param nodeProperties nodes to query
	 */
	public Integer publishResourceQueryResponse(KuraNode requestNode, CoAPMessage originalQuery, 
													AttributeCollection attributes, Set<NodeProperty> nodeProperties){
		Integer token = null;
		
		if(originalQuery.getSender() != null){
			PropagationObject propObj = new PropagationObject(originalQuery, attributes, nodeProperties);
			CoAPMessage message = new CoAPMessage(MessageUtils.createSerialNumber(messagesId),
										requestNode, propObj, CoAPOperation.RESOURCE_QUERY_RESPONSE);
			token = publishMessage(message, CoAPTopic.RESOURCE_QUERY_RESPONSE_TOPIC, 
							originalQuery.getSender(),
							RESOURCE_REQUEST_PUBLISH_QOS, null, BROKER_RESOURCE_PRIORITY);
		}
		
		return token;
	}
	
	
	
	
	
	/**
	 * Standard method to publish to multiple nodes.
	 * 
	 * @param destinationTopic
	 * @param message
	 * @param nodes
	 */
	public List<Integer> publishToMultipleNodes(String destinationTopic, CoAPMessage message, Set<KuraNode> nodes){
		List<Integer> tokenList = new ArrayList<Integer>();
		
		for(KuraNode node : nodes){
			Integer token = publishMessage(message, destinationTopic, node, null, null, null);
			if(token != null) tokenList.add(token);
		}
		
		return tokenList;
	}
	
	
	/**
	 * Standard method to publish a message to a specified destination.
	 * 
	 * @param message
	 * @param destinationTopic
	 * @param destination
	 * @param qos
	 * @param retain
	 * @param priority
	 */
	public Integer publishMessage(CoAPMessage message, String destinationTopic, KuraNode destination,
								Integer qos, Boolean retain, Integer priority){
		
		
		
		Integer messageID = null;

		// Publish the message to the sender if destination is null
		String topicToSend = destination != null? CoAPKey.CONTROL_PREFIX + 
				PathUtils.buildCompleteNodePath(destination) + destinationTopic : "";
		
		topicToSend = (topicToSend.equals(""))? destinationTopic : topicToSend;
		
		Integer finalQoS = qos == null? NORMAL_PUBLISH_QOS : qos;
		Boolean finalRetain = retain == null? COAP_BROKER_RETAIN : retain;
		Integer finalPriority = priority == null? LOWEST_PRIORITY : priority;
		
		try {
//			messageID = dataService.publish(topicToSend,
//								ByteObjectUtils.toByteArray(message),
//								finalQoS,
//								finalRetain, 
//								finalPriority);
			dataTransportService.publish(topicToSend,
					ByteObjectUtils.toByteArray(message),
					finalQoS,
					finalRetain);
			logger.debug("Message published on {}", topicToSend);
		} catch (KuraStoreException e) {
			e.printStackTrace();
			messageID = -1;
		} catch (KuraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return messageID;
	}

	
	
	/* ------------------------------------------
	 * --- Kura Services ---
	 * ------------------------------------------ */
	public DataService getDataService() {
		return dataService;
	}
	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}
}
