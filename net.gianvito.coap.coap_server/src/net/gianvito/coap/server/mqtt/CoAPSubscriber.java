package net.gianvito.coap.server.mqtt;

import java.util.HashSet;
import java.util.Set;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.mqtt.CoAPTopic;
import net.gianvito.coap.server.utils.KuraNodeUtils;
import net.gianvito.coap.utils.PathUtils;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.KuraNotConnectedException;
import org.eclipse.kura.KuraTimeoutException;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataTransportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MQTT Subscriber used by CoAP broker to specify interest in particular topics
 * which change depending on the level in hierarchy.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPSubscriber {
	
	/* ----------------------- Attributes -------------------------- */
	
	// Logger
	private static final Logger logger = LoggerFactory.getLogger(CoAPSubscriber.class);
	
	/* ---- CoAP Server Topics ---- */
	// CTH topics
	//public static final String CTH_ONLINE_TOPIC = "coap/cth/online";
	
	// Root topics
    //public static final String ROOT_ADDED_TOPIC = "coap/root/added";
    //public static final String ROOT_NOT_AVAILABLE_TOPIC = "coap/root/not_available";
    //public static final String ROOT_RESPONSE_TOPIC = "/coap/root/response";
    
    // Node topics
    //public static final String NODE_ADDED_TOPIC = "coap/node/added";
    //public static final String PARENT_RESPONSE_TOPIC = "/coap/parent/response";
    //public static final String PARENT_AVAILABLE_TOPIC = "/coap/parent/available";
    //public static final String PARENT_NOT_AVAILABLE_TOPIC = "/coap/parent/not_available";
    //public static final String CHILD_NOT_AVAILABLE_TOPIC = "/coap/child/not_available";
    // Resource topics
    //public static final String RESOURCE_UPDATE_TOPIC = "/coap/resource/update";
    //public static final String RESOURCE_QUERY_REQUEST_TOPIC = "/coap/resource/request";
    //public static final String RESOURCE_QUERY_RESPONSE_TOPIC = "/coap/resource/response";

    // List of topics
    private Set<String> topics;
    // Subscription properties
    String nodePath;
    int nodeLevel;
    
    // CoAP --> MQTT subscribe properties
    // Subscribe level for CTH_ONLINE messages.
    private static final Integer CTH_TOPIC_BROKER_SUBSCRIBE_QOS = 2;
    // Normal subscribe level.
    private static final Integer BROKER_SUBSCRIBE_QOS = 1;
    // Subscribe level for root topics.
    private static final Integer ROOT_TOPIC_BROKER_SUBSCRIBE_QOS = 2;
    // Subscriber level for node topics.
    private static final Integer NODE_TOPIC_BROKER_SUBSCRIBE_QOS = 2;
    // Subscriber level for resource request-response.
    private static final Integer REQ_RESP_TOPIC_BROKER_SUBSCRIBE_QOS = 0;
    

    /* ----------------------- Methods ----------------------------- */
    
    /**
     * Constructor.
     */
    public CoAPSubscriber(KuraNode serverNode){
    	topics = new HashSet<String>();
    	
    	nodePath = PathUtils.buildCompleteNodePath(serverNode);
    	nodePath = CoAPKey.CONTROL_PREFIX + nodePath;
    	
    	nodeLevel = KuraNodeUtils.getNodeLevel(serverNode);
    	
    	// CTH topics
    	topics.add(CoAPTopic.CTH_ONLINE_TOPIC);
    	
    	if(nodeLevel <= 1){
    		// Root topics
    		topics.add(CoAPTopic.ROOT_ADDED_TOPIC);
    		topics.add(CoAPTopic.ROOT_NOT_AVAILABLE_TOPIC);
    	}
    		
    	// Resource topics
    	topics.add(nodePath + CoAPTopic.RESOURCE_UPDATE_TOPIC);
    	topics.add(nodePath + CoAPTopic.RESOURCE_QUERY_REQUEST_TOPIC);
    	topics.add(nodePath + CoAPTopic.RESOURCE_QUERY_RESPONSE_TOPIC);
    	
    	// Node topics
    	topics.add(CoAPTopic.NODE_ADDED_TOPIC);
    	topics.add(nodePath + CoAPTopic.PARENT_AVAILABLE_TOPIC);
    	topics.add(nodePath + CoAPTopic.PARENT_NOT_AVAILABLE_TOPIC);
    	topics.add(nodePath + CoAPTopic.CHILD_NOT_AVAILABLE_TOPIC);
    	topics.add(nodePath + CoAPTopic.ROOT_RESPONSE_TOPIC);
    	topics.add(nodePath + CoAPTopic.PARENT_RESPONSE_TOPIC);
    }
    
    /**
     * Subscribe to topics and connect to the tree.
     * 
     * @param serverNode
     * @param dataService
     */
	public void subscribeToTopics(DataService dataService){					
		logger.info("Subscribing to CoAP Broker topics");
		try {
			
			// --- Tree topics ---
			// Resource topics
			dataService.subscribe(nodePath + CoAPTopic.RESOURCE_UPDATE_TOPIC, BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.RESOURCE_QUERY_REQUEST_TOPIC, REQ_RESP_TOPIC_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.RESOURCE_QUERY_RESPONSE_TOPIC, REQ_RESP_TOPIC_BROKER_SUBSCRIBE_QOS);
			
			// CTH topics
			dataService.subscribe(CoAPTopic.CTH_ONLINE_TOPIC, CTH_TOPIC_BROKER_SUBSCRIBE_QOS);
			
			// Root topics
			if(nodeLevel <= 1){
				// Root topics	
				dataService.subscribe(CoAPTopic.ROOT_ADDED_TOPIC, ROOT_TOPIC_BROKER_SUBSCRIBE_QOS);
				dataService.subscribe(CoAPTopic.ROOT_NOT_AVAILABLE_TOPIC, ROOT_TOPIC_BROKER_SUBSCRIBE_QOS);
			}
			
			// Node topics
			dataService.subscribe(CoAPTopic.NODE_ADDED_TOPIC, NODE_TOPIC_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.PARENT_AVAILABLE_TOPIC, NODE_TOPIC_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.PARENT_NOT_AVAILABLE_TOPIC, NODE_TOPIC_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.CHILD_NOT_AVAILABLE_TOPIC, NODE_TOPIC_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.ROOT_RESPONSE_TOPIC, NODE_TOPIC_BROKER_SUBSCRIBE_QOS);
			dataService.subscribe(nodePath + CoAPTopic.PARENT_RESPONSE_TOPIC, NODE_TOPIC_BROKER_SUBSCRIBE_QOS);
			
		} catch (KuraTimeoutException e) {
			e.printStackTrace();
		} catch (KuraNotConnectedException e) {
			e.printStackTrace();
		} catch (KuraException e) {
			e.printStackTrace();
		} catch (NullPointerException e){
			logger.error("Node path error. Null.");
		}
	}
	
	/**
	 * Unsubscribe to topics.
	 * 
	 * @param serverNode
	 * @param dataService
	 */
	public void unsubscribeToTopics(DataService dataService){
		try {
			// Tree topics
			//dataService.unsubscribe(ROOT_ADDED_TOPIC);
			//dataService.unsubscribe(ROOT_REMOVE_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.PARENT_AVAILABLE_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.PARENT_NOT_AVAILABLE_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.CHILD_NOT_AVAILABLE_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.ROOT_RESPONSE_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.PARENT_RESPONSE_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.RESOURCE_QUERY_REQUEST_TOPIC);
			dataService.unsubscribe(nodePath + CoAPTopic.RESOURCE_QUERY_RESPONSE_TOPIC);
			// Resource topics
			dataService.unsubscribe(nodePath + CoAPTopic.RESOURCE_UPDATE_TOPIC);
			// Remove all topics
			topics.clear();
		} catch (KuraTimeoutException e) {
			e.printStackTrace();
		} catch (KuraNotConnectedException e) {
			e.printStackTrace();
		} catch (KuraException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Check if topic is among the subscribed ones.
	 * 
	 * @param topic
	 * @return
	 */
	public Boolean checkTopic(String topic){
		if(topic != null){
			return topics.contains(topic);
		}else{
			return false;
		}
	}
}
