package net.gianvito.coap.client;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.server.utils.KuraNodeUtils;

import org.eclipse.californium.core.CaliforniumLogger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Remote client for ResourceDirectory.
 * Adds remote resources to the query requester Resource Directory.
 * 
 * @author Gianvito Morena
 *
 */
public class CoAPRemoteClient {
	
	static{
		CaliforniumLogger.disableLogging();
	}
	
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(CoAPRemoteClient.class);
	
	
	ExecutorService executor;
	CoapClient client;
		
	/* ----------------------- Methods ----------------------------- */
	
	public CoAPRemoteClient(){
		// Best performance --> newCachedThreadPool.
		executor = Executors.newCachedThreadPool();
		client = new CoapClient();
		client.setExecutor(executor);
	}
	
	
	/**
	 * Post resources to the remote Resource Directory
	 * 
	 * @param destination
	 * @param resourcesP
	 */
	public void postResources(KuraNode localNode, KuraNode destination, Set<ResourceProperty> resourcesP){
		CoapClient clientEx = client.useExecutor();
		String payload = "";
		
		client.setURI(buildCoAPLink(localNode, destination));
		
		// Payload creation.
		for(ResourceProperty resource : resourcesP){
			payload += LinkFormat.serializeResource(resource.getResource()).toString();
		}
		
		// Send resources. Asynchronously.
//		clientEx.post(new CoapHandler(){
//
//			@Override
//			public void onError() {
//				logger.info("Error in posting resources.");
//			}
//
//			@Override
//			public void onLoad(CoapResponse response) {
//				logger.info("Post done. {}", response.getCode().name().toString());
//			}
//				
//			}, payload, MediaTypeRegistry.APPLICATION_LINK_FORMAT);
		
		CoapResponse response = clientEx.post(payload, MediaTypeRegistry.APPLICATION_LINK_FORMAT);
		logger.info("Response --> {}", response.getCode().name().toString());	
	}
	
	
	/**
	 * Post local resources to the specified destination. Only for test purpose.
	 * 
	 * @param destination
	 * @param resources
	 * @return
	 */
	public static CoapResponse postLocalResources(Set<Resource> resources){
		CoapClient client = new CoapClient();
		String payload = new String();
		
		client.setURI(buildLocalCoAPLink());
		
		// Payload creation.
		for(Resource resource : resources){
			payload += LinkFormat.serializeResource(resource).toString();
		}
		
		logger.info("CoAP Request payload --> " + payload);
				
		return client.post(payload, MediaTypeRegistry.TEXT_PLAIN);
		
	}
	
	/**
	 * Utility for building a correct CoAP Link.
	 * 
	 * @param destination
	 * @return
	 */
	public static String buildCoAPLink(KuraNode localNode, KuraNode destination){
//		return new StringBuilder()
//					.append(CoAP.COAP_URI_SCHEME)
//					.append("://").append(destination.getKuraAddress())
//					.append(":").append(destination.getKuraPort())
//					.append(CoAPKey.RESOURCE_DIRECTORY_PATH)
//					.append("/?ep=").append(KuraNodeUtils.getEPNameForRD(localNode))
//					.toString();				
				
		return CoAP.COAP_URI_SCHEME
				+ "://" + destination.getKuraAddress()
				+ ":"
				+ destination.getKuraPort()
				+ "/" + CoAPKey.RESOURCE_DIRECTORY_PATH
				+ "/?ep=" + KuraNodeUtils.getEPNameForRD(localNode);
	}
	
	/**
	 * Utility for building a local CoAP Link.
	 * 
	 * @return
	 */
	public static String buildLocalCoAPLink(){
		return new String("coap://"
							+ "127.0.0.1"
							+ ":5683"
							+ "/" + CoAPKey.RESOURCE_DIRECTORY_PATH
							+ "/?ep=" + "localhost");
	}
	
	
	public void close(){
		executor.shutdownNow();
	}


	public CoapClient getClient() {
		return client;
	}
	public void setClient(CoapClient client) {
		this.client = client;
	}
	
	
}
