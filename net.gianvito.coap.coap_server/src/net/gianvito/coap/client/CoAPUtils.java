package net.gianvito.coap.client;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.mqtt.CoAPKey;
import net.gianvito.coap.server.utils.KuraNodeUtils;

import org.eclipse.californium.core.coap.CoAP;

public class CoAPUtils {
	/**
	 * Utility for building a correct CoAP Link.
	 * 
	 * @param destination
	 * @return
	 */
	public static String buildCoAPLink(KuraNode localNode, KuraNode destination){
//		return new StringBuilder()
//					.append(CoAP.COAP_URI_SCHEME)
//					.append("://").append(destination.getKuraAddress())
//					.append(":").append(destination.getKuraPort())
//					.append(CoAPKey.RESOURCE_DIRECTORY_PATH)
//					.append("/?ep=").append(KuraNodeUtils.getEPNameForRD(localNode))
//					.toString();				
				
		return CoAP.COAP_URI_SCHEME
				+ "://" + destination.getKuraAddress()
				+ ":"
				+ destination.getKuraPort()
				+ "/" + CoAPKey.RESOURCE_DIRECTORY_PATH
				+ "/?ep=" + KuraNodeUtils.getEPNameForRD(localNode);
	}
	
	public static String buildCoAPsLink(KuraNode localNode, KuraNode destination){
		return CoAP.COAP_SECURE_URI_SCHEME
				+ "://" + destination.getKuraAddress()
				+ ":"
				+ destination.getKuraPort()
				+ "/" + CoAPKey.RESOURCE_DIRECTORY_PATH
				+ "/?ep=" + KuraNodeUtils.getEPNameForRD(localNode);
	}
	
	public static String buildCoAPsPath(KuraNode localNode){
		return CoAPKey.RESOURCE_DIRECTORY_PATH
				+ "/?ep=" + KuraNodeUtils.getEPNameForRD(localNode);
	}
	
	/**
	 * Utility for building a local CoAP Link.
	 * 
	 * @return
	 */
	public static String buildLocalCoAPLink(){
		return new String("coap://"
							+ "127.0.0.1"
							+ ":5683"
							+ CoAPKey.RESOURCE_DIRECTORY_PATH
							+ "/?ep=" + "localhost");
	}
}
