package net.gianvito.coap.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.dtls.CoAPDTLSBase;
import net.gianvito.coap.dtls.CoAPDTLSEndpoint;
import net.gianvito.coap.dtls.CoAPDTLSOptions;
import net.gianvito.coap.message.KuraNode;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.scandium.DTLSConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoAPDTLSRemoteClient {
	
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(CoAPDTLSRemoteClient.class);
	

	private CoAPDTLSBase dtlsBase;
	private CoapClient client;
	
	
//	private Executor executor;
//	private final int DTLS_EXECUTOR_THREADS = 2;
	
	/* ----------------------- Methods ----------------------------- */
	
	
	/**
	 * Constructor.
	 */
	public CoAPDTLSRemoteClient(CoAPDTLSOptions dtlsOptions){		
		//executor = Executors.newFixedThreadPool(DTLS_EXECUTOR_THREADS);
			
		dtlsBase = new CoAPDTLSBase(dtlsOptions);
		dtlsBase.createDTLSConnector(0);
		
		createClient();
	}
	
	/**
	 * Constructor.
	 */
	public CoAPDTLSRemoteClient(CoAPDTLSOptions dtlsOptions, int connectorPort){		
		//executor = Executors.newFixedThreadPool(DTLS_EXECUTOR_THREADS);
			
		dtlsBase = new CoAPDTLSBase(dtlsOptions);
		dtlsBase.createDTLSConnector(connectorPort);
		
		createClient();
	}
	
	public void createClient(){
		client = new CoapClient();
		//client.setExecutor(executor);
		setNewEndpoint();
	}
	
	public void createClient(String address, int port, String path){
		client = new CoapClient(CoAP.COAP_SECURE_URI_SCHEME, address, port, path);
		//client.setExecutor(executor);
		setNewEndpoint();
	}
	
	public void setNewEndpoint(){
		client.setEndpoint(new CoAPDTLSEndpoint(dtlsBase.getDtlsConnector()).getSecureEndpoint());
	}

	public void stopEndpoint(){
		client.getEndpoint().stop();
	}
	
	/**
	 * Temporary method used to workaround missing housekeeping thread that 
	 * closes stale sessions after a certain time in DTLSConnector in Scandium.
	 * 
	 * @param node
	 */
	public void closeSession(KuraNode node){
		closeSession(new InetSocketAddress(node.getKuraAddress(), node.getKuraPort()));
	}
	
	public void closeSession(InetSocketAddress address){
		((DTLSConnector)dtlsBase.getDtlsConnector()).close(address);
	}
	
	public void restartEndpoint(){
		try {
			if(!client.getEndpoint().isStarted()){
				dtlsBase.getDtlsConnector().start();
				client.getEndpoint().start();
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public void setUri(String uri){
		client.setURI(uri);
	}
	
	/**
	 * Post resources to the remote Resource Directory
	 * 
	 * @param destination
	 * @param resourcesP
	 */
	public void postResources(KuraNode localNode, KuraNode destination, Set<ResourceProperty> resourcesP){
		String payload = "";
		
		client.setURI(CoAPUtils.buildCoAPsLink(localNode, destination));
		System.out.println("Client URI --> " + client.getURI());
		logger.debug("Client URI --> {}", client.getURI());
		
		// Payload creation.
		for(ResourceProperty resource : resourcesP){
			payload += LinkFormat.serializeResource(resource.getResource()).toString();
		}
		
//		client.post(new CoapHandler(){
//
//			@Override
//			public void onError() {
//				logger.info("Error in posting resources.");
//			}
//
//			@Override
//			public void onLoad(CoapResponse response) {
//				logger.info("Post done. {}", response.getCode().name().toString());
//			}
//				
//			}, payload, MediaTypeRegistry.APPLICATION_LINK_FORMAT);
		
		CoapResponse response = client.post(payload, MediaTypeRegistry.APPLICATION_LINK_FORMAT);
		logger.info("Response --> {} - {}", response.getCode().name().toString(), 
				response.getResponseText() == null? "null" : response.getResponseText());
	}

	
	public CoapClient getClient() {
		return client;
	}
	public void setClient(CoapClient client) {
		this.client = client;
	}
	
	
}
