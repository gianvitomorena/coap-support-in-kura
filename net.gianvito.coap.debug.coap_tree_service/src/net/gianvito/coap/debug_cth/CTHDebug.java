package net.gianvito.coap.debug_cth;

import java.io.IOException;
import java.util.Set;

import net.gianvito.coap.collection.cth.ParentTree;
import net.gianvito.coap.cth.CoAPTreeHandler;
import net.gianvito.coap.debug.listener.CTHDebugListener;
import net.gianvito.coap.debug.utils.DebugMessage;
import net.gianvito.coap.debug.utils.DebugMessageType;
import net.gianvito.coap.utils.ByteObjectUtils;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.KuraStoreException;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.DataServiceListener;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Allows to obtain information about CoAPTreeHandler through MQTT requests.
 * 
 * @author Gianvito Morena
 *
 */
public class CTHDebug implements DataServiceListener, CTHDebugListener{
	
	/* ----------------------- Attributes -------------------------- */
	private static final Logger logger = LoggerFactory.getLogger(CTHDebug.class);
	
	private DataService dataService;
	private CoAPTreeHandler cth;
		
	public static final String PARENT_TREE_REQUEST = "coap/debug/request/cth/parent";
	public static final String DEVICES_REQUEST = "coap/debug/request/cth/devices";
	public static final String PARENT_TREE_RESPONSE = "coap/debug/response/cth/parent";
	public static final String DEVICES_RESPONSE = "coap/debug/response/cth/devices";
	
	Set<String> topics;
	
	/* ----------------------- Methods ----------------------------- */
	
	/**
	 * Constructor.
	 */
	public CTHDebug(){
		
	}
	
	/* 
	 * Activate Method
	 */
	protected void activate(ComponentContext componentContext){		
		if(dataService.isConnected()){
         	try {
				dataService.subscribe(PARENT_TREE_REQUEST, 1);
				dataService.subscribe(DEVICES_REQUEST, 1);
			} catch (KuraException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         }
		logger.info("CTHDebug activated.");
	}
	
	/* 
	 * Deactivate Method
	 */
	protected void deactivate(ComponentContext componentContext){
		try {
			dataService.unsubscribe(PARENT_TREE_REQUEST);
			dataService.unsubscribe(DEVICES_REQUEST);
		} catch (KuraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("CTHDebug deactivated.");
	}
	
	public void onTreeRequest(DebugMessage message){
		// TODO Auto-generated method stub
		ParentTree treeMap = cth.getCthCollection().getParents();
			
		DebugMessage cthMessage = new DebugMessage(DebugMessageType.PARENT_TREE, ByteObjectUtils.serialize(treeMap));
		
		try {
			dataService.publish(PARENT_TREE_RESPONSE, ByteObjectUtils.toByteArray(cthMessage), 1, false, 0);
		} catch (KuraStoreException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onDevicesRequest(DebugMessage message){
		// TODO Auto-generated method stub
		DebugMessage cthMessage = new DebugMessage(DebugMessageType.DEVICES,
				cth.getCthCollection().getDevices());
		try {
			dataService.publish(DEVICES_RESPONSE, ByteObjectUtils.toByteArray(cthMessage), 1, false, 0);
		} catch (KuraStoreException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onConnectionEstablished() {
		try {
			dataService.subscribe(PARENT_TREE_REQUEST, 1);
			dataService.subscribe(DEVICES_REQUEST, 1);
		} catch (KuraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onDisconnecting() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMessageArrived(String topic, byte[] payload, int qos,
			boolean retained) {
		DebugMessage message;
		if(isTopicRight(topic)){
			try {
				message = (DebugMessage)ByteObjectUtils.getObject(payload, DebugMessage.class);
				// Select the operation to execute
				DebugMessageType type = message.getType();
				switch(type){
					case PARENT_TREE:
						onTreeRequest(message);
						break;
					case DEVICES:
						onDevicesRequest(message);
						break;
					default:
						break;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public boolean isTopicRight(String topic){
		if(topic.equals(PARENT_TREE_REQUEST) || topic.equals(DEVICES_REQUEST)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public void onMessagePublished(int messageId, String topic) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMessageConfirmed(int messageId, String topic) {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * ------------------------------------------
	 * --- Setting services ---
	 * ------------------------------------------
	 */
	protected void setDataService(DataService dataService){
		this.dataService = dataService;
	}
	protected void unsetDataService(DataService dataService){
		this.dataService = null;
	}
	protected void setCoAPTreeHandler(CoAPTreeHandler cth){
		this.cth = cth;
	}
	protected void unsetCoAPTreeHandler(CoAPTreeHandler cth){
		this.cth = null;
	}
}
