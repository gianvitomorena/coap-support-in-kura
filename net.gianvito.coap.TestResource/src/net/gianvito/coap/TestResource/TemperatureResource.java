package net.gianvito.coap.TestResource;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.californium.core.server.resources.CoapExchange;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.gianvito.coap.server.CoAPServer;
import net.gianvito.coap.server.resource.ExtendedResource;

/**
 * Example of temperature sensor with ExtendedResource.
 * 
 * @author Gianvito Morena
 *
 */
public class TemperatureResource extends ExtendedResource{
	private static final long serialVersionUID = -5793102779956766462L;

	private static final Logger logger = LoggerFactory.getLogger(TemperatureResource.class);
	private static String resourceName = "temperature";
	private String resourceType = "temp";
	private transient CoAPServer cs;
	
	// Observe test
	private String value = "ciao";
	private transient Timer timer = new Timer();
	
	public TemperatureResource(){
		super(resourceName);
		this.setResourceType(resourceType);
		this.addAttribute("position", "garage");
		this.addAttribute("brand", "SPI");
		this.addAttribute("model", "12001");
	}
	
	protected void activate(ComponentContext componentContext){
		logger.info("Adding resource --> " + resourceName);
		// If we want to add domain and group attributes
		this.updateDomainAndGroup(cs);
		cs.addResource(this, ExtendedResource.class);
		logger.info("Resource added.");
		timer.scheduleAtFixedRate(new ValueTimer(), 5000, 5000);
	}
		
	protected void deactivate(ComponentContext componentContext){
		logger.info("Resoure removed --> " + resourceName);
		cs.removeResource(this, ExtendedResource.class);
	}
	
	@Override
	public void handleGET(CoapExchange exchange){
		//exchange.respond(LinkFormat.serializeResource(this).toString());
		exchange.respond(value);
	}
	
	protected void setCoAPServer(CoAPServer cs){
		this.cs = cs;
	}
	protected void unsetCoAPServer(CoAPServer cs){
		this.cs = null;
	}
	
	class ValueTimer extends TimerTask{
		
		public ValueTimer(){
			super();
		}
		
		@Override
		public void run() {
			if(value.equals("ciao")){
				value = "mondo";
				changed();
			}
			else{
				value = "ciao";
				changed();
			}	
		}
		
	}
}
