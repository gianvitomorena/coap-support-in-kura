package net.gianvito.coap.TestResource;

import net.gianvito.coap.server.CoAPServer;
import net.gianvito.coap.server.resource.ExtendedResource;

import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HumidityResource extends ExtendedResource{
	private static final long serialVersionUID = -8375390543248868388L;

	private static final Logger logger = LoggerFactory.getLogger(TemperatureResource.class);
	private static String resourceName = "humidity";
	private String resourceType = "temp";
	private transient CoAPServer cs;
	
	public HumidityResource(){
		super(resourceName);
		this.setResourceType(resourceType);
		this.addAttribute("position", "garage");
		this.addAttribute("brand", "SPI");
		this.addAttribute("model", "10000");
	}
	
	protected void activate(ComponentContext componentContext){
		logger.info("Adding resource --> " + resourceName);
		// If we want to add domain and group attributes
		this.updateDomainAndGroup(cs);
		cs.addResource(this, ExtendedResource.class);
		logger.info("Resource added.");
	}
		
	protected void deactivate(ComponentContext componentContext){
		logger.info("Resource removed --> " + resourceName);
		cs.removeResource(this, ExtendedResource.class);
	}
	
	@Override
	public void handleGET(CoapExchange exchange){
		exchange.respond(LinkFormat.serializeResource(this).toString());
	}
	
	protected void setCoAPServer(CoAPServer cs){
		this.cs = cs;
	}
	protected void unsetCoAPServer(CoAPServer cs){
		this.cs = null;
	}
}
