package net.gianvito.coap.dtls.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.util.Set;
import java.util.logging.Level;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.network.CoAPEndpoint;
import org.eclipse.californium.core.network.Endpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.ResourceAttributes;
import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.ScandiumLogger;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.eclipse.paho.client.mqttv3.logging.Logger;

public class DTLSClient {
	
	static {
		ScandiumLogger.initialize();
		ScandiumLogger.setLevel(Level.WARNING);
	}
	
	private static final int DEFAULT_PORT = 5684;
	
	private static final String TRUST_STORE_PASSWORD = "rootPass";
	private final static String KEY_STORE_PASSWORD = "endPass";
	private String keyStoreLocation = "/home/gianvito/certs/keyStore.jks";
	private String trustStoreLocation = "/home/gianvito/certs/trustStore.jks";
	
	private KeyStore keyStore, trustStore;
	private Certificate[] trustedCertificates;
	private DTLSConnector dtlsConnector;
	private Endpoint secureEndpoint;
	
	private CoapClient client;
	
	
	public DTLSClient(String kSLocation, String tSLocation){
		
		keyStoreLocation = kSLocation != null? kSLocation : keyStoreLocation;
		trustStoreLocation = tSLocation != null? tSLocation : trustStoreLocation;
		
		try {
	        // load key store
            keyStore = KeyStore.getInstance("JKS");
            InputStream in = new FileInputStream(keyStoreLocation);
            keyStore.load(in, KEY_STORE_PASSWORD.toCharArray());
    
            // load trust store
            trustStore = KeyStore.getInstance("JKS");
            InputStream inTrust = new FileInputStream(trustStoreLocation);
            trustStore.load(inTrust, TRUST_STORE_PASSWORD.toCharArray());
            
            // You can load multiple certificates if needed
            trustedCertificates = new Certificate[1];
            trustedCertificates[0] = trustStore.getCertificate("root");
    
	    } catch (GeneralSecurityException | IOException e) {
            System.err.println("Could not load the keystore");
            e.printStackTrace();
        }
	}
	
	public void createDTLSConnector(String address, int port, String path){
		try {
			// TODO Check if previous connector has been closed.
			dtlsConnector = new DTLSConnector(new InetSocketAddress(0), trustedCertificates);
			dtlsConnector.getConfig().setPskStore(new StaticPskStore("Client_identity", "secretPSK".getBytes()));
			dtlsConnector.getConfig().setPrivateKey((PrivateKey)keyStore.getKey("client", KEY_STORE_PASSWORD.toCharArray()), keyStore.getCertificateChain("client"), true);
			client = new CoapClient(CoAP.COAP_SECURE_URI_SCHEME, address, port, path);
			secureEndpoint = new CoAPEndpoint(dtlsConnector, NetworkConfig.getStandard());
			EndpointManager.getEndpointManager().setDefaultSecureEndpoint(secureEndpoint);
			secureEndpoint.start();
			client.setEndpoint(secureEndpoint);
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException | IOException e) {
			System.out.println("Problema con DTLS");
		}
	}

	public void setUri(String URI){
		client.setURI(URI);
	}
	
	public void stop(){
		secureEndpoint.stop();
	}
	
	public void printLocalResources(){		
		Set<WebLink> resources = client.discover();
		
		for(WebLink link : resources){
			System.out.println("Resource --> " + link.getURI());
			ResourceAttributes attributes = link.getAttributes();
			for(String attribute : attributes.getAttributeKeySet()){
				System.out.println("Attribute --> " + attribute + "=" + attributes.getAttributeValues(attribute).toString());
			}
		}
	}
	
	public void printRDResources(String address, int port){
		client.setURI("coaps://" + address + ":" + port + "/rd-lookup/res?ep=19216810000000000000");
		CoapResponse response = client.get();
		System.out.println("RD resources --> " + response.getCode() + " " + response.getResponseText());
	}
	
	public void testPost(String address, int port){
		client.setURI("coaps://" + address + ":" + port + "/rd/?ep=123123123123");
		CoapResponse response = client.post("", MediaTypeRegistry.APPLICATION_LINK_FORMAT);
		System.out.println("Risposta del test POST --> " + response.getCode().name() + " " + response.getResponseText());
	}
	
	
	
	public CoapClient getClient() {
		return client;
	}
}
