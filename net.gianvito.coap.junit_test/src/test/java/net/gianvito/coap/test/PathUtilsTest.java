package net.gianvito.coap.test;


import static org.junit.Assert.*;
import net.gianvito.coap.utils.PathUtils;

import org.junit.Test;


public class PathUtilsTest {
	
	@Test
	public void testBuildRelativePath(){
		String domainAttribute = "gianvito.net";
		//String domainAllAttribute = "all";
		String groupAttribute = "*";
		
		String rP1 = PathUtils.buildRelativeNodePath(domainAttribute, groupAttribute);
		assertEquals("/gianvito.net", rP1);
		
		String rP2 = PathUtils.buildRelativeNodePath(domainAttribute, null);
		assertEquals("/gianvito.net", rP2);
	}
	
	@Test
	public void testMoreGeneral(){
		
	}

}
