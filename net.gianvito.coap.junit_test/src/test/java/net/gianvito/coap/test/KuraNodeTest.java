package net.gianvito.coap.test;

import static org.junit.Assert.*;

import org.junit.Test;

import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.server.debug.NodePrinter;
import net.gianvito.coap.server.utils.KuraNodeUtils;
import net.gianvito.coap.utils.PathUtils;

public class KuraNodeTest {
	
	@Test
	public void sameObject(){
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 123, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "eth0", "00:00:00:00:00:01", 123, null, "*", "*");
		KuraNode kN3 = new KuraNode("192.168.1.1", "lo0", "00:00:00:00:00:00", 123, kN1, "*", "*");
		
		NodePrinter.printNode(kN1, 4);
		
		// Same object test
		assertTrue(kN1 != kN2);
		assertTrue(kN1.equals(kN3));
		
		// Parent node test
		KuraNode parentOfkN3 = kN3.getParentNode();
		kN3.setParentNode(kN2);
		assertEquals(kN1, parentOfkN3);
	}
	
	@Test
	public void testPropagationOnParent(){
		KuraNode kN0 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 123, null, "*", "*");
		KuraNode kN1 = new KuraNode("192.168.1.1", "eth0", "00:00:00:00:00:00", 123, kN0, "gianvito.net", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "wlan0", "00:00:00:00:00:01", 123, kN1, "gianvito.net", "torino");
		KuraNode kN3 = new KuraNode("192.168.1.3", "wlan0", "00:00:00:00:00:02", 123, kN1, "gianvito.net", "bologna");
		KuraNode kN4 = new KuraNode("192.168.1.4", "eth0", "00:00:00:00:00:03", 123, kN2, "gianvito.net", "torino/12");
		KuraNode kN5 = new KuraNode("192.168.1.52", "wlan0", "00:00:00:00:00:04", 123, kN3, "gianvito.net", "bologna/10");
		KuraNode kN6 = new KuraNode("192.168.1.6", "lo0", "00:00:00:00:00:05", 123, kN1, "gianvito.com", "*");
		KuraNode kN7 = new KuraNode("192.168.1.62", "wlan0", "00:00:00:00:00:52", 123, kN6, "gianvito.com", "bologna");
		
		
		String domainDotNetAttribute = "gianvito.net";
		String domainAllAttribute = "all";
		String groupAttribute = "*";
		String groupBologna = "bologna";
		
		// Between kN2 and kN1
		Boolean prop1 = KuraNodeUtils.checkPropagationOnParent(kN1, kN2, domainDotNetAttribute, groupAttribute);
		assertEquals(false, prop1);
		
		// Between kN2 and kN3
		Boolean prop2 = KuraNodeUtils.checkPropagationOnParent(kN3, kN2, domainDotNetAttribute, groupAttribute);
		assertEquals(false, prop2);
		
		// Between kN5 and kN4
		Boolean prop3 = KuraNodeUtils.checkPropagationOnParent(kN4, kN5, domainDotNetAttribute, groupAttribute);
		assertEquals(false, prop3);
		
		// Between kN5 and kN7 with domainAllAttribute and different domain
		Boolean prop41 = KuraNodeUtils.checkPropagationOnParent(kN7, kN5, domainAllAttribute, groupAttribute);
		assertEquals(false, prop41);
		
		// Between kN5 and kN4 with domainAllAttribute and same domain
		Boolean prop42 = KuraNodeUtils.checkPropagationOnParent(kN4, kN5, domainAllAttribute, groupAttribute);
		assertEquals(false, prop42);
						
		// Between kN5 and kN3 with domainAllAttribute and kN3 path more general than kN5
		Boolean prop52 = KuraNodeUtils.checkPropagationOnParent(kN3, kN5, domainAllAttribute, groupAttribute);
		assertEquals(true, prop52);
		
		
		// Between kN3 and kN2. Different group.
		Boolean prop7 = KuraNodeUtils.checkPropagationOnParent(kN2, kN3, domainDotNetAttribute, groupBologna);
		assertEquals(false, prop7);
		
		// Between kN2 and kN7. Different domain and group.
		Boolean prop72 = KuraNodeUtils.checkPropagationOnParent(kN7, kN2, "gianvito.com", "*");
		assertEquals(false, prop72);
		
		// Between kN2 and kN1. Same domain but with different domain search.
		Boolean prop82 = KuraNodeUtils.checkPropagationOnParent(kN1, kN2, "gianvito.com", "*");
		assertEquals(true, prop82);
		
		// Between kN2 and kN1. Same domain and group as the sender.
		Boolean prop9 = KuraNodeUtils.checkPropagationOnParent(kN1, kN2, "gianvito.net", "torino");
		assertEquals(false, prop9);
	}
	
	@Test
	public void testValidityForAdding(){
		KuraNode kN0 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 123, null, "*", "*");
		KuraNode kN1 = new KuraNode("192.168.1.1", "eth0", "00:00:00:00:00:00", 123, kN0, "gianvito.net", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "lo0", "00:00:00:00:00:01", 123, kN1, "gianvito.net", "torino");
		KuraNode kN3 = new KuraNode("192.168.1.3", "wlan0", "00:00:00:00:00:02", 123, kN1, "gianvito.net", "bologna");
//		KuraNode kN4 = new KuraNode("192.168.1.4", "wlan0", "00:00:00:00:00:03", 123, kN2, "gianvito.net", "torino/12");
//		KuraNode kN5 = new KuraNode("192.168.1.52", "wlan0", "00:00:00:00:00:04", 123, kN3, "gianvito.net", "bologna/10");
		KuraNode kN6 = new KuraNode("192.168.1.6", "eth0", "00:00:00:00:00:05", 123, kN1, "gianvito.com", "*");
//		KuraNode kN7 = new KuraNode("192.168.1.62", "wlan0", "00:00:00:00:00:52", 123, kN6, "gianvito.com", "bologna");
//		
//		
//		String domainAttribute = "gianvito.net";
//		String domainAllAttribute = "all";
//		String groupAttribute = "*";
		
		
		// Between kN2 and kN2
		Boolean prop1 = PathUtils.checkNodeValidityForAdding(kN2, kN2);
		assertEquals(false, prop1);
		
		// Between kN2 and kN3
		Boolean prop2 = PathUtils.checkNodeValidityForAdding(kN3, kN2);
		assertEquals(true, prop2);
		
		// Between kN2 and kN1
		Boolean prop3 = PathUtils.checkNodeValidityForAdding(kN1, kN2);
		assertEquals(false, prop3);
		
		// Between kN2 and kN0
		Boolean prop4 = PathUtils.checkNodeValidityForAdding(kN6, kN2);
		assertEquals(true, prop4);
		
	}
	
	
}
