package net.gianvito.coap.test;

import static org.junit.Assert.assertEquals;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.collection.server.LinkedNodeCollection;
import net.gianvito.coap.message.KuraNode;

import org.junit.Test;

public class LinkedNodeCollectionTest {
	
	@Test
	public void versionCheck(){
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 5683, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "eth0", "00:00:00:00:00:01", 5683, null, "gianvito.net", "bologna");
		KuraNode kN3 = new KuraNode("192.168.1.1", "lo0", "00:00:00:00:00:00", 5683, null, "gianvito.net", "bologna/gobetti");
		
//		LinkAttribute lA1 = new LinkAttribute("position", "bologna");
//		LinkAttribute lA2 = new LinkAttribute("position", "bologna");
//		LinkAttribute lA3 = new LinkAttribute("position", "bologna2");
		
		
		AttributeCollection aC1 = new AttributeCollection();
		AttributeCollection aC2 = new AttributeCollection();
		AttributeCollection aC3 = new AttributeCollection();
		
		LinkedNodeCollection lNC = new LinkedNodeCollection();
		
		lNC.addNode(kN1, aC1);
		lNC.addNode(kN2, aC2);
		lNC.addNode(kN3, aC3);
		
		System.out.println("aC1 version --> " + aC1.getVersion());
		System.out.println("aC2 version --> " + aC2.getVersion());
		System.out.println("aC3 version --> " + aC3.getVersion());
		System.out.println("lNC kN1 version --> " + lNC.getCollection(kN1).getVersion());
		System.out.println("lNC kN2 version --> " + lNC.getCollection(kN2).getVersion());
		System.out.println("lNC kN3 version --> " + lNC.getCollection(kN3).getVersion());
		
		assert(aC1.getVersion() < aC2.getVersion());
		assertEquals(false, lNC.checkIfArrivedIsNewer(kN2, aC1));
		assertEquals(true, lNC.checkIfArrivedIsNewer(kN1, aC2));
		assertEquals(false, lNC.checkIfArrivedIsNewer(kN1, aC1));
		assertEquals(true, lNC.checkIfArrivedIsNewer(kN2, aC3));
		
	}
}
