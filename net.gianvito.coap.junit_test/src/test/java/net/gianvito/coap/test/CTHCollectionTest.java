package net.gianvito.coap.test;

import static org.junit.Assert.*;

import java.util.Set;

import net.gianvito.coap.collection.cth.CTHCollection;
import net.gianvito.coap.collection.cth.RemoveResult;
import net.gianvito.coap.cth.debug.CTHPrinter;
import net.gianvito.coap.message.KuraNode;

import org.junit.Test;

public class CTHCollectionTest {
	
	
	@Test
	public void removeRoot(){
		CTHCollection col = new CTHCollection();
		
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 5683, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "wlan0", "00:00:00:00:00:01", 5683, null, "gianvito.net", "bologna");
		//KuraNode kN3 = new KuraNode("192.168.1.1", "eth0", "00:00:00:00:00:00", 5683, null, "gianvito.net", "bologna/gobetti");
						
		KuraNode insertedRoot = col.addRoot(kN1);
		assertEquals(null, insertedRoot); // Null because there wasn't a root
		// Update root on root
		kN1.setParentNode(kN1);
		// Send update to CTH
		col.updateRootOnNode(kN1, null);
		
		KuraNode kN2p = col.addNodeRequest(kN2);
		// Check returned root
		assertTrue(kN2p.equals(kN1));
		// Update root on node
		kN2.setParentNode(kN1);
		// Update collection about root update on node
		col.updateRootOnNode(kN2, null);
		
		RemoveResult result = col.removeRoot(kN1);
		
		Boolean removeResult = result.getOperationResult();
		assertTrue(removeResult);
		KuraNode removedRoot = result.getRemovedNode();
		assertEquals(kN1, removedRoot);
		Set<KuraNode> rootChildren = result.getNodeChildren();
		assertEquals(1, rootChildren.size());
	}
	
	
	@Test
	public void removeNode(){
		CTHCollection col = new CTHCollection();

		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 5683, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "wlan0", "00:00:00:00:00:01", 5683, null, "gianvito.net", "bologna");
		KuraNode kN3 = new KuraNode("192.168.1.1", "eth0", "00:00:00:00:00:00", 5683, null, "gianvito.net", "milano");
		KuraNode kN4 = new KuraNode("192.168.1.3", "eth0", "00:00:00:00:00:02", 5683, null, "gianvito.net", "*");
						
		KuraNode insertedRoot = col.addRoot(kN1);
		assertEquals(null, insertedRoot);
		// Update root on root
		kN1.setParentNode(kN1);
		// Send update to CTH
		col.updateRootOnNode(kN1, null);
		assertEquals(kN1, col.getDevices().get(kN1.getRelativePath()).getParentNode());
		
		// Add a level 2 node.
		KuraNode kN2p = col.addNodeRequest(kN2);
		// Check returned root
		assertTrue(kN2p.equals(kN1));
		// Update root on node
		kN2.setParentNode(kN1);
		// Update collection about root update on node
		col.updateParentOnNode(kN2, null);
		assertEquals(kN2p, col.getDevices().get(kN2.getRelativePath()).getParentNode());
		
		// Add a level 1 node
		KuraNode kN4p = col.addNodeRequest(kN4);
		// Check returned root
		assertTrue(kN4p.equals(kN1));
		// Update root on node
		kN4.setParentNode(kN1);
		// Update collection about root update on node
		col.updateRootOnNode(kN4, null);
		assertEquals(kN4p, col.getDevices().get(kN4.getRelativePath()).getParentNode());
		
		// Update kN2 parent
		kN2.setParentNode(kN4);
		// Update collection about parent update on node
		col.updateParentOnNode(kN2, kN1);
		assertEquals(kN4, col.getDevices().get(kN2.getRelativePath()).getParentNode());
		
		
		// Remove kN2
		RemoveResult result = col.removeNode(kN2);
		Boolean removeResult = result.getOperationResult();
		assertTrue(removeResult);
		KuraNode removedNode = result.getRemovedNode();
		assertEquals(kN2, removedNode);
		Set<KuraNode> rootChildren = result.getNodeChildren();
		assertEquals(0, rootChildren.size());
		
		// Add kN3
		KuraNode kN3p = col.addNodeRequest(kN3);
		// Check returned root
		assertTrue(kN3p.equals(kN4));
		// Update root on node
		kN3.setParentNode(kN3p);
		// Update collection about root update on node
		col.updateRootOnNode(kN3, null);
		assertEquals(kN3p, col.getDevices().get(kN3.getRelativePath()).getParentNode());
		
		// Remove a node with a child.
		RemoveResult result4 = col.removeNode(kN4);
		Boolean removeResult4 = result4.getOperationResult();
		assertTrue(removeResult4);
		KuraNode removedNode4 = result4.getRemovedNode();
		assertEquals(kN4, removedNode4);
		Set<KuraNode> rootChildren4 = result4.getNodeChildren();
		assertEquals(1, rootChildren4.size());
		
		/**
		System.out.println("operationResult --> " + removeResult);
		System.out.println("removedRoot --> " + removedNode.toString());
		System.out.println("nodeChildren --> " + rootChildren.toString());
		
		CTHPrinter.printDevices(col.getDevices());
		CTHPrinter.printTree(col.getParents());
		**/
	}
	
	@Test
	public void multipleNodes(){
		CTHCollection col = new CTHCollection();
		
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 5683, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "wlan0", "00:00:00:00:00:01", 5683, null, "gianvito.com", "bologna");
		KuraNode kN3 = new KuraNode("192.168.1.3", "eth0", "00:00:00:00:00:02", 5683, null, "gianvito.net", "bologna/gobetti");
		KuraNode kN4 = new KuraNode("192.168.1.4", "wlan0", "00:00:00:00:00:03", 5683, null, "gianvito.net", "bologna");
		KuraNode kN5 = new KuraNode("192.168.1.5", "wlan0", "00:00:00:00:00:04", 5683, null, "gianvito.net", "bologna/ranzani");
		KuraNode kN6 = new KuraNode("192.168.1.6", "wlan0", "00:00:00:00:00:05", 5683, null, "gianvito.net", "milano/gobetti");
		KuraNode kN7 = new KuraNode("192.168.1.7", "wlan0", "00:00:00:00:00:06", 5683, null, "*", "*");
		KuraNode kN8 = new KuraNode("192.168.1.8", "wlan0", "00:00:00:00:00:07", 5683, null, "gianvito.net", "bologna");
		KuraNode kN9 = new KuraNode("192.168.1.9", "wlan0", "00:00:00:00:00:08", 5683, null, "gianvito.net", "bologna/gobetti");
		KuraNode kN10 = new KuraNode("192.168.1.10", "wlan0", "00:00:00:00:00:09", 5683, null, "gianvito.net", "*");
		
		// Add root
		KuraNode insertedRoot = col.addRoot(kN1);
		assertEquals(null, insertedRoot);
		// Update root on root
		kN1.setParentNode(kN1);
		// Send update to CTH
		col.updateRootOnNode(kN1, null);
		assertEquals(kN1, col.getDevices().get(kN1.getRelativePath()).getParentNode());
		
		// Add other node
		KuraNode kN2p = col.addNodeRequest(kN2);
		// Check returned root
		assertTrue(kN2p.equals(kN1));
		// Update root on node
		kN2.setParentNode(kN2p);
		// Update collection about root update on node
		col.updateRootOnNode(kN2, null);
		assertEquals(kN2p, col.getDevices().get(kN2.getRelativePath()).getParentNode());
		
		// Add other node
		KuraNode kN3p = col.addNodeRequest(kN3);
		// Check returned root
		assertTrue(kN3p.equals(kN1));
		// Update root on node
		kN3.setParentNode(kN3p);
		// Update collection about root update on node
		col.updateRootOnNode(kN3, null);
		assertEquals(kN3p, col.getDevices().get(kN3.getRelativePath()).getParentNode());
	
		// Add other node
		KuraNode kN4p = col.addNodeRequest(kN4);
		// Check returned root
		assertTrue(kN4p.equals(kN1));
		// Update root on node
		kN4.setParentNode(kN4p);
		// Update collection about root update on node
		col.updateRootOnNode(kN4, null);
		assertEquals(kN4p, col.getDevices().get(kN4.getRelativePath()).getParentNode());

		// Add other node
		KuraNode kN5p = col.addNodeRequest(kN5);
		// Check returned root
		assertTrue(kN5p.equals(kN4));
		// Update root on node
		kN5.setParentNode(kN5p);
		// Update collection about root update on node
		col.updateRootOnNode(kN5, null);
		assertEquals(kN5p, col.getDevices().get(kN5.getRelativePath()).getParentNode());

		// Add another node
		KuraNode kN6p = col.addNodeRequest(kN6);
		// Check returned root
		assertTrue(kN6p.equals(kN1));
		// Update root on node
		kN6.setParentNode(kN6p);
		// Update collection about root update on node
		col.updateRootOnNode(kN6, null);
		assertEquals(kN6p, col.getDevices().get(kN6.getRelativePath()).getParentNode());
		
		// Add another root. Duplicate.
		KuraNode previousNode = col.addRoot(kN7);
		// Check returned root
		assertEquals(kN1, previousNode);
		// Update root on node
		kN7.setParentNode(previousNode);
		// Update collection about root update on node
		col.updateRootOnNode(kN7, null);
		// Check that hierarchy has not been modified
		assertEquals(previousNode, col.getDevices().get(kN7.getRelativePath()).getParentNode());
		
		// Add another node. Duplicate.
		KuraNode kN8p = col.addNodeRequest(kN8);		
		// Check returned root
		assertEquals(kN4, kN8p);
		// Update root on node
		kN8.setParentNode(kN8p);
		// Update collection about root update on node
		col.updateParentOnNode(kN8, null);
		
		CTHPrinter.printDevices(col.getDevices());
		CTHPrinter.printTree(col.getParents());
		
		// Check that hierarchy has not been modified
		assertEquals(kN4, col.getDevices().get(kN8.getRelativePath()));
		
		
		// Add another node. Duplicate with parent in level > 1.
		KuraNode kN9p = col.addNodeRequest(kN9);
		// Check returned root
		assertEquals(kN3, kN9p);
		// Update root on node
		kN9.setParentNode(kN9p);
		// Update collection about root update on node
		col.updateParentOnNode(kN9, null);
		// Check that hierarchy has not been modified
		assertEquals(kN3, col.getDevices().get(kN9.getRelativePath()));
		
		
		// Successive kN3 parent request
		kN3p = col.searchNewParentNode(kN3);
		assertEquals(kN4, kN3p);
		kN3.setParentNode(kN3p);
		col.updateParentOnNode(kN3, kN1);
		assertEquals(kN3, col.getDevices().get(kN3.getRelativePath()));
		assertEquals(kN3p, col.getDevices().get(kN3.getRelativePath()).getParentNode());
		
		// Check if a duplicate is removed correctly from tree by LWT.
		// kN6
		KuraNode rootDuplicate = col.findDeviceByMAC("/root/00:00:00:00:00:06");
		assertEquals(kN7, rootDuplicate);
		RemoveResult removeResults = col.removeNode(rootDuplicate);
		assertEquals(kN7, removeResults.getRemovedNode());
		Set<KuraNode> nodeChildrenRootDuplicate = removeResults.getNodeChildren();
		assertEquals(0, nodeChildrenRootDuplicate.size());
		
		//CTHPrinter.printDevices(col.getDevices());
		//CTHPrinter.printTree(col.getParents());
		
		// Check if a duplicate with level greater than 1 is removed correctly
		// kN9
		KuraNode nodeDuplicate = col.findDeviceByMAC("/gianvito.net/bologna/gobetti/00:00:00:00:00:08");
		assertEquals(kN9, nodeDuplicate);
		RemoveResult duplicateResults = col.removeNode(nodeDuplicate);
		assertEquals(kN9, duplicateResults.getRemovedNode());
		Set<KuraNode> nodeChildrenDuplicate = duplicateResults.getNodeChildren();
		assertEquals(0, nodeChildrenDuplicate.size());
		
		// Remove last duplicate
		// kN8
		KuraNode node2Duplicate = col.findDeviceByMAC("/gianvito.net/bologna/00:00:00:00:00:07");
		assertEquals(kN8, node2Duplicate);
		RemoveResult duplicate2Results = col.removeNode(node2Duplicate);
		assertEquals(kN8, duplicate2Results.getRemovedNode());
		Set<KuraNode> node2ChildrenDuplicate = duplicate2Results.getNodeChildren();
		assertEquals(0, node2ChildrenDuplicate.size());
		
		// Add a first level node
		KuraNode kN10p = col.addNodeRequest(kN10);
		// Check returned root
		assertTrue(kN10p.equals(kN1));
		// Update root on node
		kN10.setParentNode(kN10p);
		// Update collection about root update on node
		col.updateRootOnNode(kN10, null);
		assertEquals(kN1, col.getDevices().get(kN10.getRelativePath()).getParentNode());
		
		KuraNode foundParentOfkN4 = col.searchNewParentNode(kN4);
		assertEquals(kN10, foundParentOfkN4);
		
		//System.out.println("Current root --> " + col.getDevices().get("/root").getCompletePath());
		//CTHPrinter.printDevices(col.getDevices());
		//CTHPrinter.printTree(col.getParents());
		
	}
	
	@Test
	public void testNullInput(){
		CTHCollection col = new CTHCollection();
		
		RemoveResult result = col.removeRoot(null);
		assertFalse(result.getOperationResult());
		
		KuraNode nodeResult = col.addNodeRequest(null);
		assertEquals(null, nodeResult);
		
		RemoveResult result2 = col.removeNode(null);
		assertFalse(result2.getOperationResult());
		
		Boolean result3 = col.updateRootOnNode(null, null);
		assertFalse(result3);
		
		Boolean result4 = col.updateParentOnNode(null, null);
		assertFalse(result4);
	}
	
}
