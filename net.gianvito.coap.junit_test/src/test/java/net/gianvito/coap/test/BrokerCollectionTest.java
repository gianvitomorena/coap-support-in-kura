package net.gianvito.coap.test;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import net.gianvito.coap.collection.property.NodeProperty;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.collection.server.BrokerCollection;
import net.gianvito.coap.collection.server.PropertyCollection;
import net.gianvito.coap.collection.server.RequestResult;
import net.gianvito.coap.message.CoAPMessage;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;
import net.gianvito.coap.server.debug.BrokerPrinter;
import net.gianvito.coap.server.debug.ResourcesPrinter;
import net.gianvito.coap.server.resource.ExtendedResource;
import net.gianvito.coap.type.CoAPOperation;

import org.junit.Test;

public class BrokerCollectionTest {

	@Test
	public void creation(){
		BrokerCollection bc = new BrokerCollection();
		assertEquals(bc.getAttributeCollection().size(), 0);
		assertEquals(bc.getLinkedNodes().getLinkedNodes().size(), 0);
	}
	
	@Test
	public void resourceUpdate(){
		BrokerCollection bc = new BrokerCollection();
		
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 123, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "wlan0", "00:00:00:00:00:01", 123, null, "*", "*");
		KuraNode kN3 = new KuraNode("192.168.1.3", "eth0", "00:00:00:00:00:02", 123, null, "*", "*");
		KuraNode kN4 = new KuraNode("192.168.1.4", "lo0", "00:00:00:00:00:03", 123, null, "*", "*");
		//KuraNode kN5 = new KuraNode("192.168.1.5", "00:00:00:00:00:04", 123, null, "*", "*");
		//KuraNode kN6 = new KuraNode("192.168.1.6", "00:00:00:00:00:05", 123, null, "*", "*");
		//KuraNode kN7 = new KuraNode("192.168.1.7", "00:00:00:00:00:06", 123, null, "*", "*");
		
		LinkAttribute lA1 = new LinkAttribute("position", "bologna");
		LinkAttribute lA2 = new LinkAttribute("position", "bologna");
		LinkAttribute lA3 = new LinkAttribute("position", "bologna2");
		LinkAttribute lA4 = new LinkAttribute("location", "bologna3");
		LinkAttribute lA5 = new LinkAttribute("location", "bologna4");
		LinkAttribute lA6 = new LinkAttribute("location", "bologna5");
		LinkAttribute lA7 = new LinkAttribute("location", "bologna6");
		
		AttributeCollection kN1Att = new AttributeCollection();
		AttributeCollection kN2Att = new AttributeCollection();
		AttributeCollection kN3Att = new AttributeCollection();
		AttributeCollection kN4Att = new AttributeCollection();
		AttributeCollection kN1AttOther = new AttributeCollection();
		//AttributeCollection kN6Att = new AttributeCollection();
		//AttributeCollection kN7Att = new AttributeCollection();
		
		kN1Att.add(lA1); kN1Att.add(lA2);
		kN1AttOther.add(lA7);
		kN2Att.add(lA2);
		kN3Att.add(lA1); kN3Att.add(lA2); kN3Att.add(lA3); kN3Att.add(lA4);
		kN4Att.add(lA1); kN4Att.add(lA2); kN4Att.add(lA3); kN4Att.add(lA4); kN4Att.add(lA5); kN4Att.add(lA6); kN4Att.add(lA7);
		
		AttributeCollection modifiedAttributes = bc.handleResourceUpdate(kN1, kN1Att);
		assertEquals(1, modifiedAttributes.size());
		// Add same node
		modifiedAttributes = bc.handleResourceUpdate(kN1, kN1Att);
		assertEquals(0, modifiedAttributes.size());
		// Add same node with updated properties
		modifiedAttributes = bc.handleResourceUpdate(kN2, kN2Att);
		assertEquals(0, modifiedAttributes.size());
		// Add same node with updated properties
		modifiedAttributes = bc.handleResourceUpdate(kN1, kN1AttOther);
		assertEquals(1, modifiedAttributes.size());
		kN1Att.updateVersion();
		// Add same node with previous properties updated
		modifiedAttributes = bc.handleResourceUpdate(kN1, kN1Att);
		assertEquals(1, modifiedAttributes.size());
		// Add another node
		modifiedAttributes = bc.handleResourceUpdate(kN3, kN3Att);
		assertEquals(2, modifiedAttributes.size());
		// Add another node
		modifiedAttributes = bc.handleResourceUpdate(kN4, kN4Att);
		assertEquals(3, modifiedAttributes.size());
		// Add null node
		modifiedAttributes = bc.handleResourceUpdate(null, kN4Att);
		assertEquals(0, modifiedAttributes.size());
		// Add null AttributeCollection
		modifiedAttributes = bc.handleResourceUpdate(kN1, null);
		assertEquals(0, modifiedAttributes.size());
		// Add null null
		modifiedAttributes = bc.handleResourceUpdate(null, null);
		assertEquals(0, modifiedAttributes.size());
		
		
		// Remove node 4
		modifiedAttributes = bc.removeChild(kN4);
		assertEquals(3, modifiedAttributes.size());
		// Remove same node 4
		modifiedAttributes = bc.removeChild(kN4);
		assertEquals(0, modifiedAttributes.size());
		// Remove null node
		modifiedAttributes = bc.removeChild(null);
		assertEquals(0, modifiedAttributes.size());
		//ResourcesPrinter.printCollection(bc.getResourceCollection());
		
	}
	
	@Test
	public void testResourceSearch(){
		BrokerCollection bc1 = new BrokerCollection();
		BrokerCollection bc2 = new BrokerCollection();
		BrokerCollection bc3 = new BrokerCollection();
		BrokerCollection bc4 = new BrokerCollection();
		
		
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:01", 5683, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "eth0", "00:00:00:00:00:02", 5683, null, "gianvito.net", "*");
		KuraNode kN3 = new KuraNode("192.168.1.3", "lo0", "00:00:00:00:00:03", 5683, null, "gianvito.net", "bologna");
		KuraNode kN4 = new KuraNode("192.168.1.4", "eth0","00:00:00:00:00:04", 5683, null, "gianvito.net", "milano");
//		KuraNode kN5 = new KuraNode("192.168.1.5", "lo0","00:00:00:00:00:05", 5683, null, "gianvito.net", "bologna/gobetti");
//		KuraNode kN6 = new KuraNode("192.168.1.6", "wlan0", "00:00:00:00:00:06", 5683, null, "gianvito.net", "bologna/gobetti/10");
//		KuraNode kN7 = new KuraNode("192.168.1.7", "wlan0", "00:00:00:00:00:07", 5683, null, "gianvito.net", "milano/casa");
//		KuraNode kN8 = new KuraNode("192.168.1.8", "wlan0", "00:00:00:00:00:08", 5683, null, "gianvito.net", "milano/lavoro");
//		KuraNode kN9 = new KuraNode("192.168.1.9", "wlan0", "00:00:00:00:00:09", 5683, null, "gianvito.com", "milano");
//		KuraNode kN10 = new KuraNode("192.168.1.10", "wlan0", "00:00:00:00:00:10", 5683, null, "gianvito.com", "milano/casa");
		
		ExtendedResource waterR1 = createResource("water", kN1.getDomain(), kN1.getGroup(), "obj", "bologna", "4000", "10000");
		ExtendedResource waterRCopy1 = createResource("water", kN1.getDomain(), kN1.getGroup(), "obj", "bologna", "4000", "10000");
		ExtendedResource tempR1 = createResource("temperature", kN1.getDomain(), kN1.getGroup(), "obj", "bologna", "4000", "10000");
		ExtendedResource hum1 = createResource("humidity", kN1.getDomain(), kN1.getGroup(), "sensor", "milano", "13000", "20000");
		ExtendedResource tempR2 = createResource("humidity", kN2.getDomain(), kN2.getGroup(), "sensor", "milano", "13000", "20000");
		ExtendedResource tempR3 = createResource("humidity", kN3.getDomain(), kN3.getGroup(), "oggetto", "milano", "13000", "20000");
		ExtendedResource tempR4 = createResource("humidity", kN4.getDomain(), kN4.getGroup(), "sensore", "cucina", "10", "20");
		
		LinkAttribute lA1 = new LinkAttribute("position", "bologna");
//		LinkAttribute lA2 = new LinkAttribute("p1", "1");
		LinkAttribute lA3 = new LinkAttribute("p2", "20000");
		LinkAttribute lA4 = new LinkAttribute("position", "milano");
		LinkAttribute lA5 = new LinkAttribute("rt", "oggetto");
		LinkAttribute lA6 = new LinkAttribute("p1", "13000");
//		LinkAttribute lA7 = new LinkAttribute("p1", "10");
//		
//		AttributeCollection kN1Att = new AttributeCollection();
//		AttributeCollection kN2Att = new AttributeCollection();
//		AttributeCollection kN3Att = new AttributeCollection();
//		AttributeCollection kN4Att = new AttributeCollection();
//		AttributeCollection kN1AttOther = new AttributeCollection();
//		AttributeCollection kN6Att = new AttributeCollection();
//		AttributeCollection kN7Att = new AttributeCollection();
		
		// Create a hierarchy
		bc1.addResourceProperties(waterR1);
		bc1.addResourceProperties(waterRCopy1);
		bc1.addResourceProperties(tempR1);
		bc1.addResourceProperties(hum1);
		bc2.addResourceProperties(tempR2);
		bc3.addResourceProperties(tempR3);
		bc4.addResourceProperties(tempR4);
		
		
		// Get nodeProperties and resourceProperties and assert them on every BrokerCollection
		// bc1 receives resource update from kN2
		bc1.handleResourceUpdate(kN2, bc2.getAttributeCollection());
		bc1.handleResourceUpdate(kN3, bc3.getAttributeCollection());
		bc2.handleResourceUpdate(kN4, bc4.getAttributeCollection());
		bc1.handleResourceUpdate(kN2, bc2.getAttributeCollection());		
		
		// Create a first search. BY kN2 to kN1. Only ResourceProperties.
		AttributeCollection attToSearch = new AttributeCollection();
		attToSearch.add(lA1);
		
		RequestResult results1from2 = bc1.handleResourceRequest(kN2, attToSearch);
		Set<ResourceProperty> resProperties = results1from2.getResProperties();
		Set<NodeProperty> nodeProperties = results1from2.getNodeProperties();
//		String domain = results1from2.getDomain();
//		String group = results1from2.getGroup();
		
		BrokerPrinter.printBroker(bc1, 3);
		ResourcesPrinter.printResourceSet(resProperties);
		ResourcesPrinter.printNodeSet(nodeProperties);
		
		assertEquals(3, resProperties.size());  // With 1 duplicate.
		assertEquals(0, nodeProperties.size());
		
		
		// With ResourceProperties and NodeProperties
		AttributeCollection attToSearch2 = new AttributeCollection();
		attToSearch2.add(lA3);
		
		RequestResult results1from22 = bc1.handleResourceRequest(kN2, attToSearch2);
		Set<ResourceProperty> resProperties2 = results1from22.getResProperties();
		Set<NodeProperty> nodeProperties2 = results1from22.getNodeProperties();
//		String domain2 = results1from22.getDomain();
//		String group2 = results1from22.getGroup();
				
		ResourcesPrinter.printResourceSet(resProperties2);
		ResourcesPrinter.printNodeSet(nodeProperties2);
		
		assertEquals(1, resProperties2.size());
		assertEquals(1, nodeProperties2.size()); // Because one is the sender.
				
		// AND TEST
		AttributeCollection attToSearch3 = new AttributeCollection();
		attToSearch3.add(lA4);
		attToSearch3.add(lA5);
		attToSearch3.add(lA6);
		
		RequestResult results1from23 = bc1.handleResourceRequest(kN2, attToSearch3);
		Set<ResourceProperty> resProperties3 = results1from23.getResProperties();
		Set<NodeProperty> nodeProperties3 = results1from23.getNodeProperties();
//		String domain3 = results1from23.getDomain();
//		String group3 = results1from23.getGroup();
		
		assertEquals(0, resProperties3.size());
		assertEquals(1, nodeProperties3.size());
		
		ResourcesPrinter.printResourceSet(resProperties3);
		ResourcesPrinter.printNodeSet(nodeProperties3);
		
		
		// AND TEST with group
		AttributeCollection attToSearch4 = new AttributeCollection();
		attToSearch4.add(lA4);
		attToSearch4.add(lA5);
		attToSearch4.add(lA6);
		attToSearch4.add(new LinkAttribute("group", "genova"));
		
		RequestResult results1from24 = bc1.handleResourceRequest(kN2, attToSearch4);
		Set<ResourceProperty> resProperties4 = results1from24.getResProperties();
		Set<NodeProperty> nodeProperties4 = results1from24.getNodeProperties();
//		String domain4 = results1from24.getDomain();
//		String group4 = results1from24.getGroup();
		
		BrokerPrinter.printBroker(bc1, 4);
		
		ResourcesPrinter.printResourceSet(resProperties4);
		ResourcesPrinter.printNodeSet(nodeProperties4);
		
		assertEquals(0, resProperties4.size());
		assertEquals(0, nodeProperties4.size());
		
		// KuraNode null
		RequestResult resultsNull = bc1.handleResourceRequest(null, attToSearch4);
		assertEquals(0, resultsNull.getResProperties().size());
		assertEquals(0, resultsNull.getNodeProperties().size());
		
		// Both null
		resultsNull = bc1.handleResourceRequest(null, null);
		assertEquals(0, resultsNull.getResProperties().size());
		assertEquals(0, resultsNull.getNodeProperties().size());
		
		// AttributeCollection null.
		resultsNull = bc1.handleResourceRequest(kN2, null);
		assertEquals(0, resultsNull.getResProperties().size());
		assertEquals(0, resultsNull.getNodeProperties().size());
		
		
	}
	
	public CoAPMessage createMessage(KuraNode sender, AttributeCollection attributes){
		return new CoAPMessage(new Long(0), sender, attributes, CoAPOperation.RESOURCE_QUERY_REQUEST);
	}
	
	public PropertyCollection createHierarchy(PropertyCollection pc){
		
		
		
		return pc;
	}
	
	public ExtendedResource createResource(String name, String domain, String group,
													String rt, String position, String p1, String p2){
		ExtendedResource eR = new ExtendedResource(name);
		eR.setResourceType(rt);
		//eR.addInterfaceDescription("");
		eR.addAttribute("position", position);
		eR.addAttribute("p1", p1);
		eR.addAttribute("p2", p2);
		
		return eR;
	}
	
	
	
	
	
	
}
