package net.gianvito.coap.test;

import static org.junit.Assert.assertEquals;
import net.gianvito.coap.collection.server.AttributeCollection;
import net.gianvito.coap.collection.server.PropertyCollection;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;
import net.gianvito.coap.server.debug.ResourcesPrinter;
import net.gianvito.coap.server.resource.ExtendedResource;

import org.junit.Test;

public class PropertyCollectionTest {
	
	
	
	@Test
	public void testResource(){
		PropertyCollection pC = new PropertyCollection();
		
		ExtendedResource waterR = createResource("water", "obj", "bologna", "4000", "10000");
		ExtendedResource waterRCopy = createResource("water", "obj", "bologna", "4000", "10000");
		ExtendedResource tempR = createResource("temperature","obj", "bologna", "4000", "10000");
		ExtendedResource tempR2 = createResource("humidity", "sensor", "milano", "13000", "20000");
		
		AttributeCollection modifiedAttributes = pC.addResourceProperties(waterR);
		assertEquals(4, modifiedAttributes.size());
		
		// Add the same attributes
		modifiedAttributes = pC.addResourceProperties(waterR);
		assertEquals(0, modifiedAttributes.size());
		
		// Add a copy. It's recognized as another object because of the date.
		modifiedAttributes = pC.addResourceProperties(waterRCopy);
		assertEquals(0, modifiedAttributes.size());
		
		// Add another object with the same properties
		modifiedAttributes = pC.addResourceProperties(tempR);
		assertEquals(0, modifiedAttributes.size());
		
		// Add another object with different properties
		modifiedAttributes = pC.addResourceProperties(tempR2);
		assertEquals(4, modifiedAttributes.size());
		
		// Add null
		modifiedAttributes = pC.addResourceProperties(null);
		assertEquals(0, modifiedAttributes.size());
		
		//ResourcesPrinter.printCollection(pC);

		// Remove waterR.
		modifiedAttributes = pC.removeResourceProperties(waterR);
		assertEquals(0, modifiedAttributes.size());
		// Remove waterR again.
		modifiedAttributes = pC.removeResourceProperties(waterR);
		assertEquals(0, modifiedAttributes.size());
		// Remove waterRCopy.
		modifiedAttributes = pC.removeResourceProperties(waterRCopy);
		assertEquals(0, modifiedAttributes.size());
		// Remove waterRCopy.
		modifiedAttributes = pC.removeResourceProperties(tempR);
		assertEquals(4, modifiedAttributes.size());
		// Remove waterRCopy.
		modifiedAttributes = pC.removeResourceProperties(tempR2);
		assertEquals(4, modifiedAttributes.size());
		// Remove null
		modifiedAttributes = pC.removeResourceProperties(null);
		assertEquals(0, modifiedAttributes.size());
		
		//ResourcesPrinter.printCollection(pC);
		
		
	}
	
	@Test
	public void testNode(){
		PropertyCollection pC = new PropertyCollection();
		
		KuraNode kN1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 123, null, "*", "*");
		KuraNode kN2 = new KuraNode("192.168.1.2", "eth0", "00:00:00:00:00:01", 123, null, "*", "*");
		KuraNode kN3 = new KuraNode("192.168.1.3", "lo0", "00:00:00:00:00:02", 123, null, "*", "*");
		KuraNode kN4 = new KuraNode("192.168.1.4", "wlan0", "00:00:00:00:00:03", 123, null, "*", "*");
		
		LinkAttribute lA1 = new LinkAttribute("position", "bologna");
		LinkAttribute lA2 = new LinkAttribute("position", "bologna");
		LinkAttribute lA3 = new LinkAttribute("position", "bologna2");
		LinkAttribute lA4 = new LinkAttribute("location", "bologna3");
		LinkAttribute lA5 = new LinkAttribute("location", "bologna4");
		LinkAttribute lA6 = new LinkAttribute("location", "bologna5");
		LinkAttribute lA7 = new LinkAttribute("location", "bologna6");
		
		AttributeCollection kN1Att = new AttributeCollection();
		AttributeCollection kN2Att = new AttributeCollection();
		AttributeCollection kN3Att = new AttributeCollection();
		AttributeCollection kN4Att = new AttributeCollection();
		AttributeCollection kN1AttOther = new AttributeCollection();
		
		kN1Att.add(lA1); kN1Att.add(lA2);
		kN1AttOther.add(lA7);
		kN2Att.add(lA2);
		kN3Att.add(lA1); kN3Att.add(lA2); kN3Att.add(lA3); kN3Att.add(lA4);
		kN4Att.add(lA1); kN4Att.add(lA2); kN4Att.add(lA3); kN4Att.add(lA4); kN4Att.add(lA5); kN4Att.add(lA6); kN4Att.add(lA7);
		
		AttributeCollection modifiedAttributes = pC.addNodeProperties(kN1, kN1Att);
		assertEquals(1, modifiedAttributes.size());
		// Add same node
		modifiedAttributes = pC.addNodeProperties(kN1, kN1Att);
		assertEquals(0, modifiedAttributes.size());
		// Add same node with other attributes
		modifiedAttributes = pC.addNodeProperties(kN1, kN1AttOther);
		assertEquals(1, modifiedAttributes.size());
		// Add another node with same properties
		modifiedAttributes = pC.addNodeProperties(kN2, kN2Att);
		assertEquals(0, modifiedAttributes.size());
		// Add another node with other properties
		modifiedAttributes = pC.addNodeProperties(kN3, kN3Att);
		assertEquals(2, modifiedAttributes.size());
		// Add mixed properties
		modifiedAttributes = pC.addNodeProperties(kN4, kN4Att);
		assertEquals(2, modifiedAttributes.size());
		
		// Add a null node
		modifiedAttributes = pC.addNodeProperties(null, kN4Att);
		assertEquals(0, modifiedAttributes.size());
		// Add a null AttributeCollection
		modifiedAttributes = pC.addNodeProperties(kN1, null);
		assertEquals(0, modifiedAttributes.size());
		// Add a null null
		modifiedAttributes = pC.addNodeProperties(null, null);
		assertEquals(0, modifiedAttributes.size());
		
		ResourcesPrinter.printCollection(pC, 4);
		
		// Remove node 4.
		modifiedAttributes = pC.removeNodeProperties(kN4, kN4Att);
		assertEquals(2, modifiedAttributes.size());
		// Remove node 4 again.
		modifiedAttributes = pC.removeNodeProperties(kN4, kN4Att);
		assertEquals(0, modifiedAttributes.size());
		// Remove node 3 with wrong attributes
		modifiedAttributes = pC.removeNodeProperties(kN3, kN1Att);
		assertEquals(0, modifiedAttributes.size());
		// Remove node 3 with its attributes.
		modifiedAttributes = pC.removeNodeProperties(kN3, kN3Att);
		assertEquals(2, modifiedAttributes.size());
		// Remove node 2.
		modifiedAttributes = pC.removeNodeProperties(kN2, kN2Att);
		assertEquals(0, modifiedAttributes.size());
		// Remove node 1 with first collection.
		modifiedAttributes = pC.removeNodeProperties(kN1, kN1Att);
		assertEquals(1, modifiedAttributes.size());
		// Remove node 1 with other collection.
		modifiedAttributes = pC.removeNodeProperties(kN1, kN1AttOther);
		assertEquals(1, modifiedAttributes.size());
		// Remove a node again.
		modifiedAttributes = pC.removeNodeProperties(kN1, kN1AttOther);
		assertEquals(0, modifiedAttributes.size());
		
		// Remove a null node
		modifiedAttributes = pC.removeNodeProperties(null, kN4Att);
		assertEquals(0, modifiedAttributes.size());
		// Remove a null AttributeCollection
		modifiedAttributes = pC.removeNodeProperties(kN1, null);
		assertEquals(0, modifiedAttributes.size());
		// Remove a null null
		modifiedAttributes = pC.removeNodeProperties(null, null);
		assertEquals(0, modifiedAttributes.size());
		
		//ResourcesPrinter.printCollection(pC);
	}
	
/*	public List<ExtendedResource> createSameAttributeResources(){
		ArrayList<ExtendedResource> list = new ArrayList();
		
		for(Integer i = 0; i < 10; i++){
			list.add(createResource("tempo", "temp", "bologna", i.toString(), i.toString()));
		}
		
		return list;
		
	}
	
	public List<ExtendedResource> createDifferentAttributeResources(){
		ArrayList<ExtendedResource> list = new ArrayList();
		
		for(Integer i = 0; i < 10; i++){
			list.add(createResource("tempo" ,"temp", "bologna", i.toString(), i.toString()));
		}
		
		return list;
		
	}*/
	
	public ExtendedResource createResource(String name, String rt, String position, String p1, String p2){
		ExtendedResource eR = new ExtendedResource(name);
		eR.setResourceType(rt);
		//eR.addInterfaceDescription("");
		eR.addAttribute("position", position);
		eR.addAttribute("p1", p1);
		eR.addAttribute("p2", p2);
		
		return eR;
	}
	
	public KuraNode createNode(){
		return null;
		
	}
}
