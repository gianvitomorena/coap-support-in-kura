package net.gianvito.coap.stress;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.gianvito.coap.dtls.test.DTLSClient;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.junit.Test;

public class RdRemoteStressTest {
	ExecutorService executor = Executors.newCachedThreadPool();
	
	/**
	 * POST test between devices.
	 */
	@Test
	public void resourceRequestStress(){
		// First test. POST from Rasp2 to PC
		//CoapClient client = new CoapClient("192.168.2.110:5683/rd-remote?domain=gianvito.com&group=all");
		// Second test. POST from PC to Rasp1
		//CoapClient client = new CoapClient("192.168.2.211:5683/rd-remote?domain=gianvito.net");
		// First test. POST from Rasp2 to Rasp1
		CoapClient client = new CoapClient("192.168.2.211:5683/rd-remote?domain=gianvito.com&group=all");
		// Second test. POST from Rasp1 to Rasp2
		//CoapClient client = new CoapClient("192.168.2.212:5683/rd-remote?domain=root");
		// Second test. POST from PC110 to PC118
		//CoapClient client = new CoapClient("192.168.2.118:5683/rd-remote?domain=gianvito.net&group=bologna");
		// Second test. POST from PC118 to PC110
		//CoapClient client = new CoapClient("192.168.2.110:5683/rd-remote?domain=gianvito.net&group=all");
		
		
		Date beginTest = new Date();
		System.out.println("Resource Request stress for 100 requests begins on " + beginTest);
		for(int i = 0; i < 1; i++){
			CoapResponse response = client.get();
			System.out.println("Response text --> " + response.getResponseText());
			System.out.println("Execution number --> " + i);
		}
		Date endTest = new Date();
		System.out.println("Resource Request stress for 100 requests ends on " + endTest);
		long duration = endTest.getTime() - beginTest.getTime();
		System.out.println("Duration " + duration);
	}
	
	
	

	

	
	

	
	

}
