package net.gianvito.coap.stress;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

import net.gianvito.coap.client.CoAPDTLSRemoteClient;
import net.gianvito.coap.client.CoAPRemoteClient;
import net.gianvito.coap.collection.property.ResourceProperty;
import net.gianvito.coap.message.KuraNode;
import net.gianvito.coap.resources.LinkAttribute;
import net.gianvito.coap.server.resource.ExtendedResource;

public class CoAPRDStress {
	ExecutorService executor = Executors.newCachedThreadPool();
	
	
	CoAPRemoteClient client1, client2, client3, client4, client5, client6, client7, client8, client9, client10;
	KuraNode node1, node2, node3, node4, node5, node6, node7, node8, node9, node10, destination;
	CoAPRemoteClient[] clients;
	KuraNode[] nodes;
	ExtendedResource rs1, rs2, rs3, rs4, rs5, rs6, rs7, rs8, rs9, rs10;
	LinkAttribute att1, att2, att3, att4;
	Set<LinkAttribute> attributes;
	Set<ResourceProperty> properties;
	
	
	@Test
	public void testMultipleClients(){
		setDestination("192.168.2.211", 5683);
		int repetitionNumber = 1;
		int clientsNumber = 500;
		// Init variables
		initializeClients();
		initializeLocalNodes();
		initializeNNodes(clientsNumber);
		initializeNClients(clientsNumber);
		initializeAttributes();
		initializeResources();
		
		long beginTest = System.nanoTime();		
		
		//test10Clients(repetitionNumber);
		//executeExecutor(repetitionNumber);
		testNClients(repetitionNumber, clientsNumber);
		
		
		long endTest = System.nanoTime();
		long duration = (endTest - beginTest) / 1000000;
		System.out.println("Duration " + duration);	
	}
	
	public void test10Clients(int repetitionNumber){
		for(int i = 0; i < repetitionNumber; i++){
			client1.postResources(node1, destination, properties);
			client2.postResources(node2, destination, properties);
			client3.postResources(node3, destination, properties);
			client4.postResources(node4, destination, properties);
			client5.postResources(node5, destination, properties);
			client6.postResources(node6, destination, properties);
			client7.postResources(node7, destination, properties);
			client8.postResources(node8, destination, properties);
			client9.postResources(node9, destination, properties);
			client10.postResources(node10, destination, properties);
		}
	}
	
	public void testNClients(int repetitionNumber, int clientsNumber){
		for(int j = 0; j < repetitionNumber; j++){
			for(int i = 0; i < clientsNumber; i++){
				clients[i].postResources(nodes[i], destination, properties);
			}
		}
	}
	
	public void executeExecutor(int repetitionNumber){
		executor.execute(new RequestThread(client1, repetitionNumber, properties, node1, destination));
		executor.execute(new RequestThread(client2, repetitionNumber, properties, node2, destination));
		executor.execute(new RequestThread(client3, repetitionNumber, properties, node3, destination));
		executor.execute(new RequestThread(client4, repetitionNumber, properties, node4, destination));
		executor.execute(new RequestThread(client5, repetitionNumber, properties, node5, destination));
		executor.execute(new RequestThread(client6, repetitionNumber, properties, node6, destination));
		executor.execute(new RequestThread(client7, repetitionNumber, properties, node7, destination));
		executor.execute(new RequestThread(client8, repetitionNumber, properties, node8, destination));
		executor.execute(new RequestThread(client9, repetitionNumber, properties, node9, destination));
		executor.execute(new RequestThread(client10, repetitionNumber, properties, node10, destination));
	}
	
	public void initializeClients(){
		client1 = new CoAPRemoteClient();
		client2 = new CoAPRemoteClient();
		client3 = new CoAPRemoteClient();
		client4 = new CoAPRemoteClient();
		client5 = new CoAPRemoteClient();
		client6 = new CoAPRemoteClient();
		client7 = new CoAPRemoteClient();
		client8 = new CoAPRemoteClient();
		client9 = new CoAPRemoteClient();
		client10 = new CoAPRemoteClient();
	}
	
	
	public void initializeNClients(int clientsNumber){
		clients = new CoAPRemoteClient[clientsNumber];
		
		for(int i = 0; i < clientsNumber; i++){
			clients[i] = new CoAPRemoteClient();
			//clients[i].getClient().setExecutor(executor);
		}
	}
	
	
	public void initializeLocalNodes(){
		node1 = new KuraNode("192.168.1.1", "wlan0", "00:00:00:00:00:00", 123, null, "*", "*");
		node2 = new KuraNode("192.168.1.1", "eth0", "00:00:00:00:00:00", 123, node1, "gianvito.net", "*");
		node3 = new KuraNode("192.168.1.2", "wlan0", "00:00:00:00:00:01", 123, node2, "gianvito.net", "torino");
		node4 = new KuraNode("192.168.1.3", "wlan0", "00:00:00:00:00:02", 123, node2, "gianvito.net", "bologna");
		node5 = new KuraNode("192.168.1.4", "eth0", "00:00:00:00:00:03", 123, node3, "gianvito.net", "torino/12");
		node6 = new KuraNode("192.168.1.52", "wlan0", "00:00:00:00:00:04", 123, node4, "gianvito.net", "bologna/10");
		node7 = new KuraNode("192.168.1.6", "lo0", "00:00:00:00:00:05", 123, node1, "gianvito.com", "*");
		node8 = new KuraNode("192.168.1.62", "wlan0", "00:00:00:00:00:52", 123, node6, "gianvito.com", "bologna");
		node9 = new KuraNode("192.168.1.72", "wlan0", "00:00:00:00:00:52", 123, node6, "gianvito.com", "bologna/9");
		node10 = new KuraNode("192.168.1.82", "wlan0", "00:00:00:00:00:52", 123, node6, "gianvito.com", "bologna/15");
	}
	
	public void initializeNNodes(int nodesNumber){
		nodes = new KuraNode[nodesNumber];
		
		for(int i = 0; i < nodesNumber; i++){
			nodes[i] = new KuraNode("192.168.1." + i, "wlan0", "00:00:00:00:00:00", 123, null, "*", "*"); 
		}
	}
	
	
	
	
	public void initializeAttributes(){
		att1 = new LinkAttribute("model", "1");
		att2 = new LinkAttribute("brand", "apple");
		att3 = new LinkAttribute("serial", 15);
		att4 = new LinkAttribute("IMEI", 123123123);
		
		attributes = new HashSet<LinkAttribute>();
		attributes.add(att1);
		attributes.add(att2);
		attributes.add(att3);
		attributes.add(att4);
	}
	
	public void initializeResources(){
		rs1 = new ExtendedResource("/rs1");  rs1.addAttributes(attributes);
		rs2 = new ExtendedResource("/rs2");  rs2.addAttributes(attributes);
		rs3 = new ExtendedResource("/rs3");  rs3.addAttributes(attributes);
		rs4 = new ExtendedResource("/rs4");  rs4.addAttributes(attributes);
		rs5 = new ExtendedResource("/rs5");  rs5.addAttributes(attributes);
		rs6 = new ExtendedResource("/rs6");  rs6.addAttributes(attributes);
		rs7 = new ExtendedResource("/rs7");  rs7.addAttributes(attributes);
		rs8 = new ExtendedResource("/rs8");  rs8.addAttributes(attributes);
		rs9 = new ExtendedResource("/rs9");  rs9.addAttributes(attributes);
		rs10 = new ExtendedResource("/rs10");  rs10.addAttributes(attributes);
		
		properties = new HashSet<ResourceProperty>();
		properties.add(new ResourceProperty(rs1));  properties.add(new ResourceProperty(rs2));
		properties.add(new ResourceProperty(rs3));  properties.add(new ResourceProperty(rs4));
		properties.add(new ResourceProperty(rs5));  properties.add(new ResourceProperty(rs6));
		properties.add(new ResourceProperty(rs7));  properties.add(new ResourceProperty(rs8));
		properties.add(new ResourceProperty(rs9));  properties.add(new ResourceProperty(rs10));
	}
	
	
	public void setDestination(String address, int port){
		destination = new KuraNode(address, "wlan0", "00:00:00:00:00:00", port, null, "*", "*");
	}
	
	
	class RequestThread implements Runnable{
		
		private int repetitionNumber;
		private CoAPRemoteClient client;
		private KuraNode localNode, destination;
		private Set<ResourceProperty> resP;
		
		public RequestThread(CoAPRemoteClient client, int repetitionNumber, Set<ResourceProperty> resP,
				KuraNode localNode, KuraNode destination){
			this.repetitionNumber = repetitionNumber;
			this.client = client;
			this.resP = resP;
			this.localNode = localNode;
			this.destination = destination;
		}
		
		@Override
		public void run() {
			for(int i = 0; i < repetitionNumber; i++){
				System.out.println("Posting for " + localNode.getKuraAddress() + " to " + destination.getKuraAddress());
				client.postResources(localNode, destination, resP);
			}
		}
	}
}
