package net.gianvito.coap.stress;

import java.util.Date;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResponse;
import org.junit.Test;

/**
 * MQTT test.
 */
public class MQTTStressTest {

	@Test
	public void resourceRequestStress2(){
		// First test. POST from Rasp2 to PC
		//CoapClient client = new CoapClient("192.168.2.110:5683/rd-remote?domain=gianvito.com&group=all");
		// Second test. POST from PC to Rasp1
		CoapClient client = new CoapClient("192.168.2.110:5683/rd-remote?domain=gianvito.com&dash=2");
		// First test. POST from Rasp2 to Rasp1
		//CoapClient client = new CoapClient("192.168.2.212:5683/rd-remote?domain=gianvito.net&dash=2");
		// Second test. POST from Rasp1 to Rasp2
		//CoapClient client = new CoapClient("192.168.2.110:5683/rd-remote?domain=root&dash=2");
		// Second test. POST from PC to PC
		//CoapClient client = new CoapClient("192.168.2.110:5683/rd-remote?domain=gianvito.net&dash=2");
		
		Date beginTest = new Date();
		System.out.println("Resource Request stress for 100 requests begins on " + beginTest);
		for(int i = 0; i < 10000; i++){
			//System.out.println("Execution number --> " + i + "\n");
			client.get();
//			client.get(new CoapHandler(){
//
//				@Override
//				public void onError() {
//					// TODO Auto-generated method stub
//					System.out.println("Error");
//				}
//
//				@Override
//				public void onLoad(CoapResponse arg0) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//			});
			//System.out.println("Response text --> " + response.getResponseText());
		}
		Date endTest = new Date();
		System.out.println("Resource Request stress for 100 requests ends on " + endTest);
		long duration = endTest.getTime() - beginTest.getTime();
		System.out.println("Duration " + duration);
	}
}
