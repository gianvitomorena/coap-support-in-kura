package net.gianvito.coap.stress;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.californium.core.CoapClient;
import org.junit.Test;

public class MultiThreadStress {
	ExecutorService executor = Executors.newCachedThreadPool();
	
	
	@Test
	public void resourceMultiThread(){
		CoapClient client1 = new CoapClient("192.168.2.211:5683/rd-remote?domain=gianvito.com");
		CoapClient client2 = new CoapClient("192.168.2.202:5683/rd-remote?domain=root");
		CoapClient client3 = new CoapClient("192.168.2.211:5683/rd-remote?domain=root");
		CoapClient client4 = new CoapClient("192.168.2.202:5683/rd-remote?group=bologna");
		
		Date beginTest = new Date();
		System.out.println("Resource request multithread for 100 requests begins on " + beginTest);
		for(int i = 0; i < 0; i++){
			executeRequest(client1);
			//executeRequest(client2);
			executeRequest(client3);
			//executeRequest(client4);
			System.out.println("Execution number --> " + i);
		}
		Date endTest = new Date();
		System.out.println("Resource Request multithread for 100 requests ends on " + endTest);
		long duration = endTest.getTime() - beginTest.getTime();
		System.out.println("Duration " + duration);
	}
	
	class RequestThread implements Runnable{
				
		CoapClient client;
		
		public RequestThread(CoapClient client){
			this.client = client;
		}

		public void run() {
			client.get();
		}
	}
	
	public void executeRequest(CoapClient client){
		Runnable requestHandler = new RequestThread(client);
		executor.execute(requestHandler);
	}
}
