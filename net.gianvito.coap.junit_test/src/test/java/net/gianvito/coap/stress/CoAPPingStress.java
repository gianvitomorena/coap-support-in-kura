package net.gianvito.coap.stress;

import java.util.Date;

import org.eclipse.californium.core.CoapClient;

public class CoAPPingStress {
	//@Test
	public void pingStress(){
		CoapClient client = new CoapClient("192.168.2.211");
		Date beginTest = new Date();
		System.out.println("Resource Request stress for 100 requests begins on " + beginTest);
		for(int i = 0; i < 0; i++){
			System.out.println("Execution number --> " + i + "\n");
			boolean pingResponse = client.ping();
			System.out.println("Response --> " + pingResponse);
		}
		Date endTest = new Date();
		System.out.println("Resource Request stress for 100 requests ends on " + endTest);
		long duration = endTest.getTime() - beginTest.getTime();
		System.out.println("Duration " + duration);
	}
}
