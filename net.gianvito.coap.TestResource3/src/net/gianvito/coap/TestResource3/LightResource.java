package net.gianvito.coap.TestResource3;

import net.gianvito.coap.server.CoAPServer;
import net.gianvito.coap.server.resource.ExtendedResource;

import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightResource extends ExtendedResource{
	private static final long serialVersionUID = 5342691119562068656L;
	
	private static final Logger logger = LoggerFactory.getLogger(LightResource.class);
	private static String resourceName = "light";
	private String resourceType = "object";
	private CoAPServer cs;
	
	public LightResource(){
		super(resourceName);
		this.setResourceType(resourceType);
		this.addAttribute("position", "salotto");
		this.addAttribute("brand", "HouseSPA");
		this.addAttribute("model", "101");
	}
	
	protected void activate(ComponentContext componentContext){
		logger.info("Adding resource --> " + resourceName);
		// If we want to add domain and group attributes
		this.updateDomainAndGroup(cs);
		cs.addResource(this, ExtendedResource.class);
		logger.info("Resource added.");
	}
		
	protected void deactivate(ComponentContext componentContext){
		logger.info("Resource removed. --> " + resourceName);
		cs.removeResource(this, ExtendedResource.class);
	}
	
	@Override
	public void handleGET(CoapExchange exchange){
		exchange.respond(LinkFormat.serializeResource(this).toString());
	}
	
	protected void setCoAPServer(CoAPServer cs){
		this.cs = cs;
	}
	protected void unsetCoAPServer(CoAPServer cs){
		this.cs = null;
	}
}
