package net.gianvito.coap.TestResource7;

import net.gianvito.coap.server.CoAPServer;
import net.gianvito.coap.server.resource.ExtendedResource;

import org.eclipse.californium.core.coap.LinkFormat;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeyboardResource extends ExtendedResource{
	private static final long serialVersionUID = -7157426223005582248L;
	
	private static final Logger logger = LoggerFactory.getLogger(KeyboardResource.class);
	private static String resourceName = "keyboard";
	private String resourceType = "hardware";
	private CoAPServer cs;
	
	public KeyboardResource(){
		super(resourceName);
		this.setResourceType(resourceType);
		this.addAttribute("position", "room_2");
		this.addAttribute("brand", "Apple");
		this.addAttribute("model", "Wireless_Keyboard");
	}
	
	protected void activate(ComponentContext componentContext){
		logger.info("Adding resource --> " + resourceName);
		// If we want to add domain and group attributes
		this.updateDomainAndGroup(cs);
		cs.addResource(this, ExtendedResource.class);
		logger.info("Resource added.");
	}
		
	protected void deactivate(ComponentContext componentContext){
		logger.info("Resource removed --> " + resourceName);
		cs.removeResource(this, ExtendedResource.class);
	}
	
	@Override
	public void handleGET(CoapExchange exchange){
		exchange.respond(LinkFormat.serializeResource(this).toString());
	}
	
	protected void setCoAPServer(CoAPServer cs){
		this.cs = cs;
	}
	protected void unsetCoAPServer(CoAPServer cs){
		this.cs = null;
	}
}
